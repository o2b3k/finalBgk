$(document).ready(function () {
	"use strict";
	$(".gk_burger-menu").on("click", function (e) {
		$('.gk_nav-phone').toggle();
		e.stopPropagation();
		e.preventDefault();
	});

	$(window).resize(function () {
		if ($(window).width() < 992) {
			$(".gk_nav .container").removeClass("container")
			$(".fl").addClass("container-fluid")
		}
		if ($(window).width() > 992) {
			$(".fl").removeClass("container-fluid")
			$(".fl").addClass("container")
			$(".gk_nav-phone").css("display", "none")
		}
	});
	
	// var x = $('.ul_main_menu');
	// var showMore = $('.more')
	
	// console.log(x.children().length);
	// if(x.children().length <= 9){
	// 	$('.ul_main_menu > li:last').hide();
	// }else{
	// 	$('.ul_main_menu > li:last').show();
	// }
	
	
});