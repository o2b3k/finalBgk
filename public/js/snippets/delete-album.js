const deleteAlbumForm = $('#delete-album-form');
$('.btn-delete-album').click(function () {
    if (confirm('Вы действительно хотите удалить эту альбом')){
        var $this = $(this);
        deleteAlbumForm.find('#album_id').val($this.data('id'));
        deleteAlbumForm.submit();
    }
});