const deleteFooterForm = $('#delete-footer-form');
$('.btn-delete-footer').click(function () {
    if (confirm('Вы действительно хотите удалить эту футер')){
        let $this = $(this);
        deleteFooterForm.find('#footer_id').val($this.data('id'));
        deleteFooterForm.submit();
    }
});