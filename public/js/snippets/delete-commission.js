const deleteCommissionForm = $('#delete-commission-form');
$('.btn-delete-commission').click(function () {
    if (confirm('Вы действительно хотите удалить эту коммиссию')){
        let $this = $(this);
        deleteCommissionForm.find('#commission_id').val($this.data('id'));
        deleteCommissionForm.submit();
    }
});