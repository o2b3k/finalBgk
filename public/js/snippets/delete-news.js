const deleteNewsFrom = $('#delete-news-form');
$('.btn-delete-news').click(function () {
   if (confirm('Вы действительно хотите удалить эту статья')){
       let $this = $(this);
       deleteNewsFrom.find('#news_id').val($this.data('id'));
       deleteNewsFrom.submit();
   }
});