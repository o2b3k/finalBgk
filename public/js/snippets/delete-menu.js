const deleteMenuForm = $('#delete-menu-form');
$('.btn-delete-menu').click(function () {
    if (confirm('Вы действительно хотите удалить эту меню')){
        let $this = $(this);
        deleteMenuForm.find('#menu_id').val($this.data('id'));
        deleteMenuForm.submit();
    }
});