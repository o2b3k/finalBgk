const deleteDocumentForm = $('#delete-document-form');
$('.btn-delete-document').click(function () {
    if (confirm('Вы действительно хотите удалить эту Документ')){
        var $this = $(this);
        deleteDocumentForm.find('#document_id').val($this.data('id'));
        deleteDocumentForm.submit();
    }
});