const deleteCategoryForm = $('#delete-category-form');
$('.btn-delete-category').click(function () {
    if (confirm('Вы действительно хотите удалить эту категорию')){
        let $this = $(this);
        deleteCategoryForm.find('#category_id').val($this.data('id'));
        deleteCategoryForm.submit();
    }
});