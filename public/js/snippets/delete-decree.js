const deleteDecreeFrom = $('#delete-decree-form');
$('.btn-delete-decree').click(function () {
    if (confirm('Вы действительно хотите удалить эту Документ')){
        let $this = $(this);
        deleteDecreeFrom.find('#decree_id').val($this.data('id'));
        deleteDecreeFrom.submit();
    }
});