const deletePagesForm = $('#delete-pages-form');
$('.btn-delete-pages').click(function () {
    if (confirm('Вы действительно хотите удалить эту страницу')){
        var $this = $(this);
        deletePagesForm.find('#pages_id').val($this.data('id'));
        deletePagesForm.submit();
    }
});