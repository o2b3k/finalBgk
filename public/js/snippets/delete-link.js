const deleteLinkForm = $('#delete-link-form');
$('.btn-delete-link').click(function () {
    if (confirm('Вы действительно хотите удалить эту ссылку')){
        let $this = $(this);
        deleteLinkForm.find('#link_id').val($this.data('id'));
        deleteLinkForm.submit();
    }
});