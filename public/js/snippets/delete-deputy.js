const deleteDeputyForm = $('#delete-deputy-form');
$('.btn-delete-deputy').click(function () {
    if (confirm('Вы действительно хотите удалить эту депутат')){
        let $this = $(this);
        deleteDeputyForm.find('#deputy_id').val($this.data('id'));
        deleteDeputyForm.submit();
    }
});