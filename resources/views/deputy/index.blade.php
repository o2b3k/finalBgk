@extends('main')
@section('page_header')
    <h1 class="page-title">Депутаты</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('deputy.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Ф.И.О</th>
                        <th>Партия</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $count = 1 ?>
                    @foreach($deputies as $deputy)
                        <tr>
                            <td>{{ $count++ }}</td>
                            <td>{{ $deputy->fio }}</td>
                            <td>
                                @isset($deputy->faction)
                                    {{ $deputy->faction->name }}
                                @endisset
                            </td>
                            <td>
                                <a href="{{ route('deputy.Edit', ['$deputy' => $deputy ]) }}" class="btn btn-sm btn-success">Изменить</a>
                                <button type="button" data-id="{{ $deputy->id }}" class="btn btn-sm btn-danger btn-delete-deputy">
                                    Удалить
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $deputies->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-deputy-form" class="d-none" action="{{ route('deputy.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="deputy_id" name="deputy_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-deputy.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush