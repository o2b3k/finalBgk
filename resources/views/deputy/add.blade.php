@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('deputy.Store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="form-group">
                            <h4 class="example-title">Ф.И.О *</h4>
                            <input type="text" name="fio" class="form-control"
                                   id="inputHelpText" value="{{ old('fio') }}" required>
                            @if($errors->has('fio'))
                                <span class="has-error">{{ $errors->first('fio') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <h4 class="example-title">Телефон</h4>
                            <input type="text" name="tel" class="form-control"
                                   id="inputHelpText" value="{{ old('tel') }}">
                            @if($errors->has('tel'))
                                <span class="has-error">{{ $errors->first('tel') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <h4 class="example-title">E-mail</h4>
                            <input type="email" name="email" class="form-control"
                                   id="inputHelpText" value="{{ old('email') }}">
                            @if($errors->has('email'))
                                <span class="has-error">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <h4 class="example-title">Facebook</h4>
                            <input type="url" name="fb" class="form-control"
                                   id="inputHelpText" value="{{ old('fb') }}">
                            @if($errors->has('fb'))
                                <span class="has-error">{{ $errors->first('fb') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <h4 class="example-title">Instagram</h4>
                            <input type="url" name="ins" class="form-control"
                                   id="inputHelpText" value="{{ old('ins') }}">
                            @if($errors->has('ins'))
                                <span class="has-error">{{ $errors->first('ins') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <h4 class="example-title">Изоброжения</h4>
                            <input type="file" name="image" class="form-control"
                                   id="inputHelpText" value="{{ old('image') }}" required>
                            @if($errors->has('image'))
                                <span class="has-error">{{ $errors->first('image') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('faction_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Партия *</h4>
                            <select name="faction_id" id="faction_id" class="form-control" required>
                                    <option value="null"></option>
                                @foreach($factions as $faction)
                                    <option value="{{ $faction->id }}">{{ $faction->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('profile') ? 'has-error' : '' }}">
                            <h4 class="example-title">Анкета *</h4>
                            <textarea name="profile" class="form-control my-editor"></textarea>
                            @if($errors->has('profile'))
                                <span class="has-error">{{ $errors->first('profile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Язык</label>
                            <select name="lang" class="form-control" title="">
                                <option value="ru">Русский</option>
                                <option value="ky">Кыргызча</option>
                            </select>
                        </div>
                    </div>
                </div>
                <a href="{{ route('deputy.RuIndex') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Добавить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            language_url : '/js/ru.js',
            selector: "textarea.my-editor",
            height : "250",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            },
        };
        tinymce.init(editor_config);
    </script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush