@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('footer.Store') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-8 col-ld-8">
                        <div class="form-group">
                            <h4 class="example-title">Имя</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name') }}" required>
                            <span class="text-help">Наименование футер</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <h4 class="example-title">Язык</h4>
                            <select name="lang" id="" class="form-control">
                                <option value="ru">Русский</option>
                                <option value="ky">Кыргызча</option>
                            </select>
                            @if($errors->has('lang'))
                                <span class="has-error">{{ $errors->first('lang') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group d-none">
                            <h4 class="example-title">Позиция</h4>
                            <input type="text" name="position" class="form-control"
                                   id="inputHelpText" value="{{ old('position') }}">
                            <span class="text-help">Позиция футер</span>
                            @if($errors->has('position'))
                                <span class="has-error">{{ $errors->first('position') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success float-sm-right">Добавить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush