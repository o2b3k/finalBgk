@extends('main')
@section('page_header')
    <h1 class="page-title">{{ $footer->name }}</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Имя</th>
                        <th>Ссылка</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($footer->link as $link)
                        <tr>
                            <td>{{ $link->id }}</td>
                            <td>{{ $link->name }}</td>
                            <td>{{ $link->url }}</td>
                            <td>
                                <button type="button" data-id="{{ $link->id }}"
                                        class="btn btn-sm btn-danger btn-delete-link">
                                    Удалить
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <form action="{{ route('footer.storeLink',['footer' => $footer]) }}" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-5 col-ld-5">
                        <div class="form-group">
                            <h4 class="example-title">Имя</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name') }}" required>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-5 col-ld-5">
                        <div class="form-group">
                            <h4 class="example-title">Ссылка</h4>
                            <input type="text" name="url" class="form-control"
                                   id="inputHelpText" value="{{ old('url')  }}" required>
                            @if($errors->has('url'))
                                <span class="has-error">{{ $errors->first('url') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2 col-ld-2">
                        <div class="form-group">
                            <h4 class="example-title">
                                Добавить
                            </h4>
                            <button class="btn btn-sm">
                                Добавить
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-link-form" action="{{ route('link.Destroy') }}" class="hidden" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="link_id" id="link_id">
    </form>
@stop
@push('js')
    <script src="{{ asset('js/snippets/delete-link.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush