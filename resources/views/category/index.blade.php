@extends('main')
@section('page_header')
    <h1 class="page-title">Категория</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('category.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Наименование</th>
                        <th>Ссылка</th>
                        <th>Опубликован</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $cat)
                        <tr>
                            <td>{{ $cat->id }}</td>
                            <td>{{ $cat->name }}</td>
                            <td>{{ $cat->slug }}</td>
                            <td class="w-50">
                                <div class="checkbox-custom checkbox-primary">
                                    <input type="checkbox" class="published" data-id="{{ $cat->id }}"
                                           @if($cat->published) checked @endif/>
                                    <label for=""></label>
                                </div>
                            </td>
                            <td>
                                <a href="{{ route('category.Edit',['id' => $cat->id ]) }}" class="btn btn-sm btn-success">Изменить</a>
                                <button type="button" data-id="{{ $cat->id }}" class="btn btn-sm btn-danger btn-delete-category">
                                    Удалить
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $categories->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-category-form" class="d-none" action="{{ route('category.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="category_id" name="category_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-category.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            $('.published').on('click', function(event){
                let id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('category.Published') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
        })
    </script>
@endpush