@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('category.Update',['id' => $category->id ]) }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Имя</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name',$category->name) }}">
                            <span class="text-help">Наименование категория</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-12">
                        <div class="form-group">
                            <label for="type">Тип категория</label>
                            <select name="type" id="type" class="form-control" disabled>
                                <option value="{{ $category->type }}"{{ old('type') == $category->type ? ' selected' : '' }}>
                                    @if($category->type == \App\Models\Category::TYPE_CHILD_CATEGORY)
                                        Дочерняя категория
                                    @else
                                        Родительская категория
                                    @endif
                                </option>
                            </select>
                            @if ($errors->has('type'))
                                <span class="has-error">{{ $errors->first('type') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('category.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Изменить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script>
        var parentId = $('#parent');
        $('#type').change(function () {
            var $this = $(this);
            if ($this.val() === '{{ \App\Models\Category::TYPE_CHILD_CATEGORY }}'){
                parentId.val($('#parent').val());
                parentId.closest('.form-group').removeClass('d-none');
            }else{
                parentId.val('');
                parentId.closest('.form-group').addClass('d-none');
            }
        })
    </script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush