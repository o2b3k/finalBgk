@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <h4 class="example-title">{{ $faction->name }}</h4>
            <form action="{{ route('faction.addMember',['faction' => $faction]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Ф.И.О</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($faction->members as $member)
                                <tr>
                                    <td>{{ $member->id }}</td>
                                    <td>{{ $member->fio }}</td>
                                    <td>
                                        <a href="{{ route('faction.deleteMember',['faction' => $faction,'id' => $member->id]) }}" class="btn btn-sm btn-danger">Удалить</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-8 col-lg-8">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Депутаты *</h4>
                            <select name="deputy_id" id="deputy_id" class="form-control">
                                @foreach($deputies as $deputy)
                                    <option value="{{ $deputy->id }}">{{ $deputy->fio }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2 col-lg-2">
                        <div class="form-group">
                            <h4 class="example-title">Добавить *</h4>
                            <button type="submit" class="btn btn-success float-sm-left">Добавить</button>
                        </div>
                    </div>
                </div>
                <a href="{{ route('faction.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush