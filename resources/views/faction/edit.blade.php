@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('faction.Update', ['id' => $faction->id]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12 col-ld-12">
                        <div class="form-group">
                            <h4 class="example-title">Название</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name', $faction->name) }}">
                            <span class="text-help">Название фракция</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-12">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Руководитель *</h4>
                            <select name="deputy_id" id="deputy_id" class="form-control">
                                @isset($faction->deputy)
                                    <option value="{{ $faction->deputy->id }}">{{ $faction->deputy->fio }}</option>
                                @endisset
                                @foreach($deputies as $deputy)
                                    <option value="{{ $deputy->id }}">{{ $deputy->fio }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('text') ? 'has-error' : '' }}">
                            <h4 class="example-title">Описание</h4>
                            <textarea name="desc" class="form-control my-editor">
                                {!! $faction->desc !!}
                            </textarea>
                            @if($errors->has('desc'))
                                <span class="has-error">{{ $errors->first('desc') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('faction.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Добавить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            language_url : '/js/ru.js',
            selector: "textarea.my-editor",
            height : "250",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            },
        };
        tinymce.init(editor_config);
    </script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush