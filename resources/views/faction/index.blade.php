@extends('main')
@section('page_header')
    <h1 class="page-title">Фракция</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('faction.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Название</th>
                            <th>Руководитель</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($factions as $faction)
                        <tr>
                            <td>{{ $faction->id }}</td>
                            <td>{{ $faction->name }}</td>
                            <td>
                                @isset($faction->deputy)
                                    {{ $faction->deputy->fio }}
                                @endisset
                            </td>
                            <td>
                                <a href="{{ route('faction.Edit',['id' => $faction->id ]) }}" class="btn btn-sm btn-success">Изменить</a>
                                <a href="{{ route('faction.addMemberForm',['faction' => $faction]) }}" class="btn btn-sm btn-primary">
                                    <i class="icon md-account-add"></i>
                                </a>
                                <button type="button" data-id="{{ $faction->id }}" class="btn btn-sm btn-danger btn-delete-faction">
                                    Удалить
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-faction-form" class="d-none" action="{{ route('faction.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="faction_id" name="faction_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-faction.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            $('.published').on('click', function(event){
                let id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('category.Published') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
        })
    </script>
@endpush