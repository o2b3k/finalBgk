@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop
@section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop
@section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container default-body">
            <div class="row">
                <div class="col-lg-8">
                    <div class="gk_link_menu-custom">
                        <a href="{{ route('index') }}">{{ trans('home.home') }}</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>{{ trans('home.deputy')  }}
                    </div>
                    <div class="gk_profile">
                        <h2 class="heading_on">{{ trans('home.deputy') }}
                        </h2>
                        @isset($deputies)
                            @foreach($deputies as $member)
                                <a href="{{ route('front.DeputyView',['id' => $member->id]) }}">
                                    <div class="gk_profile-user">
                                        <div class="gk_profile-img_name">
                                            <div class="gk_profile-user-img">
                                                <img src="{{ asset($member->image) }}" alt="">
                                            </div>
                                            <div class="gk_profile-user-info">
                                                <h2>{{ $member->fio }}</h2>
                                                <p>{!! \App\Models\Deputy::getExcerpt($member->profile) !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        @endisset
                    </div>
                    <!--Pagination  -->
                    <div class="gk_pagination">
                        <ul>
                            @isset($deputies)
                                {{ $deputies->links() }}
                            @endisset
                        </ul>
                    </div>
                </div>
                <!--Sidebar  -->
                @include('frontend.sidebar')
                <!--End Sidebar  -->
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@stop
@section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop