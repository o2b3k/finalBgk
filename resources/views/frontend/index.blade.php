@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop @section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop @section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_default_main">
        <div class="gk_content-news-module">
            <div class="container">
                <div class="row">
                    <div class="gk_content_large">
                        @isset($position[0])
                            @if($position[0]->news_id != null)
                                <div class="gk_content_first">
                                    <img src="{{ asset($position[0]->news->image) }}" class="gk_main_bg img-responsive"
                                         alt="">
                                    <div class="gk_content_text">
                                        <div>
                                            <div class="gk_content_Date__heading">
                                                <span class="gk_date">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $position[0]->news->created_at)->toDateString()}}
                                                </span>
                                                <a href="{{ route('viewNews',['slug' => $position[0]->news->slug]) }}">
                                                    <h2>{{ strip_tags(substr($position[0]->news->title,0,200)) }}
                                                        ...</h2>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endisset
                        <div class="gk_content_second">
                            @isset($position[1])
                                @if($position[1]->news_id != null)
                                    <div class="gk_img">
                                        <img src="{{ asset($position[1]->news->image) }}"
                                             class="gk_second_bg img-responsive" alt="">
                                        <div class="gk_content_text">
                                            <div class="gk_content_Date__heading">
                                                <span class="gk_date">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $position[1]->news->created_at)->toDateString()
                                                    }}
                                                </span>
                                                <a href="{{ route('viewNews',['slug' => $position[1]->news->slug]) }}">
                                                    <h2>{{ strip_tags(substr($position[1]->news->title,0,120)) }}
                                                        ...</h2>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endisset
                            @isset($position[2])
                                @if($position[2]->news_id != null)
                                    <div class="gk_img">
                                        <img src="{{ asset($position[2]->news->image) }}"
                                             class="gk_second_bg img-responsive" alt="">
                                        <div class="gk_content_text">
                                            <div class="gk_content_Date__heading">
                                    <span class="gk_date">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $position[2]->news->created_at)->toDateString()
                                        }}
                                    </span>
                                                <a href="{{ route('viewNews',['slug' => $position[2]->news->slug]) }}">
                                                    <h2>{{ strip_tags(substr($position[2]->news->title,0,120)) }}
                                                        ...</h2>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gk_item__block">
            <div class="container">
                <div class="row">
                    <div class="gk_grid_massonry">
                        <!-- Новости -->
                        <div class="gk_subitem__block">
                            <div class="gk_subitem__heading">
                                <h4>{{ trans('home.news') }}</h4>
                                <span>
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </span>
                            </div>
                            <div class="gk_subitem__article">
                                @isset($news) @foreach($news as $new)
                                    <div class="gk__article">
                                        <img src="{{ asset($new->image) }}" class="img-responsive mr-1" alt="">
                                        <div class="gk_subcontent">
                                            <div class="gk_group_article">
                                                <h5>
                                                    <a href="{{ route('viewNews',['slug' => $new->slug]) }}">{!! \App\Models\News::getExcerpt($new->title) !!}</a>
                                                </h5>
                                                <span class="gk_date">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new->created_at)->toDateString()
                                            }}
                                        </span>
                                            </div>
                                            <div>
                                                <p>Категория: @isset($new->category)
                                                        <a href="{{ route('front.CategoryNews',['id' => $new->category->id]) }}">{{ $new->category->name }}</a>@endisset
                                                </p>
                                                <p>{!! \App\Models\News::getExcerpt($new->text) !!}</p>
                                            </div>
                                            <a href="{{ route('viewNews',['slug' => $new->slug]) }}"
                                               class="read_more">{{ trans('home.read_more') }}</a>
                                        </div>
                                    </div>
                                @endforeach @endisset
                                <div class="gk_view__all">
                                    <a href="{{ route('front.News') }}">{{ trans('home.show_all') }}</a>
                                </div>
                            </div>
                        </div>
                        <!-- Законы и законопроекты -->
                        <div class="gk_subitem__block gk_zakony">
                            <div class="gk_subitem__heading">
                                <h4>{{ trans('home.index_legislation') }}</h4>
                                <span>
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            </span>
                            </div>
                            <div class="gk_subitem__article">
                                @isset($documents)
                                    @foreach($documents as $document)
                                        <div class="gk__article">
                                            <div class="gk_subcontent">
                                                <div class="gk_group_article">
                                                    <span class="gk_date">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $document->created_at)->toDateString()
                                                        }}
                                                    </span>
                                                    <a href="{{ route('front.DocView', ['id' => $document->id]) }}">
                                                        <h5>{{ $document->name }}</h5>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach @endisset
                                <div class="gk_view__all">
                                    <a href="{{ route('front.Legislation') }}">{{ trans('home.show_all') }}</a>
                                </div>
                            </div>
                        </div>
                        <!-- Ссылки на стронние ресурсы -->
                        <div class="gk_subitem__block gk_link__item">
                            <div class="gk_subitem__heading">
                                <h4>{{ trans('home.home_url') }}</h4>
                                <span>
                                <i class="fa fa-external-link" aria-hidden="true"></i>
                            </span>
                            </div>
                            <a href="http://president.kg" target="_blank">
                                <div class="gk_link__item_block">
                                    <div class="gk_link_Text">
                                        <div class="gk_img_flex_text">
                                            <div class="gk_link_banner">
                                                <img src="/img/banner3.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="gk_flex_text">
                                                <h5>Официальный сайт президента кыргызской республики</h5>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                            <a href="http://kenesh.kg/" target="_blank">
                                <div class="gk_link__item_block">
                                    <div class="gk_link_Text">
                                        <div class="gk_img_flex_text">
                                            <div class="gk_link_banner">
                                                <img src="/img/banner3.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="gk_flex_text">
                                                <h5>Официальный сайт жогорку кенеша </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="http://www.gov.kg/" target="_blank">
                                <div class="gk_link__item_block">
                                    <div class="gk_link_Text">
                                        <div class="gk_img_flex_text">
                                            <div class="gk_link_banner">
                                                <img src="/img/banner3.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="gk_flex_text">
                                                <h5>Правительство кыргызской республики</h5>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                            <a href="http://www.meria.kg/" target="_blank">
                                <div class="gk_link__item_block">
                                    <div class="gk_link_Text">
                                        <div class="gk_img_flex_text">
                                            <div class="gk_link_banner">
                                                <img src="/img/banner3.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="gk_flex_text">
                                                <h5>Мэрия города Бишкек</h5>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                        
                        <!-- Обращение граждан -->
                        <div class="gk_subitem__block gk_callback">
                            <div class="gk_subitem__heading">
                                <h4>{{ trans('home.complain') }}</h4>
                                <span>
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </span>
                            </div>
                            <div class="gk_callback_content">
                                <div class="gk_call_info">
                                    <p>{{ trans('home.info_complain') }}</p>
                                </div>
                                <form action="{{ route('front.ComplainStore') }}" method="POST"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="gk_form__group">
                                        <label for="fio">ФИО *</label>
                                        <input type="text" name="fio" placeholder="Введите ФИО" required>
                                        @if ($errors->has('fio'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('fio') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="email">Email *</label>
                                        <input type="email" name="email" placeholder="your@example.com" required>
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="phone">Телефон</label>
                                        <input type="tel" name="tel" placeholder="+996 XXX XXX XXX">
                                    </div>
                                    <div class="gk_form_group">
                                        <label for="deputy_id">Кому *</label>
                                        <select name="deputy_id">
                                            @isset($factions)
                                                @foreach($factions as $faction)
                                                    <option value="{{ $faction->id }}"
                                                            disabled>{{ $faction->name }}</option>
                                                    @isset($faction->deputy)
                                                        <option value="" disabled>Руководитель</option>
                                                        <option value="{{ $faction->deputy->id }}">
                                                            <span>—</span>{{ $faction->deputy->fio }}</option>
                                                    @endisset
                                                    <option value="" disabled>Члены фракции</option>
                                                    @foreach($faction->members as $deputy)
                                                        <option value="{{ $deputy->id }}">
                                                            <span>— </span>{{ $deputy->fio }}</option>
                                                    @endforeach @endforeach @endisset
                                        </select>
                                    </div>
                                    <div class="gk_form__group">

                                        <input type="file" name="file" id="file_view">
                                        <input type="button" value="Выбрать файл" class="upload" id="upl"
                                               onclick="addItem()">
                                        <script>
                                            var uploadFile = document.getElementById('upl');
                                            var inputFile = document.getElementById('file_view');
                                            var fileName = '';

                                            function addItem() {
                                                uploadFile.addEventListener('click', function () {
                                                    inputFile.click();

                                                    inputFile.addEventListener("change", function () {
                                                        var nFiles = inputFile.files;
                                                        for (var i = 0; i < nFiles.length; i++) {
                                                            if (nFiles[i]) {
                                                                fileName = nFiles[i].name;
                                                                console.log(fileName)
                                                            }
                                                        }
                                                        uploadFile.value = "Выбрано " + fileName
                                                    })
                                                })
                                            }
                                        </script>
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="subject">Тема *</label>
                                        <input type="text" name="subject" placeholder="Ваш вопрос" required>
                                        @if ($errors->has('subject'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('subject') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="text">Сообщение *</label>
                                        <textarea name="text" id="" cols="30" rows="10"
                                                  placeholder=". . ."></textarea>
                                    </div>
                                    <div class="gk_form__group">
                                        {!! NoCaptcha::display() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <button type="submit" class="submit">Отправить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
    <!-- END CONTENT -->
@stop @section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush