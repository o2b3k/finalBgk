@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop
@section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop
@section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container default-body">
            <div class="row">
                <div class="col-lg-8">
                    <div class="gk_link_menu-custom">
                        <a href="{{ route('index') }}">{{ trans('home.home') }}</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>{{ $page->name }}
                    </div>
                    <div class="gk_article__content">
                        <div class="gk_article__content-heading">
                            <h2 class="heading_on">{{ $page->name }}</h2>
                            <br>
                            <div class="gk_article__content-icon">
                                <span class="gk_article__content-date"><i class="fa fa-calendar mr-1" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $page->created_at)->toDateString() }}</span>
                                <div class="gk_article__content_icon_second">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="gk_article__content-description">
                            @isset($page->media)
                                <div class="gk_article__content-image">
                                    <img src="{{ asset($page->media) }}" class="img-responsive" alt="">
                                </div>
                            @endisset
                            <br>
                            <div class="gk_article__content-param">
                                <p>{!! $page->text !!}</p>
                            </div>
                            <br>
                        <!--End  -->
                        </div>
                    </div>
                    <br>
                    <div class="gk_article__share">
                        <div> <span class="gk_article_share_text">Поделиться:</span>
                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                            <script src="//yastatic.net/share2/share.js"></script>
                            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,linkedin,whatsapp,telegram"></div>
                        </div>
                        <div onclick="window.print()"><i class="fa fa-print gk_print" aria-hidden="true"></i>Распечатать</div>
                    </div>
                </div>
                <!--Sidebar  -->
                @include('frontend.sidebar')
                <!--End Sidebar  -->
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@stop
@section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop