@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop
@section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop
@section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container default-body">
            <div class="row">
                <div class="col-lg-8">
                    <div class="gk_link_menu-custom">
                        <a href="{{ route('index') }}">{{ trans('home.home') }}</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        Проекты постановление
                    </div>
                    <!--List legislation  -->
                    <div class="gk_legislation">
                        <div class="gk_legislation-table">
                            <br>
                            <table class="table">
                                <tr>
                                    <th>Проекты постановление</th>
                                    <th></th>
                                </tr>
                                @isset($documents)
                                    @foreach($documents as $document)
                                        <tr>
                                        <td class="table_one">
                                            <span class="gk_date">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $document->created_at)->toDateString() }}
                                                </span>
                                            <p><a href="{{ route('front.DocView', ['id' => $document->id]) }}">{{ $document->name }}</a></p>
                                        </td>
                                        <td class="gk_pdf"><a href="{{ asset($document->attachment) }}" target="_blank">
                                                <i class="ion-ios-list-outline" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endisset
                            </table>
                        </div>
                    </div>
                    <!--Pagination  -->
                    <div class="gk_pagination">
                        <ul>
                            @isset($documents)
                                {{ $documents->links() }}
                            @endisset
                        </ul>
                    </div>
                </div>
                <!--Sidebar  -->
                @include('frontend.sidebar')
                <!--End Sidebar  -->
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@stop
@section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop