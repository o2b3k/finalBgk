@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop
@section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop
@section('content')
<!-- CONTENT -->
<div class="gk_content gk_article">
    <div class="container default-body">
        <div class="row">
            <div class="col-lg-8">
                <div class="gk_link_menu-custom">
                    <a href="{{ route('index') }}">{{ trans('home.home') }}</a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Прямая трансляция
                </div>
                <div class="gk_article__content">
                    <div class="gk_article__content-heading">
                        <h2 class="heading_on">Прямая трансляция</h2>
                    </div>
                    <br>
                    <div class="gk_article__content-description">
                        <div class="gk_article__content-param">
                            <div class="link_live">
                                <iframe id="player" type="text/html" width="100%" height="400" src="http://www.youtube.com/embed/M7lc1UVf-VE?enablejsapi=1&origin=http://example.com"
                                    frameborder="0"></iframe>
                            </div>
                            <div class="theme">
                            <strong>На тему:</strong>
                                    <h6>Постоянная комиссия по местному самоуправлению и работе с общественными организациями</h6>
                            </div>
                        </div>
                        <br>
                        <!--End  -->
                    </div>
                </div>
            </div>
            <!--Sidebar  -->
            @include('frontend.sidebar')
            <!--End Sidebar  -->
        </div>
    </div>
</div>
<!-- END CONTENT -->
@stop @section('footer')
<!--FOOTER  -->
@include('frontend.footer')
<!--END FOOTER  -->
@stop