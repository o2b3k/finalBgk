<!--Sidebar  -->
<div class="col-lg-4">
    <aside class="gk_article__aside">
        <div class="gk_article__aside-heading">
            <h3>{{ trans('home.main_event') }}</h3>
        </div>
        @if(\App\Models\News::one($request) != false)
            <div class="gk_main_user">
                <a href="{{ route('viewNews',['slug' => \App\Models\News::one($request)->news->slug]) }}">
                    <div class="gk_main_user-img">
                        <img src="{{ asset(\App\Models\News::one($request)->news->image) }}" alt="">
                    </div>
                    <div class="gk_main_user-name">
                        <h3>{!! \App\Models\News::getTitle(\App\Models\News::one($request)->news->title) !!}</h3>
                    </div>
                    <div class="gk_article__aside_latest-date">
                        <span><i class="fa fa-calendar" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', \App\Models\News::one($request)->created_at)->toDateString() }}</span>
                    </div>
                </a>
            </div>
        @endif
        <div class="gk_article__aside-heading">
            <h3>{{ trans('home.latest_news') }}</h3>
        </div>
        @foreach(\App\Models\News::popularNews($request) as $news)
            <div class="gk_article__aside-latest">
                <div class="gk_article__aside_latest-heading">
                    <a href="{{ route('viewNews',['slug' => $news->slug]) }}">
                        <h4 class="h0">{!! \App\Models\News::getTitle($news->title) !!}</h4>
                    </a>
                </div>
                <div class="gk_article__aside_latest-desc">
                    <p>{!! \App\Models\News::getExcerpt($news->text) !!}</p>
                </div>
                <div class="gk_article__aside_latest-date">
                    <span><i class="fa fa-calendar" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $news->created_at)->toDateString() }}</span>
                </div>
            </div>
        @endforeach
    </aside>
</div>
<!--End Sidebar  -->