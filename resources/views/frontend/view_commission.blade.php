@extends('template.master') @section('menu')
    @include('frontend.main_menu')
@stop @section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop @section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container default-body">
            <div class="row">
                <div class="col-lg-8">
                    <div class="gk_link_menu-custom">
                        <a href="{{ route('index') }}">{{ trans('home.home') }}</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <a href="">{{ trans('home.commission') }}</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>Test
                    </div>
                    <div class="gk_profile">
                        <h2 class="heading_on">Test</h2>
                        @foreach($commission as $comm)
                            <article class="beefup">
                                <h2 class="beefup__head">2. Постоянная комиссия по бюджету и финансам</h2>
                                <div class="beefup__body">
                                    @isset($comm->chairman)
                                        <h3 class="gk_profile_main-user">Председатель</h3>
                                        <a href="{{ route('front.DeputyView',['deputy' => $comm->chairman]) }}">
                                            <div class="gk_profile-user">
                                                <div class="gk_profile-img_name">
                                                    <div class="gk_profile-user-img">
                                                        <img src="{{ asset($comm->chairman->image) }}" alt="">
                                                    </div>
                                                    <div class="gk_profile-user-info">
                                                        <h2>{{ $comm->chairman->fio }}</h2>
                                                        <p>{!! \App\Models\Deputy::getExcerpt($comm->chairman->profile) !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    @endisset
                                    @isset($comm->deputy)
                                        <h3 class="gk_profile_main-user">Заместитель</h3>
                                        <a href="{{ route('front.DeputyView',['id' => $comm->deputy]) }}">
                                            <div class="gk_profile-user">
                                                <div class="gk_profile-img_name">
                                                    <div class="gk_profile-user-img">
                                                        <img src="{{ asset($comm->deputy->image) }}" alt="">
                                                    </div>
                                                    <div class="gk_profile-user-info">
                                                        <h2>{{ $comm->deputy->fio }}</h2>
                                                        <p>{!! \App\Models\Deputy::getExcerpt($comm->deputy->profile) !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    @endisset
                                </div>
                            </article>
                        @endforeach
                        <div class="gk_legislation">
                            <div class="gk_legislation-table">

                            </div>
                        </div>
                        <!--Pagination  -->
                    </div>
                </div>
                <!--Sidebar  -->
                @include('frontend.sidebar')
                <!--End Sidebar  -->
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@stop
@section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop