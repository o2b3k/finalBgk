@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop
@section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop
@section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container default-body">
            <div class="row">
                <div class="col-lg-8">
                    <div class="gk_link_menu-custom">
                        <a href="{{ route('index') }}">{{ trans('home.home') }}</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i> <a href="{{ route('front.FactionView',['faction' => $deputy->faction]) }}">{{ $deputy->faction->name }}</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>{{ $deputy->fio }}
                    </div>
                    <div class="gk_profile">
                    <h2 class="heading_on">{{ $deputy->fio }}</h2>
                        <a href="{{ route('front.Deputy') }}" class="back"><i class="fa fa-angle-left mr-1" aria-hidden="true"></i>Вернуться</a>
                        <div class="gk_profile-user gk_details">
                            <div class="gk_profile-img_name">
                                <div class="gk_profile-user-img">
                                    <img src="{{ asset($deputy->image) }}" alt="">
                                </div>
                                <div class="gk_profile-user-info">
                                    <h2>{{ $deputy->fio }}</h2>
                                    <p>{!! $deputy->profile !!}</p>
                                    <br>
                                    <div class="gk_info-date">
                                        <div class="gk_top-item-2">
                                            <div class="gk_top_social-icon">
                                                <ul class="gk_top_sprite-ul" style="justify-content: flex-start; padding: 0">
                                                    @isset($deputy->fb)
                                                        <a href="{{ $deputy->fb }}">
                                                            <li style = "background: #4d6fa9">
                                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                            </li>
                                                        </a>
                                                    @endisset
                                                    @isset($deputy->ins)
                                                        <a href="{{ $deputy->ins}}">
                                                            <li style = "background: #00aced">
                                                                <i class="fa fa-instagram" aria-hidden="true"></i>
                                                            </li>
                                                        </a>
                                                    @endisset
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
                <!--Sidebar  -->
                @include('frontend.sidebar')
                <!--End Sidebar  -->
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@stop
@section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop