@extends('template.master') @section('menu')
    @include('frontend.main_menu')
@stop @section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop @section('content')
<!-- CONTENT -->
<div class="gk_content gk_article">
    <div class="container default-body">
        <div class="row">
            <div class="col-lg-8">
                <div class="gk_link_menu-custom">
                    <a href="{{ route('index') }}">{{ trans('home.home') }}</a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <a href="">{{ trans('home.gallery') }}</a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>{{ $album->name }}
                </div>
                <div class="gk_article__content">
                    <div class="gk_article__content-heading">
                        <h2 class="heading_on">{{ $album->name }}</h2>

                    </div>
                    <br>
                    <div class="gk_article__content-description">
                        <div class="fotorama" data-nav="thumbs"   data-allowfullscreen="native">
                            @isset($album->photos)
                                @foreach($album->photos as $photo)
                                    <img src="{{ asset($photo->path) }}">
                                @endforeach
                            @endisset
                        </div>
                    </div>
                </div>
                <br>
                <div class="gk_article__share">
                    <div>
                        <span class="gk_article_share_text">Поделиться:</span>
                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="//yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,linkedin,whatsapp,telegram"></div>
                    </div>
                    <div onclick="window.print()">
                        <i class="fa fa-print gk_print" aria-hidden="true"></i>Распечатать</div>
                </div>
            </div>
            <!--Sidebar  -->
            @include('frontend.sidebar')
            <!--End Sidebar  -->
        </div>
    </div>
</div>
<!-- END CONTENT -->
@stop @section('footer')
<!--FOOTER  -->
@include('frontend.footer')
<!--END FOOTER  -->
@stop