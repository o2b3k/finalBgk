<footer>
    <div class="gk__footer p__t gk_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="gk_logo__footer">
                        <img src="/img/logo.png" alt="">
                    </div>
                    <div class="gk_info__footer">
                        <span class="gk_item__footer">Телефон: +996 (312) 625814</span>
                        <span class="gk_item__footer">Email: bishkek.gorkenesh@gmail.com</span>
                        <span class="gk_item__footer">Адрес: Кыргызстан г. Бишкек проспект Чуй, 166</span>
                        <div class="gk_top-item-2">
                            <div class="gk_top_social-icon">
                                <ul class="gk_top_sprite-ul">
                                    <li><a href="https://www.facebook.com/bishkekgorkenesh/" target="_blank"><i style="color: white;" class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="https://www.instagram.com/gorkenesh/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 footer_center_links">
                    @foreach(\App\Models\News::footer($request) as $footer)
                        <div class="gk__good_links">
                            <h5>{{ $footer->name }}</h5>
                            <ul class="gk__good_links-ul">
                                @foreach($footer->link as $link)
                                    @if(strpos($link->url,"pages"))
                                        <li><a href="{{ route('front.pageView', ['slug' => \App\Models\Footer::getSlug($link->url)])  }}">{{ $link->name }}</a></li>
                                    @else
                                        <li><a href="{{ $link->url }}">{{ $link->name }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
                <div class="col-lg-5">
                    <div class="gk__good_links" style="width: 100%!important;">
                        <h5>Карта</h5>
                        <div class="gk_foter__map">
                            <a class="dg-widget-link" href="http://2gis.kg/bishkek/firm/70000001019367685/center/74.58674490451814,42.87509137028095/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Бишкека</a><div class="dg-widget-link"><a href="http://2gis.kg/bishkek/center/74.586587,42.876178/zoom/16/routeTab/rsType/bus/to/74.586587,42.876178╎Бишкекский городской Кенеш?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Бишкекский городской Кенеш</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":640,"height":600,"borderColor":"#a3a3a3","pos":{"lat":42.87509137028095,"lon":74.58674490451814,"zoom":16},"opt":{"city":"bishkek"},"org":[{"id":"70000001019367685"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;"></noscript>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gk__footer_copyright">
				<span class="gk__copyright">
																	Все права защищены © 2016 Бишкекский городской кенеш
																	При копировании материалов сайта ссылка на сайт обязательна
																</span>
        <a href="https://www.facebook.com/profile.php?id=100007692642938&ref=bookmarks" target="_blank">Разработка сайтов</a>
    </div>
</footer>