@extends('template.master') @section('menu')
    @include('frontend.main_menu')
@stop @section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop @section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container default-body">
            <div class="row">
                <div class="col-lg-8">
                    <div class="gk_link_menu-custom">
                        <a href="{{ route('index') }}">{{ trans('home.home') }}</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <a href="">{{ trans('home.commission') }}</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        @isset($category)
                            {{ $category->name }}
                        @endisset
                    </div>
                    <div class="gk_profile">
                        <h2 class="heading_on">
                            @isset($category)
                                {{ $category->name }}
                            @endisset
                        </h2>
                        @isset($commissions)
                            @foreach($commissions as $comm)
                                <article class="beefup">
                                    <h2 class="beefup__head">{{ $comm->name }}</h2>
                                    <div class="beefup__body">
                                        <table class="table">
                                            <tr>
                                                <th>Документы</th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                @isset($comm->position)
                                                    <td class="table_one">
                                                        <span class="gk_date">
                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $comm->position->created_at)->toDateString()}}
                                                        </span>
                                                        <p>
                                                            <a href="{{ route('front.DocView',['document' => $comm->position]) }}">{{ $comm->position->name }}</a>
                                                        </p>
                                                    </td>
                                                    <td class="gk_pdf">
                                                        <a href="{{ asset($comm->position->attachment) }}"
                                                           target="_blank">
                                                            <i class="ion-ios-list-outline" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                @endisset
                                            </tr>
                                            <tr>
                                                @isset($comm->plan)
                                                    <td class="table_one">
                                                        <span class="gk_date">
                                                            <i class="fa fa-calendar"
                                                               aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $comm->plan->created_at)->toDateString()}}
                                                        </span>
                                                        <p>
                                                            <a href="{{ route('front.DocView',['document' => $comm->plan]) }}">{{ $comm->plan->name }}</a>
                                                        </p>
                                                    </td>
                                                    <td class="gk_pdf">
                                                        <a href="{{ asset($comm->plan->attachment) }}" target="_blank">
                                                            <i class="ion-ios-list-outline" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                @endisset
                                            </tr>
                                            <tr>
                                                @isset($comm->reports)
                                                    <td class="table_one">
                                                <span class="gk_date">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $comm->reports->created_at)->toDateString()}}
                                                </span>
                                                        <p>
                                                            <a href="{{ route('front.DocView',['document' => $comm->reports]) }}">{{ $comm->reports->name }}</a>
                                                        </p>
                                                    </td>
                                                    <td class="gk_pdf">
                                                        <a href="{{ asset($comm->reports->attachment) }}"
                                                           target="_blank">
                                                            <i class="ion-ios-list-outline" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                @endisset
                                            </tr>
                                        </table>
                                        @isset($comm->chairman)
                                            <h3 class="gk_profile_main-user">Председатель</h3>
                                            <a href="{{ route('front.DeputyView',['deputy' => $comm->chairman->id]) }}">
                                                <div class="gk_profile-user">
                                                    <div class="gk_profile-img_name">
                                                        <div class="gk_profile-user-img">
                                                            <img src="{{ asset($comm->chairman->image) }}" alt="">
                                                        </div>
                                                        <div class="gk_profile-user-info">
                                                            <h2>{{ $comm->chairman->fio }}</h2>
                                                            <p>{!! \App\Models\Deputy::getExcerpt($comm->chairman->profile) !!}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        @endisset
                                        @isset($comm->deputy)
                                            <h3 class="gk_profile_main-user">Заместитель</h3>
                                            <a href="{{ route('front.DeputyView',['id' => $comm->deputy->id]) }}">
                                                <div class="gk_profile-user">
                                                    <div class="gk_profile-img_name">
                                                        <div class="gk_profile-user-img">
                                                            <img src="{{ asset($comm->deputy->image) }}" alt="">
                                                        </div>
                                                        <div class="gk_profile-user-info">
                                                            <h2>{{ $comm->deputy->fio }}</h2>
                                                            <p>{!! \App\Models\Deputy::getExcerpt($comm->deputy->profile) !!}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        @endisset
                                        @isset($comm->members)
                                                <h3 class="gk_profile_main-user">Члены комиссии</h3>
                                            @foreach($comm->members as $member)
                                                <a href="{{ route('front.DeputyView',['id' => $member->id]) }}">
                                                    <div class="gk_profile-user">
                                                        <div class="gk_profile-img_name">
                                                            <div class="gk_profile-user-img">
                                                                <img src="{{ asset($member->image) }}" alt="">
                                                            </div>
                                                            <div class="gk_profile-user-info">
                                                                <h2>{{ $member->fio }}</h2>
                                                                <p>{!! \App\Models\Deputy::getExcerpt($member->profile) !!}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            @endforeach
                                        @endisset
                                    </div>
                                </article>
                            @endforeach
                        @endisset
                        <div class="gk_legislation">
                            <div class="gk_legislation-table">

                            </div>
                        </div>
                        <!--Pagination  -->
                    </div>
                </div>
                <!--Sidebar  -->
            @include('frontend.sidebar')
            <!--End Sidebar  -->
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@stop @section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop