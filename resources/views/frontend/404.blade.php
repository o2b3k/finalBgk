@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop
@section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop
@section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container ">
            <div class="row">
                <div class="col-lg-12">
                    <div class="gk_article__content">
                        <div class="gk_article__content-description" style="width: 100%; text-align: center">
                            <span class="big"><font color="#0098DA"> ОШИБКА 404: </font>Страница не найдено</span>
                            <p>Возможно страницу, которую вы искали была удалена</p>
                            <style>
                                .big{
                                    font-size: 2.5rem;
                                    font-weight: 600;
                                    width: 100%;
                                    text-align: center
                                }
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@stop
@section('footer')
    <!--FOOTER  -->

    <!--END FOOTER  -->
@stop