<div class="gk_phone">
    <div class="container">
        <div class="row">
            <div class="gk_nav-phone">
                <ul class="gk_nav__phone-ul">
                    @foreach(\App\Models\News::getMenu($request) as $menu)
                        <li class="gk_nav__phone_ul-li">
                            @if($menu->model)
                                <a href="{{ route('front.Menu',['model' => $menu->model,'model_id' => $menu->model_id]) }}">{{ $menu->name }}</a>
                            @else
                                @if(!$menu->children)
                                    <a href="{{ $menu->url }}">{{ $menu->name }}</a>
                                @else
                                    <a href="javascript:void(0)">{{ $menu->name }}</a>
                                @endif
                            @endif @isset($menu->children)
                                <ul class="phone_submenu">
                                    @foreach($menu->children as $menu)
                                        <li>
                                            @if($menu->url != null)
                                                <a href="{{ $menu->url }}"
                                                   class="phone_link">{{ $menu->name }}</a>
                                            @else
                                                <a href="{{ route('front.Menu',['model' => $menu->model,'model_id' => $menu->model_id]) }}"
                                                   class="phone_link">{{ $menu->name }}</a>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endisset
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>