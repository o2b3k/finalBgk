<nav>
    <div class="container gk_nav fl">
        <div class="row">
            <div class="gk_nav-menu">
                <div class="gk_burger-menu">
                    Меню
                </div>
                <ul class="ul_main_menu">
                    @foreach(\App\Models\News::getMenu($request) as $menu)
                        <li class="gk_nav_menu-li">
                            @if($menu->model)
                                <a href="{{ route('front.Menu',['model' => $menu->model,'model_id' => $menu->model_id]) }}"
                                   class="menu-link">{{ $menu->name }}</a>
                            @else
                                <a href="{{ $menu->url }}" class="menu-link">{{ $menu->name }}</a>
                            @endif @isset($menu->children)
                                <ul class="gk_submenu">
                                    @foreach($menu->children as $menu)
                                        <li class="gk_submenu-li">
                                            @if($menu->url != null)
                                                <a href="{{ $menu->url }}"
                                                   class="gk_submenu-link">{{ $menu->name }}</a>
                                            @else
                                                <a href="{{ route('front.Menu',['model' => $menu->model,'model_id' => $menu->model_id]) }}"
                                                   class="gk_submenu-link">{{ $menu->name }}</a>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endisset
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</nav>