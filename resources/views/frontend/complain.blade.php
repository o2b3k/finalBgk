@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop
@section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop
@section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container default-body">
            <div class="row">
                <div class="col-lg-8">
                    <!-- Обращение граждан -->
                    <div class="gk_callback">
                        <div class="gk_subitem__block">
                            <div class="gk_subitem__heading">
                                <h4>{{ trans('home.complain') }}</h4>
                                <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                            </div>
                            <div class="gk_callback_content">
                                <div class="gk_call_info">
                                    <p>{{ trans('home.info_complain') }}</p>
                                </div>
                                <form action="{{ route('front.ComplainStore') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="gk_form__group">
                                        <label for="fio">ФИО *</label>
                                        <input type="text" name="fio" placeholder="Введие ФИО" required>
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="email">Email *</label>
                                        <input type="email" name="email" placeholder="your@example.com" required>
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="phone">Телефон</label>
                                        <input type="tel" name="tel" placeholder="+996 XXX XXX XXX">
                                    </div>
                                    <div class="gk_form_group">
                                        <label for="deputy_id">Кому *</label>
                                        <select name="deputy_id">
                                            @isset($factions)
                                                @foreach($factions as $faction)
                                                    <option value="{{ $faction->id }}" disabled>{{ $faction->name }}</option>
                                                    @isset($faction->deputy)
                                                        <option value="" disabled>Руководитель</option>
                                                        <option value="{{ $faction->deputy->id }}">
                                                            <span>—</span>{{ $faction->deputy->fio }}</option>
                                                    @endisset
                                                    <option value="" disabled>Члены фракции</option>
                                                    @foreach($faction->members as $deputy)
                                                        <option value="{{ $deputy->id }}">
                                                            <span>— </span>{{ $deputy->fio }}</option>
                                                    @endforeach @endforeach @endisset
                                        </select>
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="subject">Файл</label>
                                        <input type="file" name="file">
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="subject">Тема *</label>
                                        <input type="text" name="subject" placeholder="Ваш вопрос" required>
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="text">Сообщение *</label>
                                        <textarea name="text" id="" cols="30" rows="10" placeholder=". . ."></textarea>
                                    </div>
                                    <div class="gk_form__group">
                                        {!! NoCaptcha::display() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <button type="submit" class="submit">Отправить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Sidebar  -->
                @include('frontend.sidebar')
                <!--End Sidebar  -->
            </div>
        </div>
    </div>
@stop
@section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush