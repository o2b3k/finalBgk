@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop
@section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop
@section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container ">
            <div class="row">
                <div class="col-lg-8">
                    <div class="gk_link_menu-custom">
                        <a href="#">Главная</a> <i class="fa fa-angle-right" aria-hidden="true"></i> Проекты постановлений
                        БГК
                    </div>
                    <!--List legislation  -->
                    <div class="gk_legislation">
                        <h2 class="heading_on">Проекты постановлений Бишкексокго Городского Кенеша
                        </h2>
                        <div class="gk_legislation-table">
                            <table class="table">
                                <tr>
                                    <th>Проекты постановлений БГК</th>
                                    <th>Файл</th>
                                </tr>
                                @foreach($categories as $category)
                                    <tr>
                                        <td><h2>{{ $category->name }}</h2></td>
                                        <td></td>
                                    </tr>
                                    @isset($category->decree)
                                        @foreach($category->decree as $document)
                                            <tr>
                                                <td class="table_one">
                                                        <span class="gk_date">
                                                            <i class="fa fa-calendar" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $document->created_at)->toDateString() }}
                                                        </span>
                                                    <p><a href="{{ route('front.DecreeView',['id' => $document->id]) }}">{{ $document->name }}</a></p>
                                                </td>
                                                <td class="gk_pdf">
                                                    <a href="{{ asset($document->file) }}" target="_blank"><i class="ion-ios-list-outline" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endisset
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <!--Sidebar  -->
                @include('frontend.sidebar')
                <!--End Sidebar  -->
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@stop
@section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop