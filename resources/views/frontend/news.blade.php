@extends('template.master')
@section('menu')
    @include('frontend.main_menu')
@stop
@section('phone')
    <!-- Phone Menu -->
    @include('frontend.phone_menu')
    <!-- End Phone menu -->
@stop
@section('content')
    <!-- CONTENT -->
    <div class="gk_content gk_article">
        <div class="container default-body">
            <div class="row">
                <div class="col-lg-8">
                    <div class="gk_link_menu-custom">
                        <a href="{{ route('index') }}">{{ trans('home.home') }}</a><i class="fa fa-angle-right" aria-hidden="true"></i>{{ trans('home.news') }}
                    </div>
                    <div class="gk_news">
                        <h2 class="heading_on">
                            @isset($news[0]->category)
                                {{ $news[0]->category->name }}
                            @endisset
                        </h2>
                            @isset($news)
                                @foreach($news as $new)
                                    <div class="gk_news_article">
                                        <div class="gk_news_img">
                                            <img src="{{ asset($new->image) }}" class="img-responsive mr-1" alt="">
                                        </div>
                                        <div class="gk_news_text_article ml-1">
                                        <div class="gk_news_group_article">
                                            <a href="{{ route('viewNews',['slug' => $new->slug]) }}" target="_blank">
                                                <h5>{!! \App\Models\News::getExcerpt($new->title) !!}</h5>
                                            </a>
                                            <span class="gk_date">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new->created_at)->toDateString() }}
                                            </span>
                                        </div>
                                        <div>
                                        <p>Категория: @isset($new->category)
                                            <a href="{{ route('front.CategoryNews',['id' => $new->category->id]) }}">{{ $new->category->name }}</a>@endisset</p>
                                        <p>{!! \App\Models\News::getExcerpt($new->text) !!}</p>
                                        </div>
                                        <a href="{{ route('viewNews',['slug' => $new->slug]) }}" class="read_more">{{ trans('home.read_more') }}</a>
                                    </div>
                                    </div>
                                @endforeach
                            @endisset
                    </div>
                    <!--Pagination  -->
                    <div class="gk_pagination">
                        <ul>
                            {{ $news->links() }}
                        </ul>
                    </div>
                </div>
                <!--Sidebar  -->
                @include('frontend.sidebar')
                <!--End Sidebar  -->
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@stop
@section('footer')
    <!--FOOTER  -->
    @include('frontend.footer')
    <!--END FOOTER  -->
@stop