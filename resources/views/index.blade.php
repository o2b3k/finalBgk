<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <title>Бишкекский Городской Кенеш</title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="/img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-touch-icon-114x114.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <style>
        body {
            opacity: 0;
            overflow-x: hidden;
        }

        html {
            background-color: #fff;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
<div class="gk_wrapper_site">
    <!-- TOP SYSTEM MENU -->
    <div class="gk_top-system-menu">
        <div class="container">
            <div class="row">
                <ul class="gk_top_flags">
                    <li class="gk_top_flags-li">
                        <img src="/img/kg.png" class="img-fluid" alt="">
                        <a href="#" class="gk_top_flags-link">КЫРГ</a>
                    </li>
                    <li class="gk_top_flags-li">
                        <img src="/img/ru.png" class="img-fluid" alt="">
                        <a href="#" class="gk_top_flags-link">РУС</a>
                    </li>
                    <li class="gk_top_flags-li">
                        <img src="/img/us.png" class="img-fluid" alt="">
                        <a href="#" class="gk_top_flags-link">ENG</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END SYSTEM MENU -->

    <!-- HEADER -->
    <div class="gk_top-header ">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="gk_top-item">
                        <div class="gk_top-logo">
                            <a href="#"><img src="/img/logo.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="gk_top-text">
                            <h1><span>Бишкекский</span> городской кенеш</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="gk_top-item-2">
                        <div class="gk_top_social-icon">
                            <ul class="gk_top_sprite-ul">
                                <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                <li><i class="fa fa-odnoklassniki" aria-hidden="true"></i></li>
                                <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="gk_top_search">
                            <input type="search" placeholder="Поиск"><button class="search_sub" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav>
            <div class="container gk_nav fl">
                <div class="row">
                    <div class="gk_nav-menu">
                        <div class="gk_burger-menu">
                            Меню
                        </div>
                        <ul>
                            <li class="gk_nav_menu-li"><a href="index.html" class="menu-link">Главная</a></li>
                            <li class="gk_nav_menu-li"><a href="#" class="menu-link">Депутаты</a></li>
                                <ul class="gk_submenu">
                                    <li class="gk_submenu-li"><a href="profile.html" class="gk_submenu-link">СДПК</a></li>
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Республика Ата-Журт</a></li>
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Өнүгүү-Прогресс</a></li>
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Мекеним Кыргызстан</a></li>
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Кыргызcтан</a></li>
                                </ul>
                            </li>
                            <li class="gk_nav_menu-li gk_nav_menu-active"><a href="#" class="menu-link">Аппарат БГК </i></a>
                                <ul class="gk_submenu">
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Конституция</a></li>
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Регламент</a></li>
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">План работы</a></li>
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Отделы</a></li>
                                </ul>
                            </li>

                            <li class="gk_nav_menu-li"><a href="#" class="menu-link">Комиссии </i></a>
                                <ul class="gk_submenu">
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Постоянные комиссии</a></li>
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Другие комиссии</a></li>
                                </ul>
                            </li>
                            <li class="gk_nav_menu-li gk_nav_menu-active"><a href="#" class="menu-link active_menu">Обращение граждан </a>
                            </li>
                            <li class="gk_nav_menu-li"><a href="#" class="menu-link">Постановления</a>
                                <ul class="gk_submenu">
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Проекты постановления</a></li>
                                    <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Принятые постановления</a></li>
                                </ul>
                            </li>
                            <li class="gk_nav_menu-li"><a href="#" class="menu-link">Галерея</a></li>
                            <li class="gk_nav_menu-li live"><a href="#" class="menu-link">Прямая трансляция</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!--END HEADER  -->
    <!-- Phone Menu -->
    <div class="gk_phone">
        <div class="container">
            <div class="row">
                <div class="gk_nav-phone">
                    <ul class="gk_nav__phone-ul">
                        <li class="gk_nav__phone_ul-li"><a href="#">Главная</a></li>
                        <li class="gk_nav__phone_ul-li"><a href="#">Аппарат БГК</a></li>
                        <li class="gk_nav__phone_ul-li"><a href="#">Депутаты</a>
                            <ul class="phone_submenu">
                                <li>
                                    <a href="#" class="phone_link">СДПК</a>
                                </li>
                                <li>
                                    <a href="#" class="phone_link">СДПК</a>
                                </li>
                                <li>
                                    <a href="#" class="phone_link">СДПК</a>
                                </li>
                                <li>
                                    <a href="#" class="phone_link">СДПК</a>
                                </li>
                            </ul>
                        </li>
                        <li class="gk_nav__phone_ul-li"><a href="#">Комиссии</a></li>
                        <li class="gk_nav__phone_ul-li"><a href="#">Обращение граждан</a></li>
                        <li class="gk_nav__phone_ul-li"><a href="#">Постановления</a></li>
                        <li class="gk_nav__phone_ul-li"><a href="#">Галерея</a></li>
                        <li class="gk_nav__phone_ul-li"><a href="#">Прямая трансляция</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Phone menu -->

    <!-- CONTENT -->
    <div class="gk_content">
        <div class="gk_content-news-module">
            <div class="container">
                <div class="row">
                    <div class="gk_content_large">
                        <div class="gk_content_first">
                            <img src="/img/b1.jpg" class="gk_main_bg img-responsive" alt="">
                            <div class="gk_content_text">
                                <div>
                                    <div class="gk_content_Date__heading">
											<span class="gk_date">
												<i class="fa fa-calendar" aria-hidden="true"></i> 12/11/2017
											</span>
                                        <a href="#">
                                            <h2>Значимость этих проблем настолько очевидна, что постоянное информационно-пропагандистское обеспечение нашей деятельности
                                                играет важную роль этих проблем настолько очевидна...</h2>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gk_content_second">
                            <div class="gk_img">
                                <img src="/img/b2.jpg" class="gk_second_bg img-responsive" alt="">
                                <div class="gk_content_text">

                                    <div class="gk_content_Date__heading">
											<span class="gk_date">
													<i class="fa fa-calendar" aria-hidden="true"></i> 12/11/2017
												</span>
                                        <a href="article.html">
                                            <h2> Значимость этих проблем настолько очевидна, что постоянное..</h2>
                                        </a>
                                    </div>

                                </div>
                            </div>
                            <div class="gk_img">
                                <img src="/img/b3.jpg" class="gk_second_bg img-responsive" alt="">
                                <div class="gk_content_text">
                                    <div class="gk_content_Date__heading">
											<span class="gk_date">
													<i class="fa fa-calendar" aria-hidden="true"></i> 12/11/2017
												</span>
                                        <a href="#">
                                            <h2>Значимость этих проблем настолько очевидна, что постоянное информационно...</h2>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gk_item__block">
            <div class="container">
                <div class="row">
                    <div class="gk_grid_massonry">
                        <!-- Новости -->
                        <div class="gk_subitem__block">
                            <div class="gk_subitem__heading">
                                <h4>Новости</h4>
                                <span><i class="fa fa-list" aria-hidden="true"></i></span>
                            </div>
                            <div class="gk_subitem__article">
                                @foreach($news as $new)
                                        <div class="gk__article">
                                            <img src="{{ asset($new->image) }}" class="img-responsive mr-1" alt="">
                                            <div class="gk_subcontent">
                                                <div class="gk_group_article">
                                                    <h5><a href="#">{{ $new->title }} </a></h5>
                                                    <span class="gk_date">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>{{ $new->updated_at }}
                                                    </span>
                                                </div>
                                                <p>{{ strip_tags(substr($new->text,0,250)) }}</p>
                                                <a href="#" class="read_more">Подробнее</a>
                                            </div>
                                        </div>
                                @endforeach
                                <div class="gk_view__all">
                                        <a href="news.html">Все новости</a>
                                    </div>
                            </div>
                        </div>
                        <!-- Ссылки на стронние ресурсы -->
                        <div class="gk_subitem__block">
                            <div class="gk_subitem__heading">
                                <h4>Ссылки на стронние ресурсы</h4>
                                <span><i class="fa fa-external-link" aria-hidden="true"></i></span>
                            </div>
                            <a href="#" target="_blank">
                                <div class="gk_link__item_block">
                                    <div class="gk_link_Text">
                                        <div class="gk_img_flex_text">
                                            <div class="gk_link_banner">
                                                <img src="/img/banner3.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="gk_flex_text">
                                                <h5>Официальный сайт президента кыргызской республики</h5>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                            <a href="#" target="_blank">
                                <div class="gk_link__item_block">
                                    <div class="gk_link_Text">
                                        <div class="gk_img_flex_text">
                                            <div class="gk_link_banner">
                                                <img src="/img/banner3.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="gk_flex_text">
                                                <h5>Официальный сайт жогорку кенеша </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="#" target="_blank">
                                <div class="gk_link__item_block">
                                    <div class="gk_link_Text">
                                        <div class="gk_img_flex_text">
                                            <div class="gk_link_banner">
                                                <img src="/img/banner3.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="gk_flex_text">
                                                <h5>Правительство кыргызской республики</h5>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                            <a href="#" target="_blank">
                                <div class="gk_link__item_block">
                                    <div class="gk_link_Text">
                                        <div class="gk_img_flex_text">
                                            <div class="gk_link_banner">
                                                <img src="/img/banner3.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="gk_flex_text">
                                                <h5>Мэрия города Бишкек</h5>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Законы и законопроекты -->
                        <div class="gk_subitem__block">
                            <div class="gk_subitem__heading">
                                <h4>Проекты постановлений БГК</h4>
                                <span><i class="fa fa-file-text-o" aria-hidden="true"></i></span>
                            </div>
                            <div class="gk_subitem__article">
                                <div class="gk__article">
                                    <div class="gk_subcontent">
                                        <div class="gk_group_article">
                                                <span class="gk_date">
                                                                        <i class="fa fa-calendar" aria-hidden="true"></i> 12/11/2017
                                                                    </span>
                                            <a href="#">
                                                <h5>Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс
                                                    внедрения и модернизации направлений прогрессивного развития. </h5>
                                            </a>

                                        </div>
                                    </div>
                                </div>
                                <div class="gk__article">
                                    <div class="gk_subcontent">
                                        <div class="gk_group_article">
                                                <span class="gk_date">
                                                                            <i class="fa fa-calendar" aria-hidden="true"></i> 12/11/2017
                                                                        </span>
                                            <a href="#">
                                                <h5>Не следует, однако забывать, что консультация с широким активом обеспечивает широкому кругу (специалистов)
                                                    участие в формировании новых предложений.</h5>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="gk__article">
                                    <div class="gk_subcontent">
                                        <div class="gk_group_article">
                                                <span class="gk_date">
                                                                                <i class="fa fa-calendar" aria-hidden="true"></i> 12/11/2017
                                                                            </span>
                                            <a href="#">
                                                <h5>Задача организации, в особенности же консультация с широким активом позволяет оценить значение направлений
                                                    прогрессивного развития. Повседневная практика показывает, что начало повседневной работы.</h5>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="gk__article">
                                    <div class="gk_subcontent">
                                        <div class="gk_group_article">
                                                <span class="gk_date">
                                                                                    <i class="fa fa-calendar" aria-hidden="true"></i> 12/11/2017
                                                                                </span>
                                            <a href="#">
                                                <h5>Идейные соображения высшего порядка, а также рамки и место обучения кадров представляет собой интересный эксперимент
                                                    проверки форм развития. Идейные соображения высшего порядка. </h5>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="gk__article">
                                    <div class="gk_subcontent">
                                        <div class="gk_group_article">
                                                <span class="gk_date">
                                                                                        <i class="fa fa-calendar" aria-hidden="true"></i> 12/11/2017
                                                                                    </span>
                                            <a href="#">
                                                <h5>Таким образом начало повседневной работы по формированию позиции влечет за собой процесс внедрения и модернизации
                                                    дальнейших направлений развития. Таким образом консультация.</h5>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="gk__article">
                                    <div class="gk_subcontent">
                                        <div class="gk_group_article">
                                                <span class="gk_date">
                                                                                            <i class="fa fa-calendar" aria-hidden="true"></i> 12/11/2017
                                                                                        </span>
                                            <a href="#">
                                                <h5>Значимость этих проблем настолько очевидна, что укрепление и развитие структуры в значительной степени обуславливает
                                                    создание существенных финансовых и административных условий.</h5>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="gk_view__all">
                                    <a href="legislation.html">Показать все</a>
                                </div>
                            </div>
                        </div>
                        <div class="gk_zakony">
                           
                        </div>
                        <!-- Обращение граждан -->
                        <div class="gk_subitem__block">
                            <div class="gk_subitem__heading">
                                <h4>Обращение граждан</h4>
                                <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                            </div>
                            <div class="gk_callback_content">
                                <div class="gk_call_info">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus unde ducimus illo debitis iusto totam rerum
                                        incidunt doloremque ipsam minus, sint. Optio excepturi impedit fugit?</p>
                                </div>
                                <form action="{{ route('complain.Store') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="gk_form__group">
                                        <label for="fio">ФИО *</label>
                                        <input type="text" name="fio" placeholder="Введие ФИО" required="">
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="email">Email *</label>
                                        <input type="text" name="email" placeholder="your@example.com" required="">
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="phone">Телефон</label>
                                        <input type="tel" name="tel" placeholder="+996 XXX XXX XXX">
                                    </div>
                                    <div class="gk_form_group">
                                        <label for="deputy_id">Кому *</label>
                                        <select name="deputy_id">
                                            @foreach($deputies as $deputy)
                                                <option value="{{ $deputy->id }}">{{ $deputy->fio }} - {{ $deputy->faction->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="subject">Файл</label>
                                        <input type="file" name="file">
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="subject">Тема *</label>
                                        <input type="text" name="subject" placeholder="Ваш вопрос" required="">
                                    </div>
                                    <div class="gk_form__group">
                                        <label for="text">Сообщение *</label>
                                        <textarea name="text" id="" cols="30" rows="10" placeholder=". . ."></textarea>
                                    </div>
                                    <button type="submit" class="submit">Отправить</button>
                                </form>
                            </div>
                        </div>
                        <div class="gk_callback">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
    <!-- END CONTENT -->

    <!--FOOTER  -->
    <footer>
        <div class="gk__footer p__t">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="gk_logo__footer">
                            <img src="/img/logo.png" alt="">
                        </div>
                        <div class="gk_info__footer">
                            <span class="gk_item__footer">Телефон: +996 777 054 723</span>
                            <span class="gk_item__footer">Email: your@example.com</span>
                            <span class="gk_item__footer">Адрес: Кыргызстан г. Бишкек 720039</span>
                            <div class="gk_top-item-2">
                                <div class="gk_top_social-icon">
                                    <ul class="gk_top_sprite-ul">
                                        <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-odnoklassniki" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="gk__good_links">
                            <h5>Полезные ссылки</h5>
                            <ul class="gk__good_links-ul">
                                <li><a href="#">Вакансии</a></li>
                                <li><a href="#">Почетные граждане</a></li>
                                <li><a href="#">Международные связи</a></li>
                            </ul>
                        </div>
                        <br>
                        <div class="gk__good_links">
                            <h5>Полезные ссылки</h5>
                            <ul class="gk__good_links-ul">
                                <li><a href="#">Вакансии</a></li>
                                <li><a href="#">Почетные граждане</a></li>
                                <li><a href="#">Международные связи</a></li>
                                <li><a href="#">Вакансии</a></li>
                                <li><a href="#">Почетные граждане</a></li>
                                <li><a href="#">Международные связи</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="gk__good_links">
                            <h5>Полезные ссылки</h5>
                            <ul class="gk__good_links-ul">
                                <li><a href="#">Тазалык</a></li>
                                <li><a href="#">Зеленострой</a></li>
                                <li><a href="#">Госагентсво архитектуры</a></li>
                            </ul>
                        </div>
                        <br>
                        <div class="gk__good_links">
                            <h5>Полезные ссылки</h5>
                            <ul class="gk__good_links-ul">
                                <li><a href="#">Тазалык</a></li>
                                <li><a href="#">Зеленострой</a></li>
                                <li><a href="#">Госагентсво архитектуры</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="gk__good_links">
                            <h5>Карта</h5>
                            <div class="gk_foter__map">
                            <a class="dg-widget-link" href="http://2gis.kg/bishkek/firm/70000001019367685/center/74.58674490451814,42.87509137028095/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Бишкека</a><div class="dg-widget-link"><a href="http://2gis.kg/bishkek/center/74.586587,42.876178/zoom/16/routeTab/rsType/bus/to/74.586587,42.876178╎Бишкекский городской Кенеш?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Бишкекский городской Кенеш</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":640,"height":600,"borderColor":"#a3a3a3","pos":{"lat":42.87509137028095,"lon":74.58674490451814,"zoom":16},"opt":{"city":"bishkek"},"org":[{"id":"70000001019367685"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;"></noscript>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gk__footer_copyright">
				<span class="gk__copyright">
																	Все права защищены © 2016 Бишкекский городской кенеш
																	При копировании материалов сайта ссылка на сайт обязательна
																</span>
            <a href="https://www.facebook.com/profile.php?id=100007692642938&ref=bookmarks" target="_blank">Разработка сайтов</a>
        </div>
    </footer>
    <!--END FOOTER  -->
</div>

<link rel="stylesheet" href="{{ asset('css/main.min.css') }}">
<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('libs/normalize.css') }}">
<link rel="stylesheet" href="{{ asset('libs/remodal/remodal.css') }}">
<link rel="stylesheet" href="{{ asset('libs/remodal/remodal-default-theme.css') }}">
<script src="{{ elixir('js/app.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
<script src="{{ asset('libs/remodal/remodal.js') }}"></script>
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script src="{{ asset('js/tos.js') }}"></script>
@if (session('status'))
    <?php $status = session('status'); ?>
    <script>
        @if (isset($status['title']))
        toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
        @else (isset($status['title']) || isset())
        toastr.{{ $status['type'] }}('{{ $status['message'] }}');
        @endif
    </script>
@endif
</body>

</html>