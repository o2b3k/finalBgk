@extends('main')
@section('page_header')
    <h1 class="page-title">Документы</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('decree.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Наименование</th>
                        <th>Категория</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($documents as $decree)
                        <tr>
                            <td>{{ $decree->id }}</td>
                            <td>{{ $decree->name }}</td>
                            <td>
                                @isset($decree->category)
                                    {{ $decree->category->name }}
                                @endisset
                            </td>
                            <td>
                                <a href="{{ route('decree.Edit',['decree' => $decree]) }}" class="btn btn-icon btn-success">
                                    <i class="icon md-edit"></i>
                                </a>
                                <button type="button" data-id="{{ $decree->id }}" class="btn btn-icon btn-danger btn-delete-decree">
                                    <i class="icon md-delete" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $documents->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-decree-form" class="d-none" action="{{ route('decree.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="decree_id" name="decree_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-decree.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush