@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ isset($category) ? route('resolution.Update',['id' => $category->id]) : route('resolution.Store') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Имя</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name', isset($category) ? $category->name : '') }}" required>
                            <span class="text-help">Наименование категория</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <label for="lang">Язык</label>
                            <select name="lang" id="lang" class="form-control">
                                @isset($category)
                                    <option value="{{ $category->lang }}">
                                            @if($category->lang == 'ru')
                                                Русский
                                            @elseif($category->lang == 'ky')
                                                Кыргызча
                                            @elseif($category->lang == 'en')
                                                English
                                            @endif
                                    </option>
                                @endisset
                                @foreach(\App\Models\Decree_Category::getLanguage(true) as $lang => $name)
                                    <option value="{{ $lang }}"{{ old('lang') == $lang ? ' selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('lang'))
                                <span class="has-error">{{ $errors->first('lang') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('resolution.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Добавить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush