@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('news.Store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12 col-ld-12">
                        <div class="form-group">
                            <h4 class="example-title">Заголовок *</h4>
                            <input type="text" name="title" class="form-control"
                                   id="inputHelpText" value="{{ old('title') }}" required>
                            @if($errors->has('title'))
                                <span class="has-error">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <label for="image">Изоброжения</label>
                            <input type="file" name="image" id="input-file-events"
                                   class="dropify-event form-control"/>
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <label for="parent">Категория *</label>
                            <select name="category_id" id="parent" class="form-control">
                                @foreach($categories as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('category_id'))
                                <span class="has-error">{{ $errors->first('category_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4">
                        <div class="form-group">
                            <label for="lang">Язык *</label>
                            <select name="lang" id="lang" class="form-control boxed">
                                @foreach(\App\Models\News::getLanguage(true) as $lang => $name)
                                    <option value="{{ $lang }}"{{ old('lang') == $lang ? ' selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('lang'))
                                <span class="has-error">{{ $errors->first('lang') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('text') ? 'has-error' : '' }}">
                            <h4 class="example-title">Текст *</h4>
                            <textarea name="text" class="form-control my-editor"></textarea>
                            @if($errors->has('text'))
                                <span class="has-error">{{ $errors->first('text') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Дата публикация</label>
                            <input type="date" name="created_at" class="form-control" value="{{ date('Y-m-d') }}">
                        </div>
                    </div>
                </div>
                @if($lang == "ru")
                    <a href="{{ route('news.Ru') }}" class="btn btn-danger float-sm-left">Назад</a>
                @else
                    <a href="{{ route('news.Ky') }}" class="btn btn-danger float-sm-left">Назад</a>
                @endif
                <button type="submit" class="btn btn-success float-sm-right">Добавить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            language_url : '/js/ru.js',
            selector: "textarea.my-editor",
            height : "250",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            },
        };
        tinymce.init(editor_config);
    </script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush