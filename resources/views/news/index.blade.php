@extends('main')
@section('page_header')
    <h1 class="page-title">Новости</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('news.Create',['lang' => $lang]) }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Наименование</th>
                        <th>Ссылка</th>
                        <th>№</th>
                        <th>Опубликован</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($news as $new)
                        <tr>
                            <td>{{ $new->id }}</td>
                            <td>{{ $new->title }}</td>
                            <td>{{ $new->slug }}</td>
                            <td>
                                @isset($new->position)
                                    @if($new->position->position == 'main_news')
                                        1
                                        @elseif($new->position->position == 'second_news')
                                        2
                                        @elseif($new->position->position == 'third_news')
                                        3
                                    @endif
                                @endisset
                            </td>
                            <td class="w-50">
                                <div class="checkbox-custom checkbox-primary">
                                    <input type="checkbox" class="published" data-id="{{ $new->id }}"
                                           @if($new->published) checked @endif/>
                                    <label for=""></label>
                                </div>
                            </td>
                            <td>
                                <a href="{{ route('news.Upload',['id' => $new->id]) }}" class="btn btn-icon btn-info">
                                    <i class="icon md-collection-folder-image"></i>
                                </a>
                                <a href="{{ route('news.Edit',['id' => $new->id ]) }}" class="btn btn-icon btn-success">
                                    <i class="icon md-edit"></i>
                                </a>
                                <button type="button" data-id="{{ $new->id }}" class="btn btn-icon btn-danger btn-delete-news">
                                    <i class="icon md-delete" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $news->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-news-form" class="d-none" action="{{ route('news.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="news_id" name="news_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-news.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            $('.published').on('click', function(event){
                let id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('news.setPublished') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
            $('.archive').on('click', function (event) {
                let id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('news.setArchive') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function (data) {
                        //empty
                    },
                });
            });
        })
    </script>
@endpush