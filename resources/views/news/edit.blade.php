@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('news.Update',['id' => $news->id]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12 col-ld-12">
                        <div class="form-group">
                            <h4 class="example-title">Заголовок</h4>
                            <input type="text" name="title" class="form-control"
                                   id="inputHelpText" value="{{ old('title', $news->title) }}">
                            <span class="text-help">Заголовок новости</span>
                            @if($errors->has('title'))
                                <span class="has-error">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 ">
                        <div class="form-group{{ $errors->has('text') ? 'has-error' : '' }}">
                            <h4 class="example-title">Текст</h4>
                            <textarea name="text" class="form-control my-editor">
                                {!! $news->text !!}
                            </textarea>
                            @if($errors->has('text'))
                                <span class="has-error">{{ $errors->first('text') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <h4 class="example-title">Изоброжения</h4>
                            <img src="{{ asset($news->image) }}" alt="" class="img-fluid w-full">
                            @if($errors->has('image'))
                                <span class="has-error">{{ $errors->first('image') }}</span>
                            @endif
                            <input type="file" name="image" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <label for="parent">Категория</label>
                            <select name="category_id" id="parent" class="form-control">
                                @isset($news->category)
                                    <option value="{{ $news->category->id }}">{{ $news->category->name }}</option>
                                @endisset
                                @foreach($categories as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4">
                        <div class="form-group">
                            <label for="lang">Язык</label>
                            <select name="lang" id="lang" class="form-control boxed">
                                    <option value="{{ $news->lang }}">
                                        @if($news->lang == 'ky')
                                            Кыргызча
                                        @elseif($news->lang == 'ru')
                                            Русский
                                        @elseif($news->lang == 'en')
                                            English
                                        @endif
                                    </option>
                                @foreach(\App\Models\News::getLanguage(true) as $lang => $name)
                                    <option value="{{ $lang }}"{{ old('lang') == $lang ? ' selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('lang'))
                                <span class="has-error">{{ $errors->first('lang') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-2 col-ld-2">
                        <div class="form-group">
                            <label for="created_at">Дата публикация</label>
                            <input type="date" name="created_at" class="form-control" value="<?php echo date_format($news->created_at,'Y-m-d') ?>">
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <label for="parent">Главная</label>
                            <select name="home" id="parent" class="form-control">
                                @if(isset($news->position))
                                    <option value="{{ $news->position->position }}">
                                        @if($news->position->position == "main_news")
                                            1
                                        @elseif($news->position->position == "second_news")
                                            2
                                        @else
                                            3
                                        @endif
                                    </option>
                                @else
                                    <option value="null"></option>
                                @endif
                                @foreach(\App\Models\Position::getType(true) as $type => $name)
                                    <option value="{{ $type }}"{{ old('type') == $type ? ' selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                @if($news->lang == "ru")
                    <a href="{{ route('news.Ru') }}" class="btn btn-danger float-sm-left">Назад</a>
                @else
                    <a href="{{ route('news.Ky') }}" class="btn btn-danger float-sm-left">Назад</a>
                @endif
                <button type="submit" class="btn btn-success float-sm-right">Изменить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            language_url : '/js/ru.js',
            selector: "textarea.my-editor",
            height : "250",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            },
        };
        tinymce.init(editor_config);
    </script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush