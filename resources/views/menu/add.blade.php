@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('menu.Store') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Имя</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name') }}">
                            <span class="text-help">Наименование меню</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Позиция</h4>
                            <input type="text" name="position" class="form-control"
                                   id="inputHelpText" value="{{ old('position') }}">
                            <span class="text-help">Позиция</span>
                            @if($errors->has('position'))
                                <span class="has-error">{{ $errors->first('position') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <label for="lang">Язык *</label>
                            <select name="lang" id="lang" class="form-control boxed">
                                @foreach(\App\Models\Menu::getLanguage(true) as $lang => $name)
                                    <option value="{{ $lang }}"{{ old('lang') == $lang ? ' selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('lang'))
                                <span class="has-error">{{ $errors->first('lang') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <label for="type">Тип меню *</label>
                            <select name="type" id="type" class="form-control boxed">
                                @foreach(\App\Models\Menu::typeMenu(true) as $type => $name)
                                    <option value="{{ $type }}"{{ old('type') == $type ? ' selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('type'))
                                <span class="has-error">{{ $errors->first('type') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group d-none">
                            <label for="parent_id">Родителский меню *</label>
                            <select name="parent_id" id="parent_id" class="form-control boxed">
                                @foreach($menus as $menu)
                                    <option value="{{ $menu->id }}">{{ $menu->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('parent_id'))
                                <span class="has-error">{{ $errors->first('parent_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <label for="url">Ссылка</label>
                            <input type="text" name="url" class="form-control">
                            @if ($errors->has('url'))
                                <span class="has-error">{{ $errors->first('url') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <label for="model">Модель *</label>
                            <select name="model" id="model" class="form-control boxed">
                                    <option value=""></option>
                                @foreach(\App\Models\Menu::getModel(true) as $model => $name)
                                    <option value="{{ $model }}"{{ old('model') == $model ? ' selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('model'))
                                <span class="has-error">{{ $errors->first('model') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-12">
                        <div class="form-group d-none">
                            <label for="model_id">Страницы *</label>
                            <select name="model_id" id="page" class="form-control boxed">
                                @foreach($pages as $page)
                                    <option value="{{ $page->id }}">{{ $page->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('model_id'))
                                <span class="has-error">{{ $errors->first('model_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-12">
                        <div class="form-group d-none">
                            <label for="model">Категория *</label>
                            <select name="model_id" id="category" class="form-control boxed">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('model_id'))
                                <span class="has-error">{{ $errors->first('model_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-12">
                        <div class="form-group d-none">
                            <label for="model">Фракция *</label>
                            <select name="model_id" id="faction" class="form-control boxed">
                                @foreach($factions as $faction)
                                    <option value="{{ $faction->id }}">{{ $faction->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('model_id'))
                                <span class="has-error">{{ $errors->first('model_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-12">
                        <div class="form-group d-none">
                            <label for="model">Альбом *</label>
                            <select name="model_id" id="album" class="form-control boxed">
                                @foreach($albums as $album)
                                    <option value="{{ $album->id }}">{{ $album->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('model_id'))
                                <span class="has-error">{{ $errors->first('model_id') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('category.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Добавить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script>
        let page = $('#page');
        let category = $('#category');
        let faction = $('#faction');
        let parentId = $('#parent_id');
        let album = $('#album');
        $('#type').change(function () {
            let $this = $(this);
            if ($this.val() === '{{ \App\Models\Menu::children }}'){
                parentId.val($('#parent_id').val());
                parentId.closest('.form-group').removeClass('d-none');
            }else{
                parentId.val('');
                parentId.closest('.form-group').addClass('d-none');
            }
        });

        $('#model').change(function () {
            let $this = $(this);
            if ($this.val() === '{{ \App\Models\Menu::page }}'){
                page.val($('#page').val());
                page.closest('.form-group').removeClass('d-none');
            }else{
                page.val('');
                page.closest('.form-group').addClass('d-none');
            }
            if ($this.val() === '{{ \App\Models\Menu::category }}'){
                category.val($('#category').val());
                category.closest('.form-group').removeClass('d-none');
            }else{
                category.val('');
                category.closest('.form-group').addClass('d-none');
            }
            if ($this.val() === '{{ \App\Models\Menu::faction }}'){
                faction.val($('#page').val());
                faction.closest('.form-group').removeClass('d-none');
            }else{
                faction.val('');
                faction.closest('.form-group').addClass('d-none');
            }
            if ($this.val() === '{{ \App\Models\Menu::album }}'){
                album.val($('#album').val());
                album.closest('.form-group').removeClass('d-none');
            }else{
                album.val('');
                album.closest('.form-group').addClass('d-none');
            }
        })
    </script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush