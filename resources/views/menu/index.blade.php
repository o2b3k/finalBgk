@extends('main')
@section('page_header')
    <h1 class="page-title">Меню</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('menu.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                            <th>Позиция</th>
                            <th>Опубликован</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($menus as $menu)
                            <tr>
                                <td>{{ $menu->id }}</td>
                                <td>
                                    <a href="{{ route('menu.Show',['menu' => $menu]) }}">{{ $menu->name }}</a>
                                </td>
                                <td>{{ $menu->position }}</td>
                                <td class="w-50">
                                    <div class="checkbox-custom checkbox-primary">
                                        <input type="checkbox" class="published" data-id="{{ $menu->id }}"
                                               @if($menu->published) checked @endif/>
                                        <label for=""></label>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('menu.Edit',['menu' => $menu]) }}"
                                       class="btn btn-sm btn-success">Изменить</a>
                                    <button type="button" data-id="{{ $menu->id }}"
                                            class="btn btn-sm btn-danger btn-delete-menu">
                                        Удалить
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-menu-form" action="{{ route('menu.Destroy') }}" class="hidden" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="menu_id" id="menu_id">
    </form>
@stop
@push('js')
    <script src="{{ asset('js/snippets/delete-menu.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            $('.published').on('click', function(event){
                let id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('album.setPublished') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
        })
    </script>
@endpush