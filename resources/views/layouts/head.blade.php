<link rel="apple-touch-icon" href="{{ asset('assets/images/apple-touch-icon.png') }}">
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">
<link rel="stylesheet" href="{{ asset('css/my.css') }}">
<!-- Stylesheets -->
<link rel="stylesheet" href="{{ asset('global/css/bootstrap.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('global/css/bootstrap-extend.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('assets/css/site.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('assets/examples/css/structure/pagination.min.css?v4.0.1') }}">
<!-- Skin tools (demo site only) -->
<link rel="stylesheet" href="{{ asset('global/css/skintools.min.css?v4.0.1') }}">
<script src="{{ asset('assets/js/Plugin/skintools.min.js?v4.0.1') }}"></script>
<!-- Icons style -->
<link rel="stylesheet" href="{{ asset('global/fonts/web-icons/web-icons.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('global/fonts/7-stroke/7-stroke.min.css?v4.0.1') }}">
<!-- Plugins -->
<link rel="stylesheet" href="{{ asset('global/vendor/animsition/animsition.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('global/vendor/asscrollable/asScrollable.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('global/vendor/switchery/switchery.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('global/vendor/intro-js/introjs.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('global/vendor/slidepanel/slidePanel.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('global/vendor/flag-icon-css/flag-icon.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('global/vendor/waves/waves.min.css?v4.0.1') }}">

<!-- Page -->
<link rel="stylesheet" href="{{ asset('assets/examples/css/dashboard/v1.min.css?v4.0.1') }}">

<!-- Fonts -->
<link rel="stylesheet" href="{{ asset('global/fonts/material-design/material-design.min.css?v4.0.1') }}">
<link rel="stylesheet" href="{{ asset('global/fonts/brand-icons/brand-icons.min.css?v4.0.1') }}">
<!--<link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">-->

<!--[if lt IE 9]>
<script src="global/vendor/html5shiv/html5shiv.min.js?v4.0.1"></script>
<![endif]-->

<!--[if lt IE 10]>
<script src="/global/vendor/media-match/media.match.min.js?v4.0.1"></script>
<script src="/global/vendor/respond/respond.min.js?v4.0.1"></script>
<![endif]-->
{{--Toast--}}
<link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
<!-- Scripts -->
<script src="{{ asset('global/vendor/breakpoints/breakpoints.min.js?v4.0.1') }}"></script>
<script>Breakpoints();</script>