<!-- Core  -->
<script src="{{ asset('global/vendor/babel-external-helpers/babel-external-helpers.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/jquery/jquery.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/popper-js/umd/popper.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/bootstrap/bootstrap.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/animsition/animsition.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/mousewheel/jquery.mousewheel.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/asscrollbar/jquery-asScrollbar.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/asscrollable/jquery-asScrollable.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/ashoverscroll/jquery-asHoverScroll.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/waves/waves.min.js?v4.0.1') }}"></script>

<!-- Plugins -->
<script src="{{ asset('global/vendor/switchery/switchery.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/intro-js/intro.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/screenfull/screenfull.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/slidepanel/jquery-slidePanel.min.js?v4.0.1') }}"></script>

<!-- Scripts -->
<script src="{{ asset('global/js/State.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Component.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Base.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Config.min.js?v4.0.1') }}"></script>

<script src="{{ asset('assets/js/Section/Menubar.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/Section/GridMenu.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/Section/Sidebar.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/Section/PageAside.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/Plugin/menu.min.js?v4.0.1') }}"></script>

<!-- Config -->
<script src="{{ asset('global/js/config/colors.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/config/tour.min.js?v4.0.1') }}"></script>
<script>Config.set('assets', 'assets');</script>

<!-- Page -->
<script src="{{ asset('assets/js/Site.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/asscrollable.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/slidepanel.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/switchery.min.js?v4.0.1') }}"></script>

<script src="{{ asset('global/js/Plugin/matchheight.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/jvectormap.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/peity.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/aspaginator/jquery-asPaginator.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/examples/js/dashboard/v1.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/aspaginator.min.js?v4.0.1') }}"></script>