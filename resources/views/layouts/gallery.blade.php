<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Альбомы</title>

    <link rel="apple-touch-icon" href="{{ asset('assets/images/apple-touch-icon.png') }}">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('global/css/bootstrap.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/css/bootstrap-extend.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/site.min.css?v4.0.1') }}">

    <!-- Skin tools (demo site only) -->
    <link rel="stylesheet" href="{{ asset('global/css/skintools.min.css?v4.0.1') }}">
    <script src="{{ asset('assets/js/Plugin/skintools.min.js?v4.0.1') }}"></script>
    <!-- Plugins -->
    <link rel="stylesheet" href="{{ asset('global/vendor/animsition/animsition.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/vendor/asscrollable/asScrollable.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/vendor/switchery/switchery.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/vendor/intro-js/introjs.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/vendor/slidepanel/slidePanel.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/vendor/flag-icon-css/flag-icon.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/vendor/waves/waves.min.css?v4.0.1') }}">
    <!-- Plugins For This Page -->
    <link rel="stylesheet" href="{{ asset('global/vendor/magnific-popup/magnific-popup.min.css?v4.0.1') }}">
    <!-- Page -->
    <link rel="stylesheet" href="{{ asset('assets/examples/css/pages/gallery.min.css?v4.0.1') }}">

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('global/fonts/material-design/material-design.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/fonts/brand-icons/brand-icons.min.css?v4.0.1') }}">
    <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">

    <link rel="stylesheet" href="{{ asset('global/fonts/web-icons/web-icons.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/fonts/7-stroke/7-stroke.min.css?v4.0.1') }}">

    <!-- Scripts -->
    <script src="{{ asset('global/vendor/breakpoints/breakpoints.min.js?v4.0.1') }}"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition">

@include('layouts.navbar')

    @yield('content')

@include('layouts.footer')

<script src="{{ asset('global/vendor/babel-external-helpers/babel-external-helpers.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/jquery/jquery.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/popper-js/umd/popper.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/bootstrap/bootstrap.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/animsition/animsition.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/mousewheel/jquery.mousewheel.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/asscrollbar/jquery-asScrollbar.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/asscrollable/jquery-asScrollable.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/ashoverscroll/jquery-asHoverScroll.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/waves/waves.min.js?v4.0.1') }}"></script>

<!-- Plugins -->
<script src="{{ asset('global/vendor/switchery/switchery.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/intro-js/intro.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/screenfull/screenfull.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/slidepanel/jquery-slidePanel.min.js?v4.0.1') }}"></script>

<!-- Plugins For This Page -->
<script src="{{ asset('global/vendor/isotope/isotope.pkgd.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/vendor/magnific-popup/jquery.magnific-popup.min.js?v4.0.1') }}"></script>

<!-- Scripts -->
<script src="{{ asset('global/js/State.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Component.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Base.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Config.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/Section/Menubar.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/Section/GridMenu.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/Section/Sidebar.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/Section/PageAside.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/Plugin/menu.min.js?v4.0.1') }}"></script>

<!-- Config -->
<script src="{{ asset('global/js/config/colors.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/js/config/tour.min.js?v4.0.1') }}"></script>
<script>
    Config.set('assets', '../assets');
</script>

<!-- Page -->
<script src="{{ asset('assets/js/Site.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/asscrollable.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/slidepanel.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/switchery.min.js?v4.0.1') }}"></script>
<script src="{{ asset('global/js/Plugin/filterable.min.js?v4.0.1') }}"></script>
<script src="{{ asset('assets/examples/js/pages/gallery.min.js?v4.0.1') }}"></script>
</body>

</html>