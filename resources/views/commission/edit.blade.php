@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('commission.Update',['commission' => $commission]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12 col-ld-12">
                        <div class="form-group">
                            <h4 class="example-title">Название комиссии *</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name',$commission->name) }}">
                            <span class="text-help">Название фракция</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('chairman_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Председатель *</h4>
                            <select name="chairman_id" id="chairman_id" class="form-control">
                                @if($commission->chairman)
                                    <option value="{{ $commission->chairman->id }}">{{ $commission->chairman->fio }}</option>
                                @else
                                    <option value=""></option>
                                @endif
                                @foreach($deputies as $deputy)
                                    <option value="{{ $deputy->id }}">{{ $deputy->fio }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Руководитель *</h4>
                            <select name="deputy_id" id="deputy_id" class="form-control">
                                @if($commission->deputy)
                                    <option value="{{ $commission->deputy->id }}">{{ $commission->deputy->fio }}</option>
                                @else
                                    <option value=""></option>
                                @endif
                                @foreach($deputies as $deputy)
                                    <option value="{{ $deputy->id }}">{{ $deputy->fio }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Положение *</h4>
                            <select name="position" id="position" class="form-control">
                                @if($commission->position)
                                    <option value="{{ $commission->position->id }}">{{ $commission->position->name }}</option>
                                @else
                                    <option value=""></option>
                                @endif
                                @foreach($documents as $document)
                                    <option value="{{ $document->id }}">{{ $document->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Категория *</h4>
                            <select name="category" id="category" class="form-control">
                                @if($commission->category)
                                    <option value="{{ $commission->category->id }}">{{ $commission->category->name }}</option>
                                @else
                                    <option value=""></option>
                                @endif
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">План работы *</h4>
                            <select name="plan" id="plan" class="form-control">
                                @if($commission->plan)
                                    <option value="{{ $commission->plan->id }}">{{ $commission->plan->name }}</option>
                                @else
                                    <option value=""></option>
                                @endif
                                @foreach($documents as $document)
                                    <option value="{{ $document->id }}">{{ $document->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Отчеты *</h4>
                            <select name="reports" id="reports" class="form-control">
                                @if($commission->reports)
                                    <option value="{{ $commission->reports->id }}">{{ $commission->reports->name }}</option>
                                @else
                                    <option value=""></option>
                                @endif
                                @foreach($documents as $document)
                                    <option value="{{ $document->id }}">{{ $document->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <a href="{{ route('commission.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Изменить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush