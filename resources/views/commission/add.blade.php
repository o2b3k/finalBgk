@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('commission.Store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12 col-ld-12">
                        <div class="form-group">
                            <h4 class="example-title">Название комиссии *</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name') }}" required>
                            <span class="text-help">Название фракция</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('chairman_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Председатель *</h4>
                            <select name="chairman_id" id="chairman_id" class="form-control">
                                    <option value=""></option>
                                @foreach($deputies as $deputy)
                                    <option value="{{ $deputy->id }}">{{ $deputy->fio }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('chairman_id'))
                                <span class="has-error">{{ $errors->first('chairman_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Руководитель *</h4>
                            <select name="deputy_id" id="deputy_id" class="form-control">
                                    <option value=""></option>
                                @foreach($deputies as $deputy)
                                    <option value="{{ $deputy->id }}">{{ $deputy->fio }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('deputy_id'))
                                <span class="has-error">{{ $errors->first('deputy_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Положение *</h4>
                            <select name="position" id="position" class="form-control">
                                <option value=""></option>
                                @foreach($documents as $document)
                                    <option value="{{ $document->id }}">{{ $document->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('position'))
                                <span class="has-error">{{ $errors->first('position') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Категория *</h4>
                            <select name="category" id="category" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('category'))
                                <span class="has-error">{{ $errors->first('category') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">План работы *</h4>
                            <select name="plan" id="plan" class="form-control">
                                <option value=""></option>
                                @foreach($documents as $document)
                                    <option value="{{ $document->id }}">{{ $document->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('plan'))
                                <span class="has-error">{{ $errors->first('plan') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('deputy_id') ? 'has-error' : '' }}">
                            <h4 class="example-title">Отчеты *</h4>
                            <select name="reports" id="reports" class="form-control">
                                    <option value=""></option>
                                @foreach($documents as $document)
                                    <option value="{{ $document->id }}">{{ $document->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('reports'))
                                <span class="has-error">{{ $errors->first('reports') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('commission.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Добавить</button>
            </form>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush