@extends('main')
@section('page_header')
    <h1 class="page-title">Коммиссии</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('commission.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Председатель</th>
                        <th>Категория</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($commissions as $commission)
                        <tr>
                            <td>{{ $commission->id }}</td>
                            <td>{{ $commission->name }}</td>
                            <td>
                                @isset($commission->chairman)
                                    {{ $commission->chairman->fio }}
                                @endisset
                            </td>
                            <td>
                                @isset($commission->category)
                                    {{ $commission->category->name }}
                                @endisset
                            </td>
                            <td>
                                <a href="{{ route('commission.Edit',['commission' => $commission ]) }}" class="btn btn-sm btn-success">Изменить</a>
                                <a href="{{ route('commissions.addMemberForm',['commission' => $commission]) }}" class="btn btn-sm btn-primary">
                                    <i class="icon md-account-add"></i>
                                </a>
                                <button type="button" data-id="{{ $commission->id }}" class="btn btn-sm btn-danger btn-delete-commission">
                                    Удалить
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-commission-form" class="d-none" action="{{ route('commission.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="commission_id" name="commission_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-commission.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush