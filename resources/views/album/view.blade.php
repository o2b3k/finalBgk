@extends('layouts.gallery')

@section('content')
    <div class="page">
        <div class="page-header page-header-bordered page-header-tabs">
            <h1 class="page-title">{{ $album->name }}</h1>
            <br>
        </div>

        <div class="page-content">
            <ul class="blocks blocks-100 blocks-xxl-4 blocks-lg-3 blocks-md-2" data-plugin="filterable"
                data-filters="#exampleFilter">
                @foreach($album->photos as $image)
                    <li data-type="animal">
                        <div class="card card-shadow">
                            <figure class="card-img-top overlay-hover overlay">
                                <img class="overlay-figure overlay-scale" src="{{ asset($image->path) }}"
                                     alt="...">
                                <figcaption class="overlay-panel overlay-background overlay-fade overlay-icon">
                                    <a class="icon md-search" href="{{ asset($image->path) }}"></a>
                                </figcaption>
                            </figure>
                            <div class="card-block">
                                <form action="{{ route('album.DestroyPhoto',['id' => $image->id]) }}" method="POST">
                                    <h4 class="card-title">{{ $image->desc }}
                                        <input type="hidden" name="_method" value="delete">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="btn btn-danger btn-sm float-sm-right">Delete</button>
                                    </h4>
                                </form>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            <form action="{{ route('album.Upload',['id' => $album->id]) }}" method="POST"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-5 col-ld-5">
                        <div class="form-group">
                            <h4 class="example-title">Изоброжения</h4>
                            <input type="file" name="image" class="form-control">
                            <span class="text-help">Выбирите файл</span>
                        </div>
                    </div>
                    <div class="col-sm-5 col-ld-5">
                        <div class="form-group">
                            <h4 class="example-title">Описания</h4>
                            <input type="text" name="desc" class="form-control"
                                   id="inputHelpText">
                            <span class="text-help">Описание изоброжения</span>
                        </div>
                    </div>
                    <div class="col-md-2 col-ld-2">
                        <div class="form-group">
                            <h4 class="example-title">Загрузить</h4>
                            <button class="btn btn-sm">Загрузить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop