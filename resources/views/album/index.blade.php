@extends('main')
@section('page_header')
    <h1 class="page-title">Альбом</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Наименование</th>
                        <th>Опубликован</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($albums as $album)
                            <tr>
                                <td>{{ $album->id }}</td>
                                <td>
                                    <a href="{{ route('album.Show',['id' => $album->id]) }}">{{ $album->name }}</a>
                                </td>
                                <td class="w-50">
                                    <div class="checkbox-custom checkbox-primary">
                                        <input type="checkbox" class="published" data-id="{{ $album->id }}"
                                               @if($album->published) checked @endif/>
                                        <label for=""></label>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('album.Edit',['id' => $album->id]) }}"
                                       class="btn btn-sm btn-success">Изменить</a>
                                    <button type="button" data-id="{{ $album->id }}"
                                            class="btn btn-sm btn-danger btn-delete-album">
                                        Удалить
                                    </button>
                                </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <form action="{{ isset($album_one) ? route('album.Update',['id' => $album_one->id]) : route('album.Store') }}" method="post" name="album">
                {{ csrf_field() }}
                <div class="row">
                <div class="col-sm-5 col-ld-5">
                    <div class="form-group">
                        <h4 class="example-title">Имя</h4>
                        <input type="text" name="name" class="form-control"
                               id="inputHelpText" value="{{ isset($album_one) ? old('name',$album_one->name) : ''}}" required>
                        <span class="text-help">Наименование альбома</span>
                    </div>
                </div>
                <div class="col-sm-5 col-ld-5">
                    <div class="form-group">
                        <h4 class="example-title">Описания</h4>
                        <input type="text" name="desc" class="form-control"
                               id="inputHelpText" value="{{ isset($album_one) ? old('desc',$album_one->desc) : ''}}">
                        <span class="text-help">Описание альбома</span>
                    </div>
                </div>
                <div class="col-md-2 col-ld-2">
                    <div class="form-group">
                        <h4 class="example-title">
                            @if(isset($album_one))
                                Изменить
                            @else
                                Добавить
                            @endif
                        </h4>
                        <button class="btn btn-sm">
                            @if(isset($album_one))
                                Изменить
                            @else
                                Добавить
                            @endif
                        </button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-album-form" action="{{ route('album.Destroy') }}" class="hidden" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="album_id" id="album_id">
    </form>
@stop
@push('js')
    <script src="{{ asset('js/snippets/delete-album.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            $('.published').on('click', function(event){
                let id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('album.setPublished') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
        })
    </script>
@endpush