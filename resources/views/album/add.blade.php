@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1></h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                @if (isset($album))
                    Изменить альбом
                @else
                    Добавить альбом
                @endif
            </h3>
        </div>
        <div class="box-body">
            <form action="{{ isset($album) ? route('album.Update',['id' => $album->id]) : route('album.Store') }}" method="POST">
                {{ csrf_field() }}

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Наименование</label>
                        <input type="text" name="name" class="form-control"
                               value="{{ old('name',isset($album) ? $album->name : '') }}" required>
                        @if($errors->has('name'))
                            <span class="has-error">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-success pull-right">
                    @if (isset($album))
                        Изменить
                    @else
                        Добавить
                    @endif
                </button>
            </form>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush