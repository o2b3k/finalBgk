<!DOCTYPE html>
<html class="no-js css-menubar" lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>БГК</title>

    @include('layouts.head')
    @stack('css')
    @yield('css')

</head>
<body class="animsition dashboard">
<!--[if lt IE 8]>
<p class="browserupgrade">
    You are using an <strong>outdated</strong> browser.
    Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<![endif]-->
@include('layouts.navbar')
    <div class="page">
        <div class="page-header">
            @section('page_header')

            @show
        </div>
        <div class="page-content">
            @yield('content')
        </div>
    </div>

    @include('layouts.footer')
    @include('layouts.scripts')
    @stack('js')
    @yield('js')
</body>
</html>