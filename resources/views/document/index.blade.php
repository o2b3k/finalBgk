@extends('main')
@section('page_header')
    <h1 class="page-title">Документы</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('document.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Наименование</th>
                        <th>Тип</th>
                        <th>Категория</th>
                        <th>Опубликован</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($documents as $document)
                        <tr>
                            <td>{{ $document->id }}</td>
                            <td>{{ $document->name }}</td>
                            <td>
                                @if($document->type == "legislation")
                                    Закон
                                    @elseif($document->type == "draft_law")
                                    Проект закона
                                    @elseif($document->type == "decree")
                                    Постановление
                                    @elseif($document->type == "document")
                                    Обычный документ
                                    @elseif($document->type == "report")
                                    Отчет
                                @endif
                            </td>
                            <td>
                                @isset($document->category)
                                    {{ $document->category->name }}
                                @endisset
                            </td>
                            <td class="w-50">
                                <div class="checkbox-custom checkbox-primary">
                                    <input type="checkbox" class="published" data-id="{{ $document->id }}"
                                           @if($document->published) checked @endif/>
                                    <label for=""></label>
                                </div>
                            </td>
                            <td>
                                <a href="{{ route('document.Edit', ['id' => $document->id ]) }}" class="btn btn-icon btn-success">
                                    <i class="icon md-edit"></i>
                                </a>
                                <button type="button" data-id="{{ $document->id }}" class="btn btn-icon btn-danger btn-delete-document">
                                    <i class="icon md-delete" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $documents->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-document-form" class="d-none" action="{{ route('document.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="document_id" name="document_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-document.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            $('.published').on('click', function(event){
                let id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('document.setPublished') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
            $('.archive').on('click', function (event) {
                let id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('document.setArchive') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function (data) {
                        //empty
                    },
                });
            });
        })
    </script>
@endpush