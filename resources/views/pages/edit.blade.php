@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('pages.Update',['id' => $pages->id]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <h4 class="example-title">Имя страницы</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name',$pages->name) }}">
                            <span class="text-help">Наименование страницы</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    @if($pages->type == "VIDEO")
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="slug">Видео</label>
                                <input type="text" name="media" class="form-control"
                                       value="{{ old('media', $pages->media) }}" title="">
                                @if($errors->has('media'))
                                    <span class="has-error">{{ $errors->first('media') }}</span>
                                @endif
                            </div>
                        </div>
                    @endif
                    @if($pages->type == "IMAGE")
                        <div class="col-sm-4 col-ld-4">
                            <div class="form-group">
                                <h4 class="example-title">Изоброжения</h4>
                                <img src="{{ asset($pages->media) }}" alt="" class="img-fluid w-full">
                                @if($errors->has('image'))
                                    <span class="has-error">{{ $errors->first('image') }}</span>
                                @endif
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                    @endif
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                            <label for="category">Текущая категория *</label>
                            <select name="category_id" id="category" class="form-control">
                                @isset($pages->category)
                                    <option value="{{ $pages->category->id }}">{{ $pages->category->name }}</option>
                                @endisset
                                @foreach($category as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <label for="lang">Текущий язык *</label>
                            <select name="lang" id="lang" class="form-control boxed">
                                <option value="{{ $pages->lang }}">
                                    @if($pages->lang == "ru")
                                        Русский
                                        @elseif($pages->lang == "ky")
                                        Кыргызча
                                        @elseif($pages->lang == "en")
                                        English
                                    @endif
                                </option>
                                @foreach(\App\Models\Page::getLanguage(true) as $lang => $name)
                                    <option value="{{ $lang }}"{{ old('lang') == $lang ? ' selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('lang'))
                                <span class="has-error">{{ $errors->first('lang') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('text') ? 'has-error' : '' }}">
                            <label for="editor"></label>
                            <textarea name="text" class="form-control my-editor">
                                 {!! $pages->text !!}
                            </textarea>
                            @if($errors->has('text'))
                                <span class="has-error">{{ $errors->first('text') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('pages.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Изменить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            language_url : '/js/ru.js',
            selector: "textarea.my-editor",
            height : "250",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            },
        };
        tinymce.init(editor_config);
    </script>
@endpush
@push('js')
    <script>
        var imageId = $('#image');
        var videoId = $('#video');

        $('#type').change(function () {
            var $this = $(this);
            if ($this.val() === '{{ \App\Models\Page::TYPE_IMAGE }}'){
                imageId.val($('#image').val());
                imageId.closest('.form-group').removeClass('d-none');
            }else {
                imageId.val('');
                imageId.closest('.form-group').addClass('d-none');
            }
            if ($this.val() === '{{ \App\Models\Page::TYPE_VIDEO }}'){
                videoId.val($('#video').val());
                videoId.closest('.form-group').removeClass('d-none');
            }else {
                videoId.val('');
                videoId.closest('.form-group').addClass('d-none');
            }
        })
    </script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush