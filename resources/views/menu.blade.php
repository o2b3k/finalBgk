<li class="site-menu-item">
    <a href="{{ route('dashboard.Index') }}">
        <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
        <span class="site-menu-title">Статистика</span>
    </a>
</li>
<li class="site-menu-item has-sub">
    <a href="javascript:void(0)">
        <i class="site-menu-icon md-collection-text" aria-hidden="true"></i>
        <span class="site-menu-title">Новости</span>
        <span class="site-menu-arrow"></span>
    </a>
    <ul class="site-menu-sub">
        <li class="site-menu-item">
            <a href="{{ route('news.Ru') }}">
                <span class="site-menu-title">Русский</span>
            </a>
        </li>
        <li class="site-menu-item">
            <a href="{{ route('news.Ky') }}">
                <span class="site-menu-title">Кыргызча</span>
            </a>
        </li>
    </ul>
</li>
<li class="site-menu-item has-sub">
    <a href="javascript:void(0)">
        <i class="site-menu-icon md-palette" aria-hidden="true"></i>
        <span class="site-menu-title">Кастомизация</span>
        <span class="site-menu-arrow"></span>
    </a>
    <ul class="site-menu-sub">
        <li class="site-menu-item has-sub">
            <a href="javascript:void(0)">
                <span class="site-menu-title">Меню</span>
                <span class="site-menu-arrow"></span>
            </a>
            <ul class="site-menu-sub">
                <li class="site-menu-item">
                    <a href="{{ route('menu.Russian') }}">
                        <span class="site-menu-title">Русский</span>
                    </a>
                </li>
                <li class="site-menu-item">
                    <a href="{{ route('menu.Kyrgyz') }}">
                        <span class="site-menu-title">Кыргызча</span>
                    </a>
                </li>
            </ul>
        <li class="site-menu-item">
            <a href="{{ route('pages.Index') }}">
                <span class="site-menu-title">Страница</span>
            </a>
        </li>
        <li class="site-menu-item">
            <a href="{{ route('category.Index') }}">
                <span class="site-menu-title">Категория</span>
            </a>
        </li>
    </ul>
</li>
<li class="site-menu-item has-sub">
    <a href="javascript:void(0)">
        <i class="site-menu-icon md-menu" aria-hidden="true"></i>
        <span class="site-menu-title">Фракции и Депутаты</span>
        <span class="site-menu-arrow"></span>
    </a>
    <ul class="site-menu-sub">
        <li class="site-menu-item has-sub">
            <a href="#">
                <span class="site-menu-title">Депутаты</span>
                <span class="site-menu-arrow"></span>
            </a>
            <ul class="site-menu-sub">
                <li class="site-menu-item">
                    <a href="{{ route('deputy.RuIndex') }}">
                        <span class="site-menu-title">Русский</span>
                    </a>
                </li>
                <li class="site-menu-item">
                    <a href="{{ route('deputy.KyIndex') }}">
                        <span class="site-menu-title">Кыргызча</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="site-menu-item">
            <a href="{{ route('faction.Index') }}">
                <span class="site-menu-title">Партии</span>
            </a>
        </li>
    </ul>
</li>
<li class="site-menu-item has-sub">
    <a href="javascript:void(0)">
        <i class="site-menu-icon md-assignment-o" aria-hidden="true"></i>
        <span class="site-menu-title">Постановления</span>
        <span class="site-menu-arrow"></span>
    </a>
    <ul class="site-menu-sub">
        <li class="site-menu-item">
            <a href="{{ route('resolution.Index') }}">
                <span class="site-menu-title">Созыв</span>
            </a>
        </li>
        <li class="site-menu-item">
            <a href="{{ route('decree.Index') }}">
                <span class="site-menu-title">Документы</span>
            </a>
        </li>
    </ul>
</li>
<li class="site-menu-item">
    <a href="{{ route('commission.Index') }}">
        <i class="site-menu-icon md-balance" aria-hidden="true"></i>
        <span class="site-menu-title">Комиссия</span>
    </a>
</li>
<li class="site-menu-item">
    <a href="{{ route('document.Index') }}">
        <i class="site-menu-icon md-assignment-o" aria-hidden="true"></i>
        <span class="site-menu-title">Документы</span>
    </a>
</li>
<li class="site-menu-item has-sub">
    <a href="javascript:void(0)">
        <i class="site-menu-icon md-storage" aria-hidden="true"></i>
        <span class="site-menu-title">Футер</span>
        <span class="site-menu-arrow"></span>
    </a>
    <ul class="site-menu-sub">
        <li class="site-menu-item">
            <a href="{{ route('footer.IndexRu') }}">
                <span class="site-menu-title">Русский</span>
            </a>
        </li>
        <li class="site-menu-item">
            <a href="{{ route('footer.IndexKy') }}">
                <span class="site-menu-title">Кыргызча</span>
            </a>
        </li>
    </ul>
</li>
<li class="site-menu-item">
    <a href="{{ route('complain.Index') }}">
        <i class="site-menu-icon md-assignment-o" aria-hidden="true"></i>
        <span class="site-menu-title">Сообщение</span>
    </a>
</li>
<li class="site-menu-item">
    <a href="{{ route('album.Index') }}">
        <i class="site-menu-icon icon wb-gallery" aria-hidden="true"></i>
        <span class="site-menu-title">Галерея</span>
    </a>
</li>
<li class="site-menu-item">
    <a href="{{ route('files') }}">
        <i class="site-menu-icon md-folder" aria-hidden="true"></i>
        <span class="site-menu-title">Менеджер файлов</span>
    </a>
</li>
<li class="site-menu-item">
    <a href="">
        <i class="site-menu-icon md-info" aria-hidden="true"></i>
        <span class="site-menu-title">Служба поддержки</span>
    </a>
</li>