@extends('main')
@section('css')
    <!-- Page -->
    <link rel="stylesheet" href="{{ asset('assets/examples/css/widgets/statistics.min.css?v4.0.1') }}">
@stop
@section('page_header')
    <h1 class="page-title">Статистика</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">

            </h3>
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <!-- Card -->
                    <div class="card card-block p-30 bg-google-plus">
                        <div class="card-watermark darker font-size-80 m-15"><i class="icon md-assignment" aria-hidden="true"></i></div>
                        <div class="counter counter-md counter-inverse text-left">
                            <div class="counter-number-group">
                                <span class="counter-number">{{ $document }}</span>
                                <span class="counter-number-related text-capitalize">документы</span>
                            </div>
                            <div class="counter-label text-capitalize"></div>
                        </div>
                    </div>
                    <!-- End Card -->
                </div>
                <div class="col-md-6">
                    <!-- Card -->
                    <div class="card card-block p-30 bg-green-a400">
                        <div class="card-watermark darker font-size-80 m-15"><i class="icon pe-news-paper" aria-hidden="true"></i></div>
                        <div class="counter counter-md counter-inverse text-left">
                            <div class="counter-number-group">
                                <span class="counter-number">{{ $news }}</span>
                                <span class="counter-number-related text-capitalize">новости</span>
                            </div>
                            <div class="counter-label text-capitalize"></div>
                        </div>
                    </div>
                    <!-- End Card -->
                </div>
                <div class="col-md-6">
                    <!-- Card -->
                    <div class="card card-block p-30 bg-primary-400">
                        <div class="card-watermark darker font-size-80 m-15"><i class="icon md-account-box" aria-hidden="true"></i></div>
                        <div class="counter counter-md counter-inverse text-left">
                            <div class="counter-number-group">
                                <span class="counter-number">{{ $deputy }}</span>
                                <span class="counter-number-related text-capitalize">депутаты</span>
                            </div>
                            <div class="counter-label text-capitalize"></div>
                        </div>
                    </div>
                    <!-- End Card -->
                </div>
                <div class="col-md-6">
                    <!-- Card -->
                    <div class="card card-block p-30 bg-github">
                        <div class="card-watermark darker font-size-80 m-15"><i class="icon md-account-box" aria-hidden="true"></i></div>
                        <div class="counter counter-md counter-inverse text-left">
                            <div class="counter-number-group">
                                <span class="counter-number">{{ $faction }}</span>
                                <span class="counter-number-related text-capitalize">партия</span>
                            </div>
                            <div class="counter-label text-capitalize"></div>
                        </div>
                    </div>
                    <!-- End Card -->
                </div>
                <div class="col-md-6">
                    <!-- Card -->
                    <div class="card card-block p-30 bg-blue-grey-500">
                        <div class="card-watermark darker font-size-80 m-15"><i class="icon md-assignment-o" aria-hidden="true"></i></div>
                        <div class="counter counter-md counter-inverse text-left">
                            <div class="counter-number-group">
                                <span class="counter-number">{{ $complain }}</span>
                                <span class="counter-number-related text-capitalize">обрашение</span>
                            </div>
                            <div class="counter-label text-capitalize">для депутатов</div>
                        </div>
                    </div>
                    <!-- End Card -->
                </div>
                <div class="col-md-6">
                    <!-- Card -->
                    <div class="card card-block p-30 bg-evernote">
                        <div class="card-watermark darker font-size-80 m-15"><i class="icon md-balance" aria-hidden="true"></i></div>
                        <div class="counter counter-md counter-inverse text-left">
                            <div class="counter-number-group">
                                <span class="counter-number">{{ $commission }}</span>
                                <span class="counter-number-related text-capitalize">коммиссии</span>
                            </div>
                            <div class="counter-label text-capitalize"></div>
                        </div>
                    </div>
                    <!-- End Card -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
@stop
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush