@extends('main')
@section('page_header')
    <h1 class="page-title">Обрашение</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                {{ $complain->subject }}
            </h3>
        </header>
        <div class="panel-body">
            <div class="col-sm-6 col-lg-6">
                <h4 class="example-title">Ф.И.О</h4>
                <h4 class="example-title">{{ $complain->fio }}</h4>
            </div>
            <br>
            <div class="col-sm-6 col-lg-6">
                <h4 class="example-title">Email</h4>
                <h4 class="example-title">{{ $complain->email }}</h4>
            </div>
            <br>
            <div class="col-sm-6 col-lg-6">
                <h4 class="example-title">Телефон</h4>
                <h4 class="example-title">{{ $complain->tel }}</h4>
            </div>
            <br>
            <div class="col-sm-6 col-lg-6">
                <h4 class="example-title">Кому</h4>
                <h4 class="example-title">
                    @isset($complain->deputy)
                        {{ $complain->deputy->fio }}
                    @endisset
                </h4>
            </div>
            <br>
            <div class="col-sm-6 col-lg-6">
                <h4 class="example-title">Тема</h4>
                <h4 class="example-title">{{ $complain->subject }}</h4>
            </div>
            <br>
            <div class="col-sm-6 col-lg-6">
                <h4 class="example-title">Сообшение</h4>
                <h4 class="example-title">{{ $complain->text }}</h4>
            </div>
            <a href="{{ route('complain.Index') }}" class="btn btn-danger">Назад</a>
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
@stop
