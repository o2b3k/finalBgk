@extends('main')
@section('page_header')
    <h1 class="page-title">Обрашения</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">

            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Ф.И.О</th>
                        <th>Кому</th>
                        <th>Фракция</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($complains as $complain)
                        <tr>
                            <td>{{ $complain->id }}</td>
                            <td>{{ $complain->fio }}</td>
                            <td>
                                @isset($complain->deputy)
                                    {{ $complain->deputy->fio }}
                                @endisset
                            </td>
                            <td>
                                @isset($complain->deputy->faction)
                                    {{ $complain->deputy->faction->name }}
                                @endisset
                            </td>
                            <td>
                                <a href="{{ route('complain.Show',['complain' => $complain]) }}" class="btn btn-sm btn-success">Посмотреть</a>
                                <button type="button" data-id="{{ $complain->id }}" class="btn btn-sm btn-danger btn-delete-complain">
                                    Удалить
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $complains->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-complain-form" class="d-none" action="{{ route('complain.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="complain_id" name="complain_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-complain.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush