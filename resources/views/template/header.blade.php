<!-- HEADER -->
<div class="gk_top-header ">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="gk_top-item">
                    <div class="gk_top-logo">
                        <a href="#"><img src="{{ asset('img/logo.png') }}" class="img-fluid" alt=""></a>
                    </div>
                    <div class="gk_top-text">
                        <h1><span>Бишкекский</span> городской кенеш</h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="gk_top-item-2">
                    <div class="gk_top_social-icon">
                        <ul class="gk_top_sprite-ul">
                            <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                            <li><i class="fa fa-odnoklassniki" aria-hidden="true"></i></li>
                            <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                    <div class="gk_top_search">
                        <input type="search" placeholder="Поиск"><button class="search_sub" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav>
        <div class="container gk_nav fl">
            <div class="row">
                <div class="gk_nav-menu">
                    <div class="gk_burger-menu">
                        Меню
                    </div>
                    <ul class="ul_main_menu">
                        <li class="gk_nav_menu-li"><a href="{{ route('index') }}" class="menu-link">Главная</a></li>
                        <li class="gk_nav_menu-li"><a href="#" class="menu-link">Депутаты</a>
                            <ul class="gk_submenu">
                                <li class="gk_submenu-li"><a href="profile.html" class="gk_submenu-link">СДПК</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Республика Ата-Журт</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Өнүгүү-Прогресс</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Мекеним Кыргызстан</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Кыргызcтан</a></li>
                            </ul>
                        </li>
                        <li class="gk_nav_menu-li gk_nav_menu-active"><a href="#" class="menu-link">Аппарат БГК </i></a>
                            <ul class="gk_submenu">
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Конституция</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Регламент</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">План работы</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Отделы</a></li>
                            </ul>
                        </li>

                        <li class="gk_nav_menu-li"><a href="#" class="menu-link">Комиссии </i></a>
                            <ul class="gk_submenu">
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Постоянные комиссии</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Другие комиссии</a></li>
                            </ul>
                        </li>
                        <li class="gk_nav_menu-li gk_nav_menu-active"><a href="#" class="menu-link active_menu">Обращение граждан </a>
                        </li>
                        <li class="gk_nav_menu-li"><a href="#" class="menu-link">Постановления</a>
                            <ul class="gk_submenu">
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Проекты постановления</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Принятые постановления</a></li>
                            </ul>
                        </li>
                        <li class="gk_nav_menu-li"><a href="#" class="menu-link">Галерея</a></li>
                        <li class="gk_nav_menu-li live"><a href="#" class="menu-link">Прямая трансляция</a></li>
                        <li class="gk_nav_menu-li more"><a href="#" class="menu-link">Еще</a>
                            <ul class="gk_submenu">
                                <li class="gk_submenu-li"><a href="profile.html" class="gk_submenu-link">СДПК</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Республика Ата-Журт</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Өнүгүү-Прогресс</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Мекеним Кыргызстан</a></li>
                                <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Кыргызcтан</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>
<!--END HEADER  -->