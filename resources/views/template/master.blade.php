<!DOCTYPE html>
<html lang="ru">
@include('template.head')
<body>
<div class="gk_wrapper_site">
    <!-- TOP SYSTEM MENU -->
    <div class="gk_top-system-menu">
        <div class="container">
            <div class="row">
                <ul class="gk_top_flags">
                    <li class="gk_top_flags-li">
                        <img src="{{ asset('img/kg.png') }}" class="img-fluid" alt="">
                        <a href="{{ url(Session::get('ky')) }}" class="gk_top_flags-link">КЫРГ</a>
                    </li>
                    <li class="gk_top_flags-li">
                        <img src="{{ asset('img/ru.png') }}" class="img-fluid" alt="">
                        <a href="{{ url(Session::get('ru'))  }}" class="gk_top_flags-link">РУС</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END SYSTEM MENU -->
    <!-- HEADER -->
    <div class="gk_top-header ">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="gk_top-item">
                        <div class="gk_top-logo">
                            <a href="/"><img src="{{ asset('img/logo.png') }}" class="img-fluid" alt=""></a>
                        </div>
                        <div class="gk_top-text">
                            <h1><b>Бишкекский</b><br> <b>городской кенеш</b></h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="gk_top-item-2">
                        <div class="gk_top_social-icon">
                            <ul class="gk_top_sprite-ul">
                                <li><a href="https://www.facebook.com/bishkekgorkenesh/" target="_blank"><i style="color: white;" class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.instagram.com/gorkenesh/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="gk_top_search">
                            <form action="{{ route('front.Search') }}" method="get">
                                <input type="search" id="search" name="search" placeholder="Поиск">
                                <button class="search_sub" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @yield('menu')
    </div>
    <!--END HEADER  -->
    @yield('phone')

    @yield('content')

    @yield('footer')

</div>
@stack('js')
@yield('js')
<div id="top"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('libs/normalize.css') }}">
<link rel="stylesheet" href="{{ asset('libs/jquery.scrollbar.css') }}">
<link rel="stylesheet" href="{{ asset('libs/remodal/remodal.css') }}">
<link rel="stylesheet" href="{{ asset('libs/remodal/remodal-default-theme.css') }}">
<link rel="stylesheet" href="{{ asset('libs/lightbox/lightgallery.min.css')}}">
<link rel="stylesheet" href="{{ asset('libs/accordeon/jquery.beefup.css')}}">
<script src="{{ elixir('js/app.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
<!-- fotorama.css & fotorama.js. -->
<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
<script src="{{ asset('libs/lightbox/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('libs/lightbox/lightgallery.js') }}"></script>
<script src="{{ asset('libs/jquery.scrollbar.js') }}"></script>
<script src="{{ asset('libs/accordeon/jquery.beefup.min.js') }}"></script>
<script src="{{ asset('libs/lightbox/lg-thumbnail.min.js') }}"></script>
<script src="{{ asset('libs/lightbox/lg-fullscreen.min.js') }}"></script>
<script src="{{ asset('libs/remodal/remodal.js') }}"></script>
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script src="{{ asset('js/tos.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#lightgallery").lightGallery(); 
    });
</script>
</body>
</html>