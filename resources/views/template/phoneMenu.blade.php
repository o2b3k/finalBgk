<!-- Phone Menu -->
<div class="gk_phone">
    <div class="container">
        <div class="row">
            <div class="gk_nav-phone">
                <ul class="gk_nav__phone-ul">
                    <li class="gk_nav__phone_ul-li"><a href="#">Главная</a></li>
                    <li class="gk_nav__phone_ul-li"><a href="#">Аппарат БГК</a></li>
                    <li class="gk_nav__phone_ul-li"><a href="#">Депутаты</a>
                        <ul class="phone_submenu">
                            <li>
                                <a href="#" class="phone_link">СДПК</a>
                            </li>
                            <li>
                                <a href="#" class="phone_link">СДПК</a>
                            </li>
                            <li>
                                <a href="#" class="phone_link">СДПК</a>
                            </li>
                            <li>
                                <a href="#" class="phone_link">СДПК</a>
                            </li>
                        </ul>
                    </li>
                    <li class="gk_nav__phone_ul-li"><a href="#">Комиссии</a></li>
                    <li class="gk_nav__phone_ul-li"><a href="#">Обращение граждан</a></li>
                    <li class="gk_nav__phone_ul-li"><a href="#">Постановления</a></li>
                    <li class="gk_nav__phone_ul-li"><a href="#">Галерея</a></li>
                    <li class="gk_nav__phone_ul-li"><a href="#">Прямая трансляция</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Phone menu -->