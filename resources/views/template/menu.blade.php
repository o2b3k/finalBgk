<nav>
    <div class="container gk_nav fl">
        <div class="row">
            <div class="gk_nav-menu">
                <div class="gk_burger-menu">
                    Меню
                </div>
                <ul class="ul_main_menu">
                    <li class="gk_nav_menu-li"><a href="{{ route('index') }}" class="menu-link">Главная</a></li>
                    <li class="gk_nav_menu-li"><a href="#" class="menu-link">Депутаты</a>
                        <ul class="gk_submenu">
                            @foreach($factions as $faction)
                                <li class="gk_submenu-li"><a href="{{ route('front.FactionView', ['faction' => $faction]) }}" class="gk_submenu-link">{{ $faction->name }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="gk_nav_menu-li gk_nav_menu-active"><a href="#" class="menu-link">Аппарат БГК </i></a>
                        <ul class="gk_submenu">
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Конституция</a></li>
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Регламент</a></li>
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">План работы</a></li>
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Отделы</a></li>
                        </ul>
                    </li>

                    <li class="gk_nav_menu-li"><a href="#" class="menu-link">Комиссии</a>
                        <ul class="gk_submenu">
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Постоянные комиссии</a></li>
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Другие комиссии</a></li>
                        </ul>
                    </li>
                    <li class="gk_nav_menu-li gk_nav_menu-active"><a href="#" class="menu-link active_menu">Обращение граждан </a>
                    </li>
                    <li class="gk_nav_menu-li"><a href="#" class="menu-link">Постановления</a>
                        <ul class="gk_submenu">
                            <li class="gk_submenu-li"><a href="{{ route('front.Legislation') }}" class="gk_submenu-link">Проекты постановления</a></li>
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Принятые постановления</a></li>
                        </ul>
                    </li>
                    <li class="gk_nav_menu-li"><a href="#" class="menu-link">Галерея</a></li>
                    <li class="gk_nav_menu-li live"><a href="#" class="menu-link">Прямая трансляция</a></li>
                    <li class="gk_nav_menu-li more"><a href="#" class="menu-link">Еще</a>
                        <ul class="gk_submenu">
                            <li class="gk_submenu-li"><a href="profile.html" class="gk_submenu-link">СДПК</a></li>
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Республика Ата-Журт</a></li>
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Өнүгүү-Прогресс</a></li>
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Мекеним Кыргызстан</a></li>
                            <li class="gk_submenu-li"><a href="#" class="gk_submenu-link">Кыргызcтан</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>