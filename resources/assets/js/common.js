$(document).ready(function () {
	"use strict";
	$(".gk_burger-menu").on("click", function (e) {
		$('.gk_nav-phone').toggle();
		e.stopPropagation();
		e.preventDefault();
	});

	$(window).resize(function () {
		if ($(window).width() < 992) {
			$(".gk_nav .container").removeClass("container")
			$(".fl").addClass("container-fluid")
		}
		if ($(window).width() > 992) {
			$(".fl").removeClass("container-fluid")
			$(".fl").addClass("container")
			$(".gk_nav-phone").css("display", "none")
		}
	});

	$(function() {
        $('.beefup').beefup();
	});	

	var top = 150;
	var delay = 1000;

	$(window).scroll(function(){
		if($(this).scrollTop() > top){
			$('#top').fadeIn()
		}else{
			$('#top').fadeOut();
		};
	});

	$('#top').on('click', function(){
		$('body, html').animate({
			scrollTop: $('html').offset().top
		}, delay)
	});


});