<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug',250);
            $table->string('title',250);
            $table->longText('text');
            $table->string('lang');
            $table->boolean('published')->default(0);
            $table->boolean('archive')->default(0);
            $table->string('media')->nullable();
            $table->string('image')->nullable();
            $table->string('teg')->nullable();
            $table->integer('count')->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('home')->nullable();
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
