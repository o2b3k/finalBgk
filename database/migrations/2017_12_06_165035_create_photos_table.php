<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->string('desc')->nullable();
            $table->integer('news_id')->unsigned()->nullable();
            $table->integer('album_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('album_id')->references('id')->on('albums')->onDelete('set null');
            $table->foreign('news_id')->references('id')->on('news')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
