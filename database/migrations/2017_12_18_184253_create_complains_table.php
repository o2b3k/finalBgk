<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fio',250);
            $table->string('email',250);
            $table->string('tel')->nullable();
            $table->string('subject',250);
            $table->longText('text');
            $table->string('file')->nullable();
            $table->integer('deputy_id')->unsigned()->nullable();
            $table->foreign('deputy_id')->references('id')->on('deputies')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complains');
    }
}
