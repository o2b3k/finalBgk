<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commission_id')->unsigned()->nullable();
            $table->integer('deputy_id')->unsigned()->nullable();
            $table->foreign('commission_id')->references('id')->on('commissions')->onDelete('set null');
            $table->foreign('deputy_id')->references('id')->on('deputies')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_members');
    }
}
