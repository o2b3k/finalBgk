<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeputiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deputies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fio',250);
            $table->longText('profile');
            $table->string('tel')->nullable();
            $table->string('email')->nullable();
            $table->string('fb')->nullable();
            $table->string('ok')->nullable();
            $table->string('ins')->nullable();
            $table->string('image')->nullable();
            $table->integer('faction_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('faction_id')->references('id')->on('factions')->onDelete('set null');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deputies');
    }
}
