<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactionMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faction_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('faction_id')->unsigned()->nullable();
            $table->integer('deputy_id')->unsigned()->nullable();
            $table->foreign('faction_id')->references('id')->on('factions')->onDelete('set null');
            $table->foreign('deputy_id')->references('id')->on('deputies')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faction_members');
    }
}
