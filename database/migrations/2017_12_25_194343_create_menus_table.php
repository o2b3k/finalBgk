<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',250);
            $table->string('slug',250);
            $table->string('url')->nullable();
            $table->string('lang')->nullable();
            $table->integer('position')->nullable();
            $table->string('model')->nullable();
            $table->integer('model_id')->nullable();
            $table->string('type')->nullable();
            $table->boolean('published')->default(false)->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('parent_id')->references('id')->on('menus')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
