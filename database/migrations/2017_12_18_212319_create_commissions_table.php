<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('slug');
            $table->integer('chairman_id')->unsigned()->nullable();
            $table->integer('deputy_id')->unsigned()->nullable();
            $table->integer('position_id')->unsigned()->nullable();
            $table->integer('plan_id')->unsigned()->nullable();
            $table->integer('reports_id')->unsigned()->nullable();
            $table->integer('news_id')->unsigned()->nullable();
            $table->foreign('chairman_id')->references('id')->on('deputies')->onDelete('set null');
            $table->foreign('deputy_id')->references('id')->on('deputies')->onDelete('set null');
            $table->foreign('position_id')->references('id')->on('documents')->onDelete('set null');
            $table->foreign('plan_id')->references('id')->on('documents')->onDelete('set null');
            $table->foreign('reports_id')->references('id')->on('documents')->onDelete('set null');
            $table->foreign('news_id')->references('id')->on('categories')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
