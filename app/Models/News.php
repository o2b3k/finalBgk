<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
/**
 * App\Models\News
 *
 * @property-read \App\Models\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Photo[] $photos
 * @property-read \App\Models\Position $position
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News findSimilarSlugs($attribute, $config, $slug)
 * @mixin \Eloquent
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $text
 * @property string $lang
 * @property int $published
 * @property int $archive
 * @property string|null $media
 * @property string|null $image
 * @property string|null $teg
 * @property int|null $count
 * @property int|null $category_id
 * @property int|null $home
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereArchive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereMedia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereTeg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 */
class News extends Model
{
    use Sluggable;

    const ru = 'ru';
    const ky = 'ky';
    const en = 'en';

    public static function getLanguage($withName)
    {
        if ($withName) {
            return [
                self::ru => 'Русский',
                self::ky => 'Кыргызча',
                self::en => 'English',
            ];
        }
        return [
            self::ru,
            self::ky,
            self::en,
        ];
    }

    protected $table = 'news';

    protected $fillable = ['slug', 'title', 'text', 'lang',
        'image', 'media', 'published', 'archive', 'category_id', 'home'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function getExcerpt($str, $startPos = 0, $maxLength = 180)
    {
        if (strlen($str) > $maxLength) {
            $excerpt = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt = substr($excerpt, 0, $lastSpace);
            $excerpt .= ' ...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    public function position()
    {
        return $this->hasOne('App\Models\Position');
    }

    public static function popularNews(Request $request)
    {
        $language = self::getLang($request);
        $popular = News::wherePublished(1)->where('lang', $language)->orderBy('updated_at', 'desc')->limit(4)->get();
        return $popular;
    }

    public static function getLang($request)
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $log = $request->route()->getPrefix();
        $language = substr($log, -2);
        if ($locale == 'en') {
            $locale = 'ru';
        }
        if ($language == '') {
            $language = $locale;
        }
        return $language;
    }

    public static function one(Request $request)
    {
        $language = self::getLang($request);
        $one = Position::where('lang', $language)->where('position','main_news')->first();
        if (!$one){
            return false;
        }else{
            return $one;
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public static function getMenu(Request $request)
    {
        $language = self::getLang($request);
        $menus = Menu::where('parent_id', null)->where('lang', $language)->orderBy('position', 'asc')->get();
        return $menus;
    }

    public static function footer(Request $request)
    {
        $language = self::getLang($request);
        $footers = Footer::whereLang($language)->get();
        return $footers;
    }
}
