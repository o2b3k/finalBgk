<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/**
 * App\Models\Menu
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Menu[] $children
 * @property-read \App\Models\Menu $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu findSimilarSlugs($attribute, $config, $slug)
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $url
 * @property string|null $lang
 * @property int|null $position
 * @property string|null $model
 * @property int|null $model_id
 * @property string|null $type
 * @property int|null $published
 * @property int|null $parent_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUrl($value)
 */
class Menu extends Model
{
    use Sluggable;

    const ky = 'ky';
    const ru = 'ru';
    const en = 'en';
    const page = 'page';
    const category = 'category';
    const faction = 'faction';
    const album = 'album';
    const parent = 'parent';
    const children = 'children';

    public static function getModel($withName)
    {
        if ($withName){
            return [
                self::page => 'Страницы',
                self::category => 'Категория',
                self::faction => 'Партия',
                self::album => 'Альбом'
            ];
        }

        return [
            self::page,
            self::category,
            self::faction,
            self::album,
        ];
    }

    public static function getLanguage($withName)
    {
        if ($withName){
            return [
                self::ru => 'Русский',
                self::ky => 'Кыргызча',
                self::en => 'English',
            ];
        }
        return [
            self::ky,
            self::ru,
            self::en,
        ];
    }

    public static function typeMenu($withName)
    {
        if ($withName){
            return [
                self::parent => 'Родитель',
                self::children => 'Дочерный'
            ];
        }
        return [
            self::parent,
            self::children,
        ];
    }

    protected $table = 'menus';

    protected $fillable = ['name','slug','position',
        'model','type','published','lang','model_id','parent_id'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ]
        ];
    }

    public function children()
    {
        return $this->hasMany('App\Models\Menu','parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Menu');
    }

    public static function getMenu(Request $request)
    {
        $language = self::getLanguage($request);
        $menus = Menu::where('parent_id', null)->
            where('lang', $language)->orderBy('position', 'asc')->get();
        return $menus;
    }

    public static function getCurrentLanguage($request)
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $log = $request->route()->getPrefix();
        $language = substr($log, -2);
        if ($locale == 'en') {
            $locale = 'ru';
        }
        if ($language == '') {
            $language = $locale;
        }
        return $language;
    }
}
