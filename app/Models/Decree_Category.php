<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Decree_Category
 *
 * @property mixed $decree
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string|null $lang
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree_Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree_Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree_Category whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree_Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree_Category whereUpdatedAt($value)
 */
class Decree_Category extends Model
{
    const ru = 'ru';
    const ky = 'ky';
    const en = 'en';

    public static function getLanguage($withName)
    {
        if ($withName){
            return [
                self::ru => 'Русский',
                self::ky => 'Кыргызча',
                self::en => 'English',
            ];
        }
        return [
            self::ru,
            self::ky,
            self::en,
        ];
    }

    protected $table = 'decree__categories';

    protected $fillable = ['name','lang'];

    public function decree()
    {
        return $this->hasMany('App\Models\Decree','category_id');
    }
}
