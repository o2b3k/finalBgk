<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Document
 *
 * @property-read \App\Models\Category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document findSimilarSlugs($attribute, $config, $slug)
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $type
 * @property string|null $lang
 * @property string|null $desc
 * @property string|null $attachment
 * @property string|null $note
 * @property int $published
 * @property int $archive
 * @property int|null $category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereArchive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereUpdatedAt($value)
 */
class Document extends Model
{
    use Sluggable;

    const legislation = 'legislation';
    const draft_law = 'draft_law';
    const decree = 'decree';
    const document = 'document';
    const report = 'report';
    const ky = 'ky';
    const ru = 'ru';
    const en = 'en';

    public static function getType($withName)
    {
        if ($withName){
            return [
                self::legislation => 'Закон',
                self::draft_law => 'Проект закона',
                self::decree => 'Постановление',
                self::document => 'Обычный документ',
                self::report => 'Отчет',
            ];
        }
        return [
            self::legislation,
            self::draft_law,
            self::decree,
            self::document,
            self::report,
        ];
    }

    public static function getLanguage($withName)
    {
        if ($withName){
            return [
                self::ky => 'Кыргызча',
                self::ru => 'Русский',
                self::en => 'English',
            ];
        }
        return [
            self::ky,
            self::ru,
            self::en,
        ];
    }

    protected $table = 'documents';

    protected $fillable = ['name','slug','type','lang',
        'desc','attachment','note','published','archive','category_id'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
