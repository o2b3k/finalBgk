<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Commission[] $commission
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[] $document
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\News[] $news
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Page[] $pages
 * @property-read \App\Models\Category $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category findSimilarSlugs($attribute, $config, $slug)
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $slug
 * @property string|null $lang
 * @property int|null $published
 * @property int|null $parent_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereUpdatedAt($value)
 */
class Category extends Model
{
    use Sluggable;

    const TYPE_PARENT_CATEGORY = 'PARENT_CATEGORY';
    const TYPE_CHILD_CATEGORY = 'CHILD_CATEGORY';

    public static function getCategories($withName)
    {
        if ($withName){
            return [
                self::TYPE_PARENT_CATEGORY => 'Родительская категория',
                self::TYPE_CHILD_CATEGORY => 'Дочерняя категория',
            ];
        }

        return [
            self::TYPE_PARENT_CATEGORY,
            self::TYPE_CHILD_CATEGORY,
        ];
    }

    protected $table = 'categories';

    protected $fillable = ['name', 'slug', 'lang', 'published'];

    protected $hidden = ['created_at', 'updated_at', 'parent_id'];

    protected $guarded = ['type'];

    public function children()
    {
        return $this->hasMany('App\Models\Category','parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class,'parent_id');
    }

    public function pages()
    {
        return $this->hasMany('App\Models\Page');
    }

    public function news()
    {
        return $this->hasMany('App\Models\News');
    }

    public function document()
    {
        return $this->hasMany('App\Models\Document');
    }

    public function commission()
    {
        return $this->hasMany('App\Models\Commission');
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
