<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Position
 *
 * @property-read \App\Models\News $news
 * @mixin \Eloquent
 * @property int $id
 * @property string $position
 * @property int|null $news_id
 * @property string $lang
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereUpdatedAt($value)
 */
class Position extends Model
{
    const main_news = 'main_news';
    const second_news = 'second_news';
    const third_news = 'third_news';

    public static function getType($withName)
    {
        if ($withName){
            return [
                self::main_news => '1',
                self::second_news => '2',
                self::third_news => '3',
            ];
        }
        return [
            self::main_news,
            self::second_news,
            self::third_news,
        ];
    }

    protected $table = 'positions';

    protected $fillable = ['position','news_id','lang'];

    public function news()
    {
        return $this->belongsTo('App\Models\News');
    }
}
