<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Deputy
 *
 * @property-read \App\Models\Commission $commission
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Commission[] $commissions
 * @property-read \App\Models\Complain $complain
 * @property-read \App\Models\Faction $faction
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Faction[] $factions
 * @mixin \Eloquent
 * @property int $id
 * @property string $fio
 * @property string $profile
 * @property string|null $tel
 * @property string|null $email
 * @property string|null $fb
 * @property string|null $ok
 * @property string|null $ins
 * @property string|null $image
 * @property int|null $faction_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereFactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereFb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereIns($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereOk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereProfile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereUpdatedAt($value)
 * @property string $lang
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deputy whereLang($value)
 */
class Deputy extends Model
{
    protected $table = 'deputies';

    protected $fillable = ['fio','profile','tel','email','fb','ok','ins','image','faction_id','lang'];

    public function faction()
    {
        return $this->belongsTo('App\Models\Faction')->addSelect('id','name');
    }

    public function complain()
    {
        return $this->belongsTo('App\Models\Complain');
    }

    public function commission()
    {
        return $this->belongsTo('App\Models\Commission');
    }

    public function commissions()
    {
        return $this->belongsToMany('App\Models\Commission');
    }

    public function factions()
    {
        return $this->belongsToMany('App\Models\Faction');
    }

    public static function getExcerpt($str, $startPos = 0, $maxLength = 250) {
        if(strlen($str) > $maxLength) {
            $excerpt   = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= ' [...]';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }
}
