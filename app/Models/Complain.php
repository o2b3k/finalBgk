<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Complain
 *
 * @property-read \App\Models\Deputy $deputy
 * @mixin \Eloquent
 * @property int $id
 * @property string $fio
 * @property string $email
 * @property string|null $tel
 * @property string $subject
 * @property string $text
 * @property string|null $file
 * @property int|null $deputy_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereDeputyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Complain whereUpdatedAt($value)
 */
class Complain extends Model
{
    protected $table = 'complains';

    protected $fillable = ['fio','email','tel','subject','text','file','deputy_id'];

    public function deputy()
    {
        return $this->belongsTo('App\Models\Deputy');
    }
}