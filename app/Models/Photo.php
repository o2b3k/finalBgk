<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Photo
 *
 * @property-read \App\Models\Album $album
 * @property-read \App\Models\News $news
 * @mixin \Eloquent
 * @property int $id
 * @property string $path
 * @property string|null $desc
 * @property int|null $news_id
 * @property int|null $album_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereAlbumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereUpdatedAt($value)
 */
class Photo extends Model
{
    protected $table = 'photos';

    protected $fillable = ['path','desc'];

    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }

    public function news()
    {
        return $this->belongsTo('App\Models\News');
    }
}
