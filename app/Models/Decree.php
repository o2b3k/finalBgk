<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Decree
 *
 * @property mixed $category
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string|null $text
 * @property string|null $file
 * @property int|null $category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereUpdatedAt($value)
 */

class Decree extends Model
{
    protected $table = 'decrees';

    protected $fillable = ['name','text','file','category_id'];

    public function category()
    {
        return $this->belongsTo('App\Models\Decree_Category');
    }
}
