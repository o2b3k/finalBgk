<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Album
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Photo[] $photos
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string|null $desc
 * @property string|null $lang
 * @property int|null $published
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Album whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Album whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Album whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Album whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Album whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Album wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Album whereUpdatedAt($value)
 */
class Album extends Model
{
    const ky = 'ky';
    const ru = 'ru';
    const en = 'en';

    public static function getLanguage($withName)
    {
        if ($withName){
            return [
                self::ky => 'Кыргызча',
                self::ru => 'Русский',
                self::en => 'English',
            ];
        }
        return [
            self::ky,
            self::ru,
            self::en,
        ];
    }
    protected $table = 'albums';

    protected $fillable = ['name','desc','published'];

    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }
}
