<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Faction
 *
 * @property-read \App\Models\Deputy $deputy
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Deputy[] $members
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faction findSimilarSlugs($attribute, $config, $slug)
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $desc
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $deputy_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faction whereDeputyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faction whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faction whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faction whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faction whereUpdatedAt($value)
 */
class Faction extends Model
{
    use Sluggable;

    protected $table = 'factions';

    protected $fillable = ['name','slug','desc','deputy_id'];

    public function deputy()
    {
        return $this->belongsTo('App\Models\Deputy');
    }

    public function members()
    {
        return $this->belongsToMany('App\Models\Deputy',
            'faction_members','faction_id',
            'deputy_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
