<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Commission
 *
 * @property-read \App\Models\Category $category
 * @property-read \App\Models\Deputy $chairman
 * @property-read \App\Models\Deputy $deputy
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Deputy[] $members
 * @property-read \App\Models\Document $plan
 * @property-read \App\Models\Document $position
 * @property-read \App\Models\Document $reports
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission findSimilarSlugs($attribute, $config, $slug)
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int|null $chairman_id
 * @property int|null $deputy_id
 * @property int|null $position_id
 * @property int|null $plan_id
 * @property int|null $reports_id
 * @property int|null $news_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $category_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereChairmanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereDeputyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission wherePositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereReportsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commission whereUpdatedAt($value)
 */
class Commission extends Model
{
    use Sluggable;

    protected $table = 'commissions';

    protected $fillable = ['name', 'slug', 'chairman_id', 'deputy_id', 'position',
        'plan', 'reports', 'news', 'category_id'];

    public function deputy()
    {
        return $this->belongsTo('App\Models\Deputy','deputy_id');
    }

    public function chairman()
    {
        return $this->belongsTo('App\Models\Deputy','chairman_id');
    }

    public function position()
    {
        return $this->belongsTo('App\Models\Document','position_id');
    }

    public function plan()
    {
        return $this->belongsTo('App\Models\Document', 'plan_id');
    }

    public function reports()
    {
        return $this->belongsTo('App\Models\Document', 'reports_id');
    }

    public function members()
    {
        return $this->belongsToMany('App\Models\Deputy','commission_members','commission_id','deputy_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id');
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
