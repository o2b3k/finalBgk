<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Footer
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Link[] $link
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int|null $position
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Footer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Footer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Footer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Footer wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Footer whereUpdatedAt($value)
 * @property string $lang
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Footer whereLang($value)
 */
class Footer extends Model
{
    protected $table = 'footers';

    protected $fillable = ['name', 'position', 'lang'];

    public function link()
    {
        return $this->hasMany('App\Models\Link');
    }

    public static function getSlug($string)
    {
        $len = strlen($string);
        $str = substr($string,27,$len);
        return $str;
    }
}
