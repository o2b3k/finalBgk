<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page
 *
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page findSimilarSlugs($attribute, $config, $slug)
 * @mixin \Eloquent
 * @property string $name
 * @property string $text
 * @property string $lang
 * @property string|null $type
 * @property string|null $media
 * @property string|null $slug
 * @property int|null $published
 * @property int|null $category_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereMedia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt($value)
 */
class Page extends Model
{
    use Sluggable;

    const TYPE_IMAGE = 'IMAGE';
    const TYPE_VIDEO = 'VIDEO';
    const TYPE_EMPTY = 'EMPTY';
    const ky = 'ky';
    const ru = 'ru';
    const en = 'en';

    public static function getType($withName)
    {
        if ($withName){
            return [
                self::TYPE_EMPTY => '',
                self::TYPE_IMAGE => 'Изображения',
                self::TYPE_VIDEO => 'Видео',
            ];
        }

        return [
            self::TYPE_EMPTY,
            self::TYPE_IMAGE,
            self::TYPE_VIDEO,
        ];
    }

    public static function getLanguage($withName)
    {
        if ($withName){
            return [
                self::ru => 'Русский',
                self::ky => 'Кыргызча',
                self::en => 'English',
            ];
        }
        return [
            self::ru,
            self::ky,
            self::en,
        ];
    }

    protected $table = 'pages';

    protected $fillable = ['name','slug','text', 'lang',
        'media','published','type','category_id'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

}
