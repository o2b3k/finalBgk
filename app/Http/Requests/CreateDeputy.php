<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDeputy extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fio' => 'required',
            'profile' => 'required|min:60'
        ];
    }

    public function messages()
    {
        return [
            'fio.required' => 'ФИО обязательно',
            'profile.required' => 'Анкета обязательно',
            'profile.min' => 'Пожалуйста запольните'
        ];
    }
}
