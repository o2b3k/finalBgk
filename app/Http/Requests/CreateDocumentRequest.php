<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'text' => 'required|min:60'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя обязательно',
            'text.required' => 'Текст обязательно',
            'text.min' => 'Текст обязательно'
        ];
    }
}
