<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ComplainStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()){
            return true;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fio' => 'required|string|min:4',
            'email' => 'required|email',
            'subject' => 'required|min:4',
            'text' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ];
    }

    public function messages()
    {
        return [
            'fio.required' => 'ФИО обязательно',
            'fio.min' => 'ФИО обязательно',
            'email.required' => 'Email обязательно',
            'subject.required' => 'Тема обязательно',
            'subject.min' => 'Тема обязательно',
            'text.required' => 'Сообщение обязательно',
            'g-recaptcha-response.required' => ' Подтвердите что вы не робот'
        ];
    }
}
