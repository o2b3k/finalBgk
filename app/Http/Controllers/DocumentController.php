<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class DocumentController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Document::orderBy('name', 'asc')->paginate(10);
        return view('document.index', ['documents' => $documents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('document.add', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::find($request->input('category_id'));
        if ($request->file('attachment')) {
            $validator = Validator::make($request->all(), $this->fileRules(), $this->messages());
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $attachment = $request->file('attachment');
            $extension = $attachment->getClientOriginalExtension();
            $fileName = str_random(5) . "-" . date('his') . "-" . str_random(3) . "." . $extension;
            $path = $attachment->move('files/shares', $fileName);
            $document = new Document();
            $document->name = $request->input('name');
            $document->type = $request->input('type');
            $document->lang = $request->input('lang');
            $document->note = $request->input('note');
            $document->attachment = $path;
            $document->category()->associate($category);
            if ($document->save()) {
                $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно опубликлван');
            }
        } else {
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $document = new Document();
            $document->name = $request->input('name');
            $document->type = $request->input('type');
            $document->lang = $request->input('lang');
            $document->note = $request->input('note');
            $document->category()->associate($category);
            if ($document->save()) {
                $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно опубликован');
            }
        }
        return redirect()->route('document.Index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::find($id);
        $categories = Category::all();
        $data = [
            'document' => $document,
            'categories' => $categories,
        ];
        return view('document.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Support\Facades\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:250',
        ]);
        $document = Document::find($id);
        $document->name = $request->input('name');
        $document->type = $request->input('type');
        $document->category_id = $request->input('category_id');
        $document->lang = $request->input('lang');
        $document->note = $request->input('note');
        if ($request->file('attachment')) {
            $attachment = $request->file('attachment');
            $extension = $attachment->getClientOriginalExtension();
            $fileName = str_random(5) . "-" . date('his') . "-" . str_random(3) . "." . $extension;
            $path = $attachment->move('files/shares', $fileName);
            $document->attachment = $path;
        }
        $document->save();
        $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно изменено');
        return redirect()->route('document.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'document_id' => 'required|exists:documents,id',
        ]);
        $document = Document::find($request->input('document_id'));
        if ($document->delete()) {
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно удалено');
        }
        return redirect()->route('document.Index');
    }

    public function setPublished(Request $request)
    {
        $id = Input::get('id');
        $document = Document::find($id);
        if ($document->published) {
            $document->published = false;
            $document->save();
            $this->toast($request, $this->typeSuccess, $this->titleError, 'Документ успешно не опубликован');
        } else {
            $document->published = true;
            $document->save();
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно опубликован');
        }
        return redirect()->back();
    }

    public function setArchive(Request $request)
    {
        $id = Input::get('id');
        $document = Document::find($id);
        if ($document->archive) {
            $document->archive = false;
            $document->save();
            $this->toast($request, $this->typeSuccess, $this->titleError, 'Документ успешно не опубликован');
        } else {
            $document->archive = true;
            $document->save();
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно опубликован');
        }
        return redirect()->back();
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя документ обязательно',
            'category_id.required' => 'Категория обязательно',
            'note.required' => 'Текст обязательно',
        ];
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'category_id' => 'required',
            'note' => 'required',
        ];
    }

    public function fileRules()
    {
        return [
            'name' => 'required',
            'category_id' => 'required',
            'note' => 'required',
            'attachment' => 'required|file|mimes:doc,docx,pdf,ppt,pptx',
        ];
    }

    public function toast(Request $request, $type, $title, $message)
    {
        $request->session()->flash('status', [
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }

}
