<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Http\Request;
use Validator;

class LinkController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $footer
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $footer)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $link = new Link();
        $link->name = $request->input('name');
        $link->url = $request->input('url');
        $link->footer()->associate($footer);
        $link->save();
        return redirect()->back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  Link  $link
     * @return \Illuminate\Http\Response
     */
    public function edit(Link $link)
    {
        return view('footer.view',['link' => $link]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'link_id' => 'required|exists:links,id',
        ]);
        $link = Link::find($request->input('link_id'));
        if ($link->delete()){
            $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Ссылка успешно удалено');
        }
        return redirect()->back();
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя обязательно',
            'url.required' => 'Введите действующий ссылку',
        ];
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'url' => 'required|url',
        ];
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
