<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Category;
use App\Models\Commission;
use App\Models\Document;
use App\Models\Faction;
use App\Models\Menu;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class MenuController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::where('parent_id',null)->get();
        return view('menu.index', ['menus' => $menus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = Page::all();
        $factions = Faction::all();
        $categories = Category::all();
        $albums = Album::all();
        $menus = Menu::where('parent_id',null)->get();
        $data = [
            'pages' => $pages,
            'factions' => $factions,
            'categories' => $categories,
            'menus' => $menus,
            'albums' => $albums,
        ];
        return view('menu.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('type') == Menu::parent){
            $request->validate([
                'name' => 'required',
                'lang' => 'required',
                'type' => 'required',
            ]);
            $menu = new Menu();
            $menu->name = $request->input('name');
            $menu->position = $request->input('position');
            $menu->lang = $request->input('lang');
            $menu->type = $request->input('type');
            $menu->url = $request->input('url');
            if ($request->input('model') != null){
                $menu->model = $request->input('model');
                $menu->model_id = $request->input('model_id');
            }
            $menu->save();
        }
        if ($request->input('type') == Menu::children){
            $request->validate([
                'name' => 'required',
                'lang' => 'required',
                'type' => 'required',
                'parent_id' => 'required',
            ]);
            $menu = new Menu();
            $menu->name = $request->input('name');
            $menu->position = $request->input('position');
            $menu->lang = $request->input('lang');
            $menu->type = $request->input('type');
            $menu->url = $request->input('url');
            $menu->model = $request->input('model');
            $menu->model_id = $request->input('model_id');
            $menu->parent_id = $request->input('parent_id');
            $menu->save();
        }
        if ($request->input('lang') == 'ru'){
            return redirect()->route('menu.Russian');
        }
        if ($request->input('lang') == 'ky'){
            return redirect()->route('menu.Kyrgyz');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        return view('menu.view',['menu' => $menu]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);
        $pages = Page::all();
        $factions = Faction::all();
        $categories = Category::all();
        $albums = Album::all();
        $menus = Menu::where('parent_id',null)->get();
        $data = [
            'pages' => $pages,
            'factions' => $factions,
            'categories' => $categories,
            'menus' => $menus,
            'menu' => $menu,
            'albums' => $albums,
        ];
        return view('menu.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::find($id);
        $request->validate([
            'name' => 'required',
            'lang' => 'required',
        ]);
        $menu->name = $request->input('name');
        $menu->position = $request->input('position');
        $menu->lang = $request->input('lang');
        $menu->type = $request->input('type');
        $menu->parent_id = $request->input('parent_id');
        $menu->url = $request->input('url');
        $menu->model = $request->input('model');
        if ($request->input('model_id') !== null){
            $menu->model_id = $request->input('model_id');
        }
        $menu->save();
        if ($menu->lang == 'ru'){
            return redirect()->route('menu.Russian');
        }
        if ($menu->lang == 'ky'){
            return redirect()->route('menu.Kyrgyz');
        }
        return redirect()->route('menu.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'menu_id' => 'required|exists:menus,id',
        ]);
        $menu = Menu::find($request->input('menu_id'));
        if ($menu->children->count() > 0){
            $this->toast($request,$this->typeError,$this->titleError,'Невозможно удалить родителский меню');
            return redirect()->back();
        }
        if ($menu->delete()){
            $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Меню успешно удалено');
        }
        if ($menu->lang == 'ru'){
            return redirect()->route('menu.Russian');
        }
        if ($menu->lang == 'ky'){
            return redirect()->route('menu.Kyrgyz');
        }
    }

    public function menuRu()
    {
        $menus = Menu::where('lang','ru')->where('parent_id', null)
            ->orderBy('position','asc')->get();
        return view('menu.index',['menus' => $menus]);
    }

    public function menuKy()
    {
        $menus = Menu::where('lang','ky')->where('parent_id', null)
            ->orderBy('position','asc')->get();
        return view('menu.index',['menus' => $menus]);
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
