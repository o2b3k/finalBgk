<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Input;
class PagesController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::paginate(10);
        return view('pages.index',['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('pages.add',['category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $category = Category::find($request->input('category_id'));
        $mediaType = $request->input('type');
        if ($mediaType == Page::TYPE_VIDEO)
        {
            $request->validate([
                'name' => 'required',
                'video' => 'required',
                'text' => 'required',
                'category_id' => 'required',
            ]);
            $pages = new Page([
                'name' => $request->input('name'),
                'text' => $request->input('text'),
                'media'=> $request->input('video'),
                'lang' => $request->input('lang'),
                'type' => $mediaType,
                'category_id' => $category->id,
            ]);
            $pages->save();
        }
        if ($mediaType == Page::TYPE_IMAGE){
            $request->validate([
                'name' => 'required',
                'text' => 'required',
                'image' => 'required|file',
                'category_id' => 'required',
            ]);
            $path = $this->pathImage($request);
            $pages = new Page([
                'name' => $request->input('name'),
                'text' => $request->input('text'),
                'lang' => $request->input('lang'),
                'media' =>$path,
                'type' => $mediaType,
                'category_id' => $category->id,
            ]);
            $pages->save();
        }
        if ($mediaType == Page::TYPE_EMPTY){
            $request->validate([
                'name' => 'required',
                'text' => 'required',
                'category_id' => 'required',
            ]);
            $pages = new Page([
                'name' => $request->input('name'),
                'text' => $request->input('text'),
                'lang' => $request->input('lang'),
                'type' => $mediaType,
                'category_id' => $category->id,
            ]);
            $pages->save();
        }
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Страница успешно опубликован');
        return redirect()->route('pages.Index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Page $page
     */
    public function edit($id)
    {
        $pages = Page::find($id);
        $category = Category::all();
        $data = [
            'pages' => $pages,
            'category' => $category
        ];
        return view('pages.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Page $page
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $pages = Page::find($id);
        if ($pages->type == Page::TYPE_VIDEO){
            $pages->name = $request->input('name');
            $pages->media = $request->input('media');
            $pages->text = $request->input('text');
            $pages->category_id = $request->input('category_id');
            $pages->lang = $request->input('lang');
            $pages->save();
            $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Страницу успешно изменено');
        }
        if ($pages->type == Page::TYPE_EMPTY){
            $pages->name = $request->input('name');
            $pages->text = $request->input('text');
            $pages->category_id = $request->input('category_id');
            $pages->lang = $request->input('lang');
            $pages->save();
            $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Страницу успешно изменено');
        }
        if ($pages->type == Page::TYPE_IMAGE){
            if ($request->input('image')){
                $path = $this->pathImage($request);
                $pages->name = $request->input('name');
                $pages->text = $request->input('text');
                $pages->category_id = $request->input('category_id');
                $pages->lang = $request->input('lang');
                $pages->media = $path;
                $pages->save();
                $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Страницу успешно изменено');
            }
            $pages->name = $request->input('name');
            $pages->text = $request->input('text');
            $pages->category_id = $request->input('category_id');
            $pages->lang = $request->input('lang');
            $pages->save();
            $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Страницу успешно изменено');
        }
        return redirect()->route('pages.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Page $page
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(),['pages_id' => 'required|exists:pages,id']);
        if ($validator->fails()){
            $this->toast($request,$this->typeError,$this->titleError,'Страницу не найдена');
            return redirect()->back();
        }
        $pages = Page::find($request->input('pages_id'));
        $pages->delete();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Страницу удалена успешно');
        return redirect()->route('pages.Index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setPublished(Request $request)
    {
        $id = Input::get('id');
        $pages = Page::find($id);
        if ($pages->published){
            $pages->published = false;
            $pages->save();
        }else{
            $pages->published = true;
            $pages->save();
        }
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Страницу успешно опубликован');
        return redirect()->back();
    }

    public function getValidationRules()
    {
        $rules = [
            'name' => ['required','max:100'],
            'text' => ['required'],
            'type' => [
                'required',
                Rule::in(Page::getType(false)),
            ],
        ];
    }

    public function pathImage(Request $request)
    {
        $image = $request->file('image');
        $size = getimagesize($image->path());
        if (!$size || count($size) < 2) {
            $this->toast($request,$this->typeError,$this->titleError,'Не удалось прочитать размер изображения');
            return redirect()->back();
        }
        $fileName = $image->getClientOriginalName();
        $path = $image->move('files/shares', $fileName);
        return $path;
    }

    public function messages()
    {
        return [
            'name.required' => 'Заголовок страницы обязательно',
            'category_id.required' => 'Категория обязательно',
            'text.required' => 'Текст обязательно',
        ];
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'category_id' => 'required',
            'text' => 'required',
        ];
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
