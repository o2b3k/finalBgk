<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use App\Models\Complain;
use App\Models\Deputy;
use App\Models\Document;
use App\Models\Faction;
use App\Models\News;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $news = News::all()->count();
        $faction = Faction::all()->count();
        $deputy = Deputy::all()->count();
        $document = Document::all()->count();
        $complain = Complain::all()->count();
        $commission = Commission::all()->count();
        $data = [
            'news' => $news,
            'faction' => $faction,
            'deputy' => $deputy,
            'document' => $document,
            'complain' => $complain,
            'commission' => $commission,
        ];
        return view('dashboard.index', $data);
    }
}
