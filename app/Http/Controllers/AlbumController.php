<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class AlbumController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::all();
        return view('album.index', ['albums' => $albums]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('album.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $album = new Album();
        $album->name = $request->input('name');
        $album->desc = $request->input('desc');
        $album->save();
        $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Альбом успешно сохранено');
        return redirect()->route('album.Index');
    }

    public function view($id)
    {
        $album = Album::find($id);
        return view('album.view', ['album' => $album]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $album = Album::find($id);
        $albums = Album::all();
        if (!$album) {
            $this->toast($request, $this->typeError, $this->titleError, 'Категория не найдена');
            return redirect()->back();
        }
        $data = [
            'albums' => $albums,
            'album_one' => $album
        ];
        return view('album.index', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $album = Album::findOrFail($id);
        $album->name = $request->input('name');
        $album->desc = $request->input('desc');
        $album->save();

        $this->toast($request, $this->titleSuccess, $this->titleSuccess, 'Альбом успешно изменено');
        return redirect()->route('album.Index');
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), ['album_id' => 'required|exists:albums,id']);
        if ($validator->fails()) {
            $this->toast($request, $this->typeError, $this->titleError, 'Альбом не найден');
            return redirect()->back();
        }
        $album = Album::find($request->input('album_id'));
        if (!empty($album->photos)) {
            foreach ($album->photos as $photo) {
                if (!empty($photo->path)) {
                    if (file_exists($photo->path)) {
                        unlink($photo->path);
                        $photo->delete();
                    }
                }
            }
        }
        $album->delete();
        $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Альбом успешно удален');
        return redirect()->route('album.Index');
    }

    public function uploadPhoto(Request $request, $id)
    {
        $album = Album::find($id);
        $path = $this->pathImage($request);
        $photo = new Photo();
        $photo->path = $path;
        $photo->desc = $request->input('desc');
        $photo->album()->associate($album);
        $photo->save();
        return redirect()->back();
    }

    public function destroyPhoto($id)
    {
        $photo = Photo::find($id);
        if (file_exists($photo->path)) {
            unlink($photo->path);
        }
        $photo->delete();
        return redirect()->back();
    }

    public function setPublished(Request $request)
    {
        $id = Input::get('id');
        $album = Album::find($id);
        if ($album->published) {
            $album->published = false;
            $album->save();
            $this->toast($request, $this->typeSuccess, $this->titleError, 'Альбом успешно не опубликован');
        } else {
            $album->published = true;
            $album->save();
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Альбом успешно опубликован');
        }
        return redirect()->back();
    }

    public function pathImage(Request $request)
    {
        $image = $request->file('image');
        $size = getimagesize($image->path());
        if (!$size || count($size) < 2) {
            $this->toast($request, $this->typeError, $this->titleError, 'Не удалось прочитать размер изображения');
            return redirect()->back();
        }
        $extension = $image->getClientOriginalExtension();
        $fileName = str_random(5) . "-" . date('his') . "-" . str_random(3) . "." . $extension;
        $path = $image->move('files/shares', $fileName);
        return $path;
    }

    public function toast(Request $request, $type, $title, $message)
    {
        $request->session()->flash('status', [
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}