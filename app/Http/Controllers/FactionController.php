<?php

namespace App\Http\Controllers;

use App\Models\Deputy;
use App\Models\Faction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class FactionController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $factions = Faction::all();
        return view('faction.index',['factions' => $factions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $deputies = Deputy::all();
        return view('faction.add', ['deputies' => $deputies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $deputy = Deputy::find($request->input('deputy_id'));
        $faction = new Faction();
        $faction->name = $request->input('name');
        $faction->desc = $request->input('desc');
        if ($deputy){
            $faction->deputy_id = $deputy->id;
        }
        $faction->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Партия успешно опубликован');
        return redirect()->route('faction.Index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return Response
     */
    public function edit($id)
    {
        $faction = Faction::find($id);
        $deputies = Deputy::all();
        $data = [
            'faction' => $faction,
            'deputies' => $deputies,
        ];
        return view('faction.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $faction = Faction::find($id);
        $deputy = Deputy::find($request->input('deputy_id'));
        $faction->name = $request->input('name');
        $faction->desc = $request->input('desc');
        $faction->deputy_id = $deputy->id;
        if ($faction->save()){
            $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Партия успешно изменено');
        }
        return redirect()->route('faction.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'faction_id' => 'required|exists:factions,id',
        ]);
        $faction = Faction::find($request->input('faction_id'));
        $faction->delete();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Партия успешно удалено');
        return redirect()->route('faction.Index');
    }

    public function addMemberForm(Faction $faction)
    {
        $deputy = Deputy::all();
        $data = [
            'deputies' => $deputy,
            'faction' => $faction
        ];
        return view('faction.addMember', $data);
    }

    public function addMember(Request $request, Faction $faction)
    {
        $deputy = Deputy::find($request->input('deputy_id'));
        $faction->members()->attach($deputy->id);
        $faction->save();
        return redirect()->back();
    }

    public function deleteMember(Faction $faction,$id)
    {
        $deputy = Deputy::find($id);
        $faction->members()->detach($deputy->id);
        $faction->save();
        return redirect()->back();
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }

}
