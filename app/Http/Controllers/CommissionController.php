<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Commission;
use App\Models\Deputy;
use App\Models\Document;
use Illuminate\Http\Request;
use Validator;

class CommissionController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commissions = Commission::all();
        return view('commission.index',['commissions' => $commissions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $deputies = Deputy::all();
        $documents = Document::all();
        $category = Category::all();
        $data = [
            'deputies' => $deputies,
            'documents' => $documents,
            'categories' => $category,
        ];
        return view('commission.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $commission = new Commission();
        $commission->name = $request->input('name');
        $commission->chairman_id = $request->input('chairman_id');
        $commission->deputy_id = $request->input('deputy_id');
        $commission->position_id = $request->input('position');
        $commission->plan_id = $request->input('plan');
        $commission->reports_id = $request->input('reports');
        $commission->category_id = $request->input('category');
        $commission->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Комиссии успешно опубликован');
        return redirect()->route('commission.Index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function edit(Commission $commission)
    {
        $deputies = Deputy::all();
        $documents = Document::all();
        $categories = Category::all();
        $data = [
            'deputies' => $deputies,
            'documents' => $documents,
            'commission' => $commission,
            'categories' => $categories,
        ];
        return view('commission.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commission $commission)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $commission->name = $request->input('name');
        $commission->chairman_id = $request->input('chairman_id');
        $commission->deputy_id = $request->input('deputy_id');
        $commission->position_id = $request->input('position');
        $commission->plan_id = $request->input('plan');
        $commission->reports_id = $request->input('reports');
        $commission->category_id = $request->input('category');
        $commission->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Комиссия успешно изменено');
        return redirect()->route('commission.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'commission_id' => 'required|exists:commissions,id',
        ]);
        $commission = Commission::find($request->input('commission_id'));
        if ($commission->delete()){
            $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Комиссия успешно удалено');
        }
        return redirect()->route('commission.Index');
    }

    public function addMemberForm(Commission $commission)
    {
        $deputies = Deputy::all();
        $data = [
            'deputies' => $deputies,
            'commission' => $commission
        ];
        return view('commission.addMember',$data);
    }

    public function addMember(Request $request, $id)
    {
        $commission = Commission::find($id);
        $deputy = Deputy::find($request->input('deputy_id'));
        $commission->members()->attach($deputy->id);
        $commission->save();
        return redirect()->back();
    }

    public function deleteMember(Commission $commission,$id)
    {
        $deputy = Deputy::find($id);
        $commission->members()->detach($deputy);
        $commission->save();
        return redirect()->back();
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }

    public function messages()
    {
        return [
            'name.required' => 'Название фракция обязательно',
            'category_id.required' => 'Категория обязательно',
            'chairman_id.required' => 'Председатель обязательно',
        ];
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
}
