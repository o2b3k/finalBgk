<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\News;
use App\Models\Photo;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Validator;

class NewsController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return view('news.index', ['news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     * @param $lang
     * @return \Illuminate\Http\Response
     */
    public function create($lang)
    {
        $categories = Category::all();
        $data = [
            'categories' => $categories,
            'lang' => $lang
        ];
        return view('news.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $category = Category::find($request->input('category_id'));
        $news = new News();
        $news->title = $request->input('title');
        $news->text = $request->input('text');
        $news->lang = $request->input('lang');
        if ($request->file('image')) {
            $path = $this->pathImage($request);
            $news->image = $path;
        }
        $news->created_at = $request->input('created_at');
        $news->category()->associate($category);
        $news->save();
        $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Новости успешно опубликован');
        if ($request->input('lang') == "ru") {
            return redirect()->route('news.Ru');
        } else {
            return redirect()->route('news.Ky');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Response
     */
    public function uploadForm($id)
    {
        $news = News::find($id);
        return view('news.upload', ['news' => $news]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function uploadPhoto(Request $request, $id)
    {
        $news = News::find($id);
        $path = $this->pathImage($request);
        $photo = new Photo();
        $photo->path = $path;
        $photo->desc = $request->input('desc');
        $photo->news()->associate($news);
        $photo->save();
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        $categories = Category::all();
        $data = [
            'news' => $news,
            'categories' => $categories,
        ];
        return view('news.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $news = News::find($id);
        $news->title = $request->input('title');
        $news->category_id = $request->input('category_id');
        $news->lang = $request->input('lang');
        $news->text = $request->input('text');
        $news->created_at = $request->input('created_at');
        if ($request->input('home') != "null") {
            $home = $request->input('home');
            $positions = Position::where('lang', $news->lang)->where('position', $home)->first();
            if ($positions == null) {
                $position = new Position();
                $position->position = $home;
                $position->news_id = $news->id;
                $position->lang = $request->input('lang');
                $position->save();
            } else {
                $positions->position = $home;
                $positions->news_id = $news->id;
                $positions->save();
            }
        }

        if ($request->file('image')) {
            $path = $this->pathImage($request);
            if (file_exists($news->image)) {
                unlink($news->image);
            }
            $news->image = $path;
        }
        $news->save();
        $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Статья успешно изменоно');
        if ($news->lang == 'ru') {
            return redirect()->route('news.Ru');
        } else {
            return redirect()->route('news.Ky');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), ['news_id' => 'required|exists:news,id']);
        if ($validator->fails()) {
            $this->toast($request, $this->typeError, $this->titleError, 'Новости не найдена');
            return redirect()->back();
        }
        $news = News::find($request->input('news_id'));
        if (!empty($news->photos)) {
            foreach ($news->photos as $photo) {
                if (!empty($photo->path)) {
                    if (file_exists($photo->path)) {
                        unlink($photo->path);
                        $photo->delete();
                    }
                }
            }
        }
        $news->delete();
        $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Статья успешно удалено');
        return redirect()->back();
    }

    public function setPublished()
    {
        $id = Input::get('id');
        $news = News::find($id);
        if ($news->published) {
            $news->published = false;
            $news->save();
            return redirect()->back();
        } else {
            $news->published = true;
            $news->save();
            return redirect()->back();
        }
    }

    public function setArchive()
    {
        $id = Input::get('id');
        $news = News::find($id);
        if ($news->archive) {
            $news->archive = false;
            $news->save();
            return redirect()->back();
        } else {
            $news->archive = true;
            $news->save();
            return redirect()->back();
        }
    }

    public function newsRu()
    {
        $news = News::where('lang', 'ru')->orderBy('created_at', 'desc')->paginate(8);
        $data = [
            'news' => $news,
            'lang' => 'ru'
        ];
        return view('news.index', $data);
    }

    public function newsKy()
    {
        $news = News::where('lang', 'ky')->orderBy('created_at', 'desc')->paginate(8);
        $data = [
            'news' => $news,
            'lang' => 'ky'
        ];
        return view('news.index', $data);
    }

    public function getValidationFactory()
    {
        $rules = [
            'title' => ['required', 'max:250'],
            'text' => ['required'],
        ];
        return $rules;
    }

    public function pathImage(Request $request)
    {
        $image = $request->file('image');
        $size = getimagesize($image->path());
        if (!$size || count($size) < 2) {
            $this->toast($request, $this->typeError, $this->titleError, 'Не удалось прочитать размер изображения');
            return redirect()->back();
        }
        $extension = $image->getClientOriginalExtension();
        $fileName = str_random(5) . "-" . date('his') . "-" . str_random(3) . "." . $extension;
        $path = $image->move('files/shares', $fileName);
        return $path;
    }

    public function messages()
    {
        return [
            'title.required' => 'Заголовок новости обязательно',
            'category_id.required' => 'Категория обязательно',
            'text.required' => 'Текст обязательно',
        ];
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'category_id' => 'required',
            'text' => 'required',
        ];
    }

    public function toast(Request $request, $type, $title, $message)
    {
        $request->session()->flash('status', [
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
