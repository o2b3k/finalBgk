<?php

namespace App\Http\Controllers;

use App\Models\Footer;
use App\Models\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;

class FooterController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRu()
    {
        $footers = Footer::whereLang('ru')->get();
        return view('footer.index', ['footers' => $footers]);
    }

    public function indexKy()
    {
        $footers = Footer::whereLang('ky')->get();
        return view('footer.index', ['footers' => $footers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('footer.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {;
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $footer = new Footer();
        $footer->name = $request->input('name');
        $footer->position = $request->input('position');
        $footer->lang = $request->input('lang');
        $footer->save();
        if ($footer->lang == "ru"){
            return redirect()->route('footer.IndexRu');
        }else{
            return redirect()->route('footer.IndexKy');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Footer  $footer
     * @return \Illuminate\Http\Response
     */
    public function show(Footer $footer)
    {
        return view('footer.view', ['footer' => $footer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Footer  $footer
     * @return \Illuminate\Http\Response
     */
    public function edit(Footer $footer)
    {
        return view('footer.edit', ['footer' => $footer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Footer  $footer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Footer $footer)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $footer->name = $request->input('name');
        $footer->lang = $request->input('lang');
        $footer->save();
        return redirect()->route('footer.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'footer_id' => 'required|exists:footers,id',
        ]);
        $footer = Footer::find($request->input('footer_id'));
        if ($footer->delete()){
            $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Футер успешно удалено');
        }
        return redirect()->route('footer.Index');
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя обязательно',
        ];
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }


    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
