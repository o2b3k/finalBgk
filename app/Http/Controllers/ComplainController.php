<?php

namespace App\Http\Controllers;

use App\Mail\ComplainMail;
use App\Models\Complain;
use App\Models\Deputy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ComplainController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complain = Complain::orderByDesc('id')->paginate(10);
        return view('complain.index', ['complains' => $complain]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fio' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'text' => 'required',
        ]);
        $deputy = Deputy::find($request->input('deputy_id'));
        $complain = new Complain();
        $complain->fio = $request->input('fio');
        $complain->email = $request->input('email');
        $complain->tel = $request->input('tel');
        $complain->subject = $request->input('subject');
        $complain->text = $request->input('text');
        if ($request->file('file')) {
            $attachment = $request->file('file');
            $extension = $attachment->getClientOriginalExtension();
            $fileName = str_random(5) . "-" . date('his') . "-" . str_random(3) . "." . $extension;
            $path = $attachment->move('files/shares', $fileName);
            $complain->file = $path;
        }
        $complain->deputy()->associate($deputy);
        if ($complain->save()) {
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Письмо успешно отправлено');
        }
        Mail::to(env('BGK_EMAIL'))->send(new ComplainMail($complain->fio,$complain->email,$complain->subject,$complain->text));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  Complain $complain
     * @return \Illuminate\Http\Response
     */
    public function show(Complain $complain)
    {
        return view('complain.view', ['complain' => $complain]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'complain_id' => 'required|exists:complains,id',
        ]);
        $complain = Complain::find($request->input('complain_id'));
        if ($complain->delete()) {
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Обрашение успешно удалено');
        }
        return redirect()->route('complain.Index');
    }

    public function toast(Request $request, $type, $title, $message)
    {
        $request->session()->flash('status', [
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
