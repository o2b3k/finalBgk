<?php

namespace App\Http\Controllers;

use App\Models\Decree_Category;
use Illuminate\Http\Request;
use Validator;

class DecreeCategoryController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Decree_Category::all();
        return view('decree.category.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('decree.category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $category = new Decree_Category();
        $category->name = $request->input('name');
        $category->lang = $request->input('lang');
        $category->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Катугория успешно добавлено');
        return redirect()->route('resolution.Index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param Request $request
     * @param  int id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $category = Decree_Category::find($id);
        if (!$category){
            $this->toast($request,$this->typeError,$this->titleError,'Категория не найдено');
            return redirect()->route('decree.CategoryIndex');
        }
        return view('decree.category.add',['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $category = Decree_Category::find($id);
        $category->name = $request->input('name');
        $category->lang = $request->input('lang');
        $category->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Категория успешно изменено');
        return redirect()->route('resolution.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'category_id' => 'required|exists:decree__categories,id',
        ]);
        $category = Decree_Category::find($request->input('category_id'));
        $category->delete();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Категория успешно удалено');
        return redirect()->route('resolution.Index');
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя обязательно'
        ];
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
