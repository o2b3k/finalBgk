<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDocumentRequest;
use App\Models\Decree;
use App\Models\Decree_Category;
use Illuminate\Http\Request;
use Validator;

class DecreeController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Decree::orderBy('name', 'asc')->paginate(10);
        return view('decree.document.index', ['documents' => $documents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Decree_Category::all();
        return view('decree.document.add', ['categories' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateDocumentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDocumentRequest $request)
    {
        $category = Decree_Category::find($request->input('category_id'));
        if ($request->file('document') != null) {
            $file = $request->file('document');
            $extension = $file->getClientOriginalExtension();
            $fileName = str_random(5) . "-" . date('his') . "-" . str_random(3) . "." . $extension;
            $path = $file->move('files/shares', $fileName);
            $document = new Decree();
            $document->name = $request->input('name');
            $document->text = $request->input('text');
            $document->file = $path;
            $document->created_at = $request->input('created_at');
            $document->category()->associate($category);
            $document->save();
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно сохранено');
            return redirect()->route('decree.Index');
        } else {
            $document = new Decree();
            $document->name = $request->input('name');
            $document->text = $request->input('text');
            $document->category()->associate($category);
            $document->save();
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно сохранено');
            return redirect()->route('decree.Index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Decree $decree
     * @return \Illuminate\Http\Response
     */
    public function show(Decree $decree)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Decree $decree
     * @return \Illuminate\Http\Response
     */
    public function edit(Decree $decree)
    {
        $categories = Decree_Category::all();
        $data = [
            'categories' => $categories,
            'decree' => $decree,
        ];
        return view('decree.document.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Decree $decree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Decree $decree)
    {
        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $category = Decree_Category::find($request->input('category_id'));
        if ($request->file('document') != null) {
            $file = $request->file('document');
            $extension = $file->getClientOriginalExtension();
            $fileName = str_random(5) . "-" . date('his') . "-" . str_random(3) . "." . $extension;
            $path = $file->move('files/shares', $fileName);
            $decree->name = $request->input('name');
            $decree->text = $request->input('text');
            $decree->file = $path;
            $decree->created_at = $request->input('created_at');
            $decree->category()->associate($category);
            $decree->save();
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно сохранено');
            return redirect()->route('decree.Index');
        } else {
            $decree->name = $request->input('name');
            $decree->text = $request->input('text');
            $decree->created_at = $request->input('created_at');
            $decree->category()->associate($category);
            $decree->save();
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно сохранено');
            return redirect()->route('decree.Index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'decree_id' => 'required|exists:decrees,id',
        ]);
        $decree = Decree::find($request->input('decree_id'));
        $decree->delete();
        $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Документ успешно удалено');
        return redirect()->route('decree.Index');
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя обязательно',
        ];
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }

    public function toast(Request $request, $type, $title, $message)
    {
        $request->session()->flash('status', [
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
