<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeputy;
use App\Models\Deputy;
use App\Models\Faction;
use Illuminate\Http\Request;

class DeputyController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRu()
    {
        $deputies = Deputy::whereLang('ru')->orderBy('fio', 'asc')->paginate(10);
        return view('deputy.index', ['deputies' => $deputies]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexKy()
    {
        $deputies = Deputy::whereLang('ky')->orderBy('fio', 'asc')->paginate(10);
        return view('deputy.index', ['deputies' => $deputies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $factions = Faction::all();
        return view('deputy.add', ['factions' => $factions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateDeputy $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDeputy $request)
    {
        if ($request->file('image') != null) {
            $path = $this->pathImage($request);
        } else {
            $path = "default/deputy.png";
        }
        if ($request->input('faction_id') !== "null") {
            $faction = Faction::find($request->input('faction_id'));
            $deputy = new Deputy();
            $deputy->fio = $request->input('fio');
            $deputy->tel = $request->input('tel');
            $deputy->email = $request->input('email');
            $deputy->fb = $request->input('fb');
            $deputy->ins = $request->input('ins');
            $deputy->image = $path;
            $deputy->profile = $request->input('profile');
            $deputy->faction_id = $faction->id;
            $deputy->lang = $request->input('lang');
            if ($deputy->save()) {
                $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Депутат успешно создан');
            }
        } else {
            $deputy = new Deputy();
            $deputy->fio = $request->input('fio');
            $deputy->tel = $request->input('tel');
            $deputy->email = $request->input('email');
            $deputy->fb = $request->input('fb');
            $deputy->ins = $request->input('ins');
            $deputy->image = $path;
            $deputy->profile = $request->input('profile');
            $deputy->lang = $request->input('lang');
            if ($deputy->save()) {
                $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Депутат успешно создан');
            }
        }
        if ($deputy->lang == "ru"){
            return redirect()->route('deputy.RuIndex');
        }else{
            return redirect()->route('deputy.KyIndex');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Deputy $deputy
     * @return \Illuminate\Http\Response
     */
    public function edit(Deputy $deputy)
    {
        $factions = Faction::all();
        $data = [
            'factions' => $factions,
            'deputy' => $deputy,
        ];
        return view('deputy.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Deputy $deputy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deputy $deputy)
    {
        $request->validate([
            'fio' => 'required',
            'profile' => 'required',
        ]);
        if ($request->file('image')) {
            $path = $this->pathImage($request);
            $deputy->fio = $request->input('fio');
            $deputy->tel = $request->input('tel');
            $deputy->email = $request->input('email');
            $deputy->fb = $request->input('fb');
            $deputy->ins = $request->input('ins');
            $deputy->profile = $request->input('profile');
            $deputy->faction_id = $request->input('faction_id');
            $deputy->image = $path;
            $deputy->lang = $request->input('lang');
        } else {
            $deputy->fio = $request->input('fio');
            $deputy->tel = $request->input('tel');
            $deputy->email = $request->input('email');
            $deputy->fb = $request->input('fb');
            $deputy->ins = $request->input('ins');
            $deputy->profile = $request->input('profile');
            $deputy->faction_id = $request->input('faction_id');
            $deputy->lang = $request->input('lang');
        }
        if ($deputy->save()) {
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Депутат успешно изменено');
        }

        if ($deputy->lang == "ru"){
            return redirect()->route('deputy.RuIndex');
        }else{
            return redirect()->route('deputy.KyIndex');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'deputy_id' => 'required|exists:deputies,id',
        ]);
        $deputy = Deputy::find($request->input('deputy_id'));
        if ($deputy->delete()) {
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Депутат успешно удалено');
        }
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $type
     * @param $title
     * @param $message
     */
    public function toast(Request $request, $type, $title, $message)
    {
        $request->session()->flash('status', [
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }

    public function pathImage(Request $request)
    {
        $image = $request->file('image');
        $size = getimagesize($image->path());
        if (!$size || count($size) < 2) {
            $this->toast($request, $this->typeError, $this->titleError, 'Не удалось прочитать размер изображения');
            return redirect()->back();
        }
        $extension = $image->getClientOriginalExtension();
        $fileName = str_random(5) . "-" . date('his') . "-" . str_random(3) . "." . $extension;
        $path = $image->move('files/shares', $fileName);
        return $path;
    }
}
