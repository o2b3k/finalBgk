<?php

namespace App\Http\Controllers;

use App\Http\Requests\ComplainStoreRequest;
use App\Mail\ComplainMail;
use App\Models\Album;
use App\Models\Category;
use App\Models\Commission;
use App\Models\Complain;
use App\Models\Decree;
use App\Models\Decree_Category;
use App\Models\Deputy;
use App\Models\Document;
use App\Models\Faction;
use App\Models\Menu;
use App\Models\News;
use App\Models\Page;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class FrontendController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $news = News::where('published', '1')->
        where('lang', $language)->orderByDesc('created_at')->limit(4)->get();
        $deputies = Deputy::all();
        $documents = Document::where('type', '=', 'decree')->orderByDesc('created_at')->paginate(10);
        $position = Position::where('lang', $language)->get();
          
        $factions = Faction::all();
        $data = [
            'news' => $news,
            'deputies' => $deputies,
            'documents' => $documents,
            'request' => $request,
            'factions' => $factions,
            'position' => $position,
        ];
        return view('frontend.index', $data);
    }

    public function viewNews(Request $request, $slug)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $news = News::where('slug', $slug)->first();
        if (!$news) {
            abort(404);
        }
        $news->count = $news->count + 1;
        $news->save();
        $menus = Menu::where('parent_id', null)->
        where('lang', $language)->orderBy('position', 'asc')->get();
        $data = [
            'news' => $news,
            'menus' => $menus,
            'request' => $request
        ];
        return view('frontend.newsView', $data);
    }

    public function changeRoute(Request $request)
    {
        $route = $request->path();
        if (\Session::has('ky') || \Session::has('ru')){
            \Session::forget('ky');
            \Session::forget('ru');
        }
        if (\App::getLocale() == "ru"){
            $kyrgyzRoute = "ky/".$route;
            $russianRoute = $route;
            \Session::flash('ky', $kyrgyzRoute);
            \Session::flash('ru', $russianRoute);
        }else{
            $kyrgyzRoute = $route;
            $russianRoute = str_replace("ky/","",$route);
            \Session::flash('ky', $kyrgyzRoute);
            \Session::flash('ru', $russianRoute);
        }
        return 200;
    }

    public function test()
    {
        $environment = \App::environment();
        dd($environment);
    }

    public function newsAll(Request $request)
    {
        $r = $this->changeRoute($request);
        $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $news = News::where('published', '1')->where('lang', $language)
            ->orderBy('created_at', 'desc')->paginate(8);
        $menus = Menu::where('parent_id', null)->where('lang', $language)->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'news' => $news,
            'menus' => $menus,
        ];
        return view('frontend.news', $data);
    }

    public function legislation(Request $request)
    {
        $r = $this->changeRoute($request);
        $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $documents = Document::where('type', 'decree')->where('lang', $language)
            ->where('published', '1')->paginate(10);
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'documents' => $documents,
            'request' => $request,
            'menus' => $menus,
        ];
        return view('frontend.legislation', $data);
    }

    public function factionView(Request $request, Faction $faction)
    {
        $r = $this->changeRoute($request);
        $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $deputies = Deputy::whereFactionId($faction->id)->where('lang','=',$language)->get();
        $data = [
            'fact' => $faction,
            'request' => $request,
            'menus' => $menus,
            'deputies' => $deputies
        ];
        return view('frontend.profile', $data);
    }

    public function commission(Request $request, $id)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $comm = Commission::find($id);
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'menus' => $menus,
            'comm' => $comm,
        ];
        return view('frontend.commission', $data);
    }

    public function complain(Request $request)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $deputies = Deputy::all();
        $factions = Faction::all();
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'menus' => $menus,
            'deputies' => $deputies,
            'factions' => $factions,
        ];
        return view('frontend.complain', $data);
    }

    public function search(Request $request)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $search = $request->input('search');
        $documents = Document::where('type','=','decree')
            ->where('name', 'like', '%' . $search . '%')->limit(4)->get();
        $result = News::where('lang', $language)
            ->where('title', 'like', '%' . $search . '%')->limit(3)->get();
        $data = [
            'request' => $request,
            'menus' => $menus,
            'result' => $result,
            'documents' => $documents
        ];
        return view('frontend.view_search', $data);
    }

    public function deputy(Request $request)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $deputies = Deputy::where('id', '>', '0')->orderBy('fio', 'asc')->paginate(8);
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'deputies' => $deputies,
            'menus' => $menus,
        ];
        return view('frontend.deputy', $data);
    }

    public function documentView(Request $request, $id)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $document = Document::find($id);
        if (!$document) {
            $menus = Menu::where('parent_id', null)->where('lang', $language)
                ->orderBy('position', 'asc')->get();
            $data = [
                'menus' => $menus,
            ];
            return view('frontend.404', $data);
        }
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'document' => $document,
            'menus' => $menus,
        ];
        return view('frontend.view_document', $data);
    }

    public function deputyView(Request $request, $id)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $deputy = Deputy::find($id);
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'deputy' => $deputy,
            'menus' => $menus,
        ];
        return view('frontend.view_deputy', $data);
    }

    public function gallery(Request $request)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'menus' => $menus,
        ];
        return view('frontend.gallery', $data);
    }

    public function live(Request $request)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'menus' => $menus,
        ];
        return view('frontend.live_youtube', $data);
    }


    public function menu(Request $request, $model, $model_id)
    {
        $language = $this->getLanguage($request);
        if ($model == Menu::page) {
            $r = $this->changeRoute($request);
            $page = Page::find($model_id);
            if (!$page) {
                $menus = Menu::where('parent_id', null)->where('lang', $language)
                    ->orderBy('position', 'asc')->get();
                $data = [
                    'menus' => $menus,
                ];
                return view('frontend.404', $data);
            }
            $menus = Menu::where('parent_id', null)->where('lang', $language)
                ->orderBy('position', 'asc')->get();
            $data = [
                'request' => $request,
                'menus' => $menus,
                'page' => $page,
            ];
            return view('frontend.show_page', $data);
        }
        if ($model == Menu::faction) {
            $r = $this->changeRoute($request);
            $faction = Faction::find($model_id);
            if (!$faction) {
                $menus = Menu::where('parent_id', null)->where('lang', $language)
                    ->orderBy('position', 'asc')->get();
                $data = [
                    'menus' => $menus,
                ];
                return view('frontend.404', $data);
            }
            $menus = Menu::where('parent_id', null)->where('lang', $language)
                ->orderBy('position', 'asc')->get();
            $deputies = Deputy::whereFactionId($faction->id)->where('lang','=',$language)->get();
            $data = [
                'request' => $request,
                'menus' => $menus,
                'fact' => $faction,
                'deputies' => $deputies
            ];
            return view('frontend.profile', $data);
        }
        if ($model == Menu::category) {
            $r = $this->changeRoute($request);
            $category = Category::find($model_id);
            if ($category->news->count() > 0) {
                $news = News::where('category_id', $category->id)->where('lang', $language)->paginate(8);
                if (!$news) {
                    $menus = Menu::where('parent_id', null)->where('lang', $language)
                        ->orderBy('position', 'asc')->get();
                    $data = [
                        'menus' => $menus,
                    ];
                    return view('frontend.404', $data);
                }
                $menus = Menu::where('parent_id', null)->where('lang', $language)
                    ->orderBy('position', 'asc')->get();
                $data = [
                    'request' => $request,
                    'menus' => $menus,
                    'news' => $news,
                ];
                return view('frontend.news', $data);
            } elseif ($category->document->count() > 0) {
                $r = $this->changeRoute($request);
                $documents = Document::where('category_id', $category->id)
                    ->where('lang', $language)->paginate(8);
                if (!$documents) {
                    $menus = Menu::where('parent_id', null)->where('lang', $language)
                        ->orderBy('position', 'asc')->get();
                    $data = [
                        'menus' => $menus,
                    ];
                    return view('frontend.404', $data);
                }
                $menus = Menu::where('parent_id', null)->where('lang', $language)
                    ->orderBy('position', 'asc')->get();
                $data = [
                    'request' => $request,
                    'menus' => $menus,
                    'category' => $category,
                    'documents' => $documents,
                ];
                return view('frontend.legislation', $data);
            } elseif ($category->commission->count() > 0) {
                $r = $this->changeRoute($request);
                $commission = Commission::where('category_id', $category->id)->get();
                if (!$commission) {
                    $menus = Menu::where('parent_id', null)->where('lang', $language)
                        ->orderBy('position', 'asc')->get();
                    $data = [
                        'menus' => $menus,
                    ];
                    return view('frontend.404', $data);
                }
                $menus = Menu::where('parent_id', null)->where('lang', $language)
                    ->orderBy('position', 'asc')->get();
                $data = [
                    'request' => $request,
                    'menus' => $menus,
                    'commissions' => $commission,
                    'category' => $category,
                ];
                return view('frontend.view_perma_comm', $data);
            }
        }
        if ($model == Menu::album) {
            $album = Album::find($model_id);
            $r = $this->changeRoute($request);
            if (!$album) {
                $menus = Menu::where('parent_id', null)->where('lang', $language)
                    ->orderBy('position', 'asc')->get();
                $data = [
                    'menus' => $menus,
                ];
                return view('frontend.404', $data);
            }
            $menus = Menu::where('parent_id', null)->where('lang', $language)
                ->orderBy('position', 'asc')->get();
            $data = [
                'request' => $request,
                'menus' => $menus,
                'album' => $album,
            ];
            return view('frontend.gallery', $data);
        }
    }

    public function categoryNews(Request $request, $id)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $news = News::where('category_id', $id)->where('lang', $language)->paginate(8);
        $menus = Menu::where('lang', $language)->where('parent_id', null)->get();
        $data = [
            'request' => $request,
            'menus' => $menus,
            'news' => $news,
        ];
        return view('frontend.news', $data);
    }

    public function complainStore(ComplainStoreRequest $request)
    {
        $request->validate([
            'fio' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'text' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);
        $deputy = Deputy::find($request->input('deputy_id'));
        $complain = new Complain();
        $complain->fio = $request->input('fio');
        $complain->email = $request->input('email');
        $complain->tel = $request->input('tel');
        $complain->subject = $request->input('subject');
        $complain->text = $request->input('text');
        if ($request->file('file')) {
            $attachment = $request->file('file');
            $fileName = $attachment->getFilename();
            $path = $attachment->move('files/shares', $fileName . '.' . $attachment->getClientOriginalExtension());
            $complain->file = $path;
        }
        $complain->deputy()->associate($deputy);
        if ($complain->save()) {
            $this->toast($request, $this->typeSuccess, $this->titleSuccess, 'Письмо успешно отправлено');
        }

        Mail::to('bakhramov.96@gmail.com')->send(new ComplainMail($complain->fio, $complain->email, $complain->subject, $complain->text));
        return redirect()->back();
    }

    public function decreeView(Request $request, $id)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $document = Decree::find($id);
        if (!$document) {
            $menus = Menu::where('parent_id', null)->where('lang', $language)
                ->orderBy('position', 'asc')->get();
            $data = [
                'menus' => $menus,
            ];
            return view('frontend.404', $data);
        }
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'document' => $document,
            'menus' => $menus,
        ];
        return view('frontend.view_decree', $data);
    }

    public function decreeAll(Request $request)
    {
        $r = $this->changeRoute($request);
        $language = $this->getLanguage($request);
        $categories = Decree_Category::all();
        if (!$categories) {
            $menus = Menu::where('parent_id', null)->where('lang', $language)
                ->orderBy('position', 'asc')->get();
            $data = [
                'menus' => $menus,
            ];
            return view('frontend.404', $data);
        }
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'categories' => $categories,
            'menus' => $menus,
        ];
        return view('frontend.view_all_decree', $data);
    }

    public function pageView(Request $request, $slug)
    {
        $r = $this->changeRoute($request);
        $page = Page::where('slug', '=', $slug)->first();
        $language = $this->getLanguage($request);
        $menus = Menu::where('parent_id', null)->where('lang', $language)
            ->orderBy('position', 'asc')->get();
        $data = [
            'request' => $request,
            'menus' => $menus,
            'page' => $page
        ];

        return view('frontend.show_page', $data);
    }

    /**
     * @param Request $request
     * @param $type
     * @param $title
     * @param $message
     */
    public function toast(Request $request, $type, $title, $message)
    {
        $request->session()->flash('status', [
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
    
    public function getLanguage($request)
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $log = $request->route()->getPrefix();
        $language = substr($log, -2);
        if ($locale == 'en') {
            $locale = 'ru';
        }
        if ($language == '') {
            $language = $locale;
        }
        return $language;
    }

}