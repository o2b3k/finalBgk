<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Input;
class CategoryController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::paginate(10);
        return view('category.index',['categories' => $category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::where('parent_id',null)->get();
        return view('category.add',['categories' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->getAddValidationRules(false));
        if ($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }
        $category = new Category();
        $categoryType = $request->input('type');
        if ($categoryType == Category::TYPE_CHILD_CATEGORY){
            $parent = $request->input('parent_id');
            return $this->saveCategory($request,$category,$parent);
        }else {
            return $this->saveCategory($request,$category);
        }
    }

    public function saveCategory(Request $request,Category $category,$parentId = null)
    {
        $category->name = $request->input('name');
        $category->type = $request->input('type');
        $category->parent_id = $parentId;
        $category->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Категория успешно сохранена');
        return redirect()->route('category.Index');
    }

    public function setPublished(Request $request)
    {
        $id = Input::get('id');
        $category = Category::find($id);
        if ($category->published){
            $category->published = false;
            $category->save();
        }else{
            $category->published = true;
            $category->save();
        }
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Категория успешно опубликован');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $categories = Category::all();
        $data = [
            'category' => $category,
            'categories' => $categories
        ];
        return view('category.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->input('name');
        $category->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Категория успешно изменено');
        return redirect()->route('category.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(),['category_id' => 'required|exists:categories,id']);
        if ($validator->fails()){
            $this->toast($request,$this->typeError,$this->titleError,'Категория не найдена');
            return redirect()->back();
        }
        $category = Category::find($request->input('category_id'));
        if ($category->children && $category->children->count() > 0){
            $this->toast($request,$this->typeError,$this->titleError,'Не могу удалить категорию, которая имеет дочерние категории');
            return redirect()->back();
        }
        $category->delete();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Категория удалена успешно');
        return redirect()->route('category.Index');
    }

    public function getValidationRules($hasParent = false)
    {
        $rules = [
            'name' => ['required','max:100'],
            'type' => [
                'required',
                Rule::in(Category::getCategories(false))
            ],
        ];
    }

    private function getAddValidationRules($hasParent = false)
    {
        $rules = [
            'name' => ['required', 'max:100'],
            'type' => [
                'required',
                Rule::in(Category::getCategories(false))
            ],
        ];

        return $rules;
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
