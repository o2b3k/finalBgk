<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ComplainMail extends Mailable
{
    use Queueable, SerializesModels;

    public $fio;
    public $email;
    public $subject;
    public $text;

    /**
     * Create a new message instance.
     *
     * @param $fio
     * @param $email
     * @param $subject
     * @param $text
     */
    public function __construct($fio, $email, $subject, $text)
    {
        $this->fio = $fio;
        $this->email = $email;
        $this->subject = $subject;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.complain_form');
    }
}
