<?php

return [

    /*
     * ==========================================
     * Recaptcha Keys
     * ==========================================
     *
     * See https://www.google.com/recaptcha/admin
     * */
    'public_key'  => env('6LduADwUAAAAAM9P59hsgUvudXIC4HHIFD2nZKAF', ''),
    'private_key' => env('6LduADwUAAAAADmocWsAV8DC-v5ffLjKQ-sgwDeJ', ''),

    /*
     * ==========================================
     * Recaptcha Options
     * ==========================================
     *
     * See https://developers.google.com/recaptcha/docs/display
     * */
    'options'     => [
        'data-theme' => 'light',   // dark, light
        'data-type'  => 'image',   // audio, image
        'data-size'  => 'normal',  // compact, normal
        'lang'       => 'ru',
    ],
];
