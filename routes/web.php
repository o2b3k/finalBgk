<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test','FrontendController@test');
Route::group(['prefix' => LaravelLocalization::setLocale()], function() {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    if (App::isLocale('en')){
        App::setLocale('ru');
    }
    Route::get('/', 'FrontendController@index')->name('index');
    Route::get('/news/{slug}', 'FrontendController@viewNews')->name('viewNews');
    Route::get('/all/news', 'FrontendController@newsAll')->name('front.News');
    Route::get('/legislation', 'FrontendController@legislation')->name('front.Legislation');
    Route::get('/faction/{faction}', 'FrontendController@factionView')->name('front.FactionView');
    Route::get('/commission/{id}', 'FrontendController@commission')->name('front.Commission');
    Route::get('/complain', 'FrontendController@complain')->name('front.Complain');
    Route::post('/complain', 'FrontendController@complainStore')->name('front.ComplainStore');
    Route::get('/deputy', 'FrontendController@deputy')->name('front.Deputy');
    Route::get('/document/{id}', 'FrontendController@documentView')->name('front.DocView');
    Route::get('/deputy/{id}', 'FrontendController@deputyView')->name('front.DeputyView');
    Route::get('/gallery', 'FrontendController@gallery')->name('front.Gallery');
    Route::get('/live', 'FrontendController@live')->name('front.Live');
    Route::get('/category/{id}', 'FrontendController@categoryNews')->name('front.CategoryNews');
    Route::get('/decree/{id}', 'FrontendController@decreeView')->name('front.DecreeView');
    Route::get('/all/decree', 'FrontendController@decreeAll')->name('front.DecreeAll');
    Route::get('/main', function (){
       return view('album');
    });
    Route::get('/search', 'FrontendController@search')->name('front.Search');
    Route::get('/page/{model}/{model_id}', 'FrontendController@menu')->name('front.Menu');
    Route::get('/pages/{slug}', 'FrontendController@pageView')->name('front.pageView');
});

Auth::routes();

Route::get('/home', function (){
    return redirect()->route('category.Index');
})->name('home');

Route::group(['middleware' => 'auth','prefix' => 'admin'], function (){
    Route::get('/index', 'DashboardController@index')->name('dashboard.Index');
    //Category routes
    Route::get('/category','CategoryController@index')->name('category.Index');
    Route::get('/category/add','CategoryController@create')->name('category.Create');
    Route::post('/category','CategoryController@store')->name('category.Store');
    Route::get('/category/edit/{id}','CategoryController@edit')->name('category.Edit');
    Route::delete('/category/delete','CategoryController@destroy')->name('category.Destroy');
    Route::get('/category/edit/{id}','CategoryController@edit')->name('category.Edit');
    Route::post('/category/edit/{id}','CategoryController@update')->name('category.Update');
    Route::post('/category/published','CategoryController@setPublished')->name('category.Published');
    //Pages routes
    Route::get('/pages','PagesController@index')->name('pages.Index');
    Route::get('/pages/add','PagesController@create')->name('pages.Create');
    Route::post('/pages/add','PagesController@store')->name('pages.Store');
    Route::get('/pages/edit/{id}','PagesController@edit')->name('pages.Edit');
    Route::post('/pages/edit/{id}','PagesController@update')->name('pages.Update');
    Route::delete('/pages/delete','PagesController@destroy')->name('pages.Destroy');
    Route::post('/pages/published','PagesController@setPublished')->name('pages.Published');
    //Album controller
    Route::get('/album','AlbumController@index')->name('album.Index');
    Route::get('/album/add','AlbumController@create')->name('album.Create');
    Route::post('/album','AlbumController@store')->name('album.Store');
    Route::get('/album/edit/{id}','AlbumController@edit')->name('album.Edit');
    Route::post('/album/edit/{id}','AlbumController@update')->name('album.Update');
    Route::post('/album/setPublished','AlbumController@setPublished')->name('album.setPublished');
    Route::get('/album/view/{id}','AlbumController@view')->name('album.Show');
    Route::delete('/album/delete','AlbumController@destroy')->name('album.Destroy');
    Route::post('/album/upload/{id}','AlbumController@uploadPhoto')->name('album.Upload');
    Route::delete('/album/photo/delete/{id}','AlbumController@destroyPhoto')->name('album.DestroyPhoto');
    //File controller
    Route::get('/file', function (){
       return view('file.index');
    })->name('files');
    //News controller
    Route::get('/news', 'NewsController@index')->name('news.Index');
    Route::get('/news/add/{lang}', 'NewsController@create')->name('news.Create');
    Route::post('/news/add', 'NewsController@store')->name('news.Store');
    Route::get('/news/edit/{id}', 'NewsController@edit')->name('news.Edit');
    Route::post('/news/edit/{id}', 'NewsController@update')->name('news.Update');
    Route::get('/news/upload/{id}', 'NewsController@uploadForm')->name('news.Upload');
    Route::post('/news/upload/{id}', 'NewsController@uploadPhoto')->name('news.UploadPhoto');
    Route::delete('/news/image/delete/{id}','NewsController@destroyImage')->name('news.destroyPhoto');
    Route::post('/news/setPublished', 'NewsController@setPublished')->name('news.setPublished');
    Route::post('/news/setArchive', 'NewsController@setArchive')->name('news.setArchive');
    Route::delete('/news/delete', 'NewsController@destroy')->name('news.Destroy');
    Route::get('/news/ru', 'NewsController@newsRu')->name('news.Ru');
    Route::get('/news/ky', 'NewsController@newsKy')->name('news.Ky');
    //Document controller
    Route::get('/document', 'DocumentController@index')->name('document.Index');
    Route::get('/document/create', 'DocumentController@create')->name('document.Create');
    Route::post('/document/create', 'DocumentController@store')->name('document.Store');
    Route::post('/document/setPublished', 'DocumentController@setPublished')->name('document.setPublished');
    Route::post('/document/setArchive', 'DocumentController@setArchive')->name('document.setArchive');
    Route::delete('/document/destroy', 'DocumentController@destroy')->name('document.Destroy');
    Route::get('/document/edit/{id}', 'DocumentController@edit')->name('document.Edit');
    Route::post('/document/edit/{id}', 'DocumentController@update')->name('document.Update');
    //Faction controller
    Route::get('/faction', 'FactionController@index')->name('faction.Index');
    Route::get('/faction/add', 'FactionController@create')->name('faction.Create');
    Route::post('/faction/add', 'FactionController@store')->name('faction.Store');
    Route::get('/faction/edit/{id}', 'FactionController@edit')->name('faction.Edit');
    Route::post('/faction/edit/{id}', 'FactionController@update')->name('faction.Update');
    Route::delete('/faction/destroy', 'FactionController@destroy')->name('faction.Destroy');
    Route::get('/faction/addMember/{faction}', 'FactionController@addMemberForm')->name('faction.addMemberForm');
    Route::post('/faction/addMember/{faction}', 'FactionController@addMember')->name('faction.addMember');
    Route::get('/faction/{faction}/{id}', 'FactionController@deleteMember')->name('faction.deleteMember');
    //Deputy controller
    Route::get('/deputy/ru', 'DeputyController@indexRu')->name('deputy.RuIndex');
    Route::get('/deputy/ky','DeputyController@indexKy')->name('deputy.KyIndex');
    Route::get('/deputy/add', 'DeputyController@create')->name('deputy.Create');
    Route::post('/deputy/add', 'DeputyController@store')->name('deputy.Store');
    Route::get('/deputy/edit/{deputy}', 'DeputyController@edit')->name('deputy.Edit');
    Route::post('/deputy/edit/{deputy}', 'DeputyController@update')->name('deputy.Update');
    Route::delete('/deputy/destroy', 'DeputyController@destroy')->name('deputy.Destroy');
    //Complain controller
    Route::get('/complain', 'ComplainController@index')->name('complain.Index');
    Route::post('/complain', 'ComplainController@store')->name('complain.Store');
    Route::get('/complain/view/{complain}', 'ComplainController@show')->name('complain.Show');
    Route::delete('/complain/delete', 'ComplainController@destroy')->name('complain.Destroy');
    //Commission controller
    Route::get('/commission', 'CommissionController@index')->name('commission.Index');
    Route::get('/commission/add', 'CommissionController@create')->name('commission.Create');
    Route::post('/commission/add', 'CommissionController@store')->name('commission.Store');
    Route::get('/commission/edit/{commission}', 'CommissionController@edit')->name('commission.Edit');
    Route::post('/commission/edit/{commission}', 'CommissionController@update')->name('commission.Update');
    Route::get('/commission/addMember/{commission}', 'CommissionController@addMemberForm')->name('commissions.addMemberForm');
    Route::post('/commission/addMember/{id}', 'CommissionController@addMember')->name('commission.addMember');
    Route::get('/commission/{commission}/{id}', 'CommissionController@deleteMember')->name('commission.deleteMember');
    Route::delete('/commission/delete', 'CommissionController@destroy')->name('commission.Destroy');
    //Footer controller
    Route::get('/footer/ru', 'FooterController@indexRu')->name('footer.IndexRu');
    Route::get('/footer/ky', 'FooterController@indexKy')->name('footer.IndexKy');
    Route::get('/footer/add', 'FooterController@create')->name('footer.Create');
    Route::post('/footer', 'FooterController@store')->name('footer.Store');
    Route::get('/footer/link/{footer}', 'FooterController@show')->name('footer.Show');
    Route::post('/footer/link/{footer}', 'LinkController@store')->name('footer.storeLink');
    Route::delete('/footer/link/delete', 'LinkController@destroy')->name('link.Destroy');
    Route::delete('/footer/delete', 'FooterController@destroy')->name('footer.Destroy');
    Route::get('/footer/edit/{footer}', 'FooterController@edit')->name('footer.Edit');
    Route::post('/footer/edit/{footer}', 'FooterController@update')->name('footer.Update');
    //Menu controller
    Route::get('/menu', 'MenuController@index')->name('menu.Index');
    Route::get('/menu/add', 'MenuController@create')->name('menu.Create');
    Route::post('/menu/add', 'MenuController@store')->name('menu.Store');
    Route::get('/menu/{id}', 'MenuController@edit')->name('menu.Edit');
    Route::post('/menu/{id}', 'MenuController@update')->name('menu.Update');
    Route::get('/menu/show/{menu}', 'MenuController@show')->name('menu.Show');
    Route::delete('/menu/delete', 'MenuController@destroy')->name('menu.Destroy');
    Route::get('/menu/russian/all', 'MenuController@menuRu')->name('menu.Russian');
    Route::get('/menu/kyrgyz/all', 'MenuController@menuKy')->name('menu.Kyrgyz');
    //DecreeCategoryController
    Route::get('/decree/category/all', 'DecreeCategoryController@index')->name('resolution.Index');
    Route::get('/decree/category/add', 'DecreeCategoryController@create')->name('resolution.Create');
    Route::post('/decree/category/add', 'DecreeCategoryController@store')->name('resolution.Store');
    Route::delete('/decree/category/destroy', 'DecreeCategoryController@destroy')->name('resolution.Delete');
    Route::get('/decree/category/edit/{id}', 'DecreeCategoryController@edit')->name('resolution.Edit');
    Route::post('/decree/category/edit/{id}', 'DecreeCategoryController@update')->name('resolution.Update');
    //DecreeController
    Route::get('/decree', 'DecreeController@index')->name('decree.Index');
    Route::get('/decree/add', 'DecreeController@create')->name('decree.Create');
    Route::post('/decree/add', 'DecreeController@store')->name('decree.Store');
    Route::get('/decree/edit/{decree}', 'DecreeController@edit')->name('decree.Edit');
    Route::post('/decree/edit/{decree}', 'DecreeController@update')->name('decree.Update');
    Route::delete('/decree/destroy', 'DecreeController@destroy')->name('decree.Destroy');
});